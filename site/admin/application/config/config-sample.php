<?php 


// CORE
$config['title'] = "Volksfreund Voting";
$config['title_short'] = "voting";
$config['language'] = "de";

if($_SERVER["HTTP_HOST"] == "voting.volksfreund.local") {  
	$config['domain'] = "http://voting.volksfreund.local"; 
	$config['base_url'] = 'http://voting.volksfreund.local/admin/'; // Base URL including trailing slash (e.g. http://localhost/)
	// DATABASE
	$config['db_host'] = 'localhost'; // Database host (e.g. localhost)
	$config['db_name'] = 'voting.volksfreund.de'; // Database name
	$config['db_username'] = 'root'; // Database username
	$config['db_password'] = 'root'; // Database password

} else {
	$config['domain'] = "https://voting.volksfreund.de";
	$config['base_url'] = 'https://voting.volksfreund.de/admin/'; // Base URL including trailing slash (e.g. http://localhost/)
	// DATABASE
	$config['db_host'] = ''; // Database host (e.g. localhost)
	$config['db_name'] = ''; // Database name
	$config['db_username'] = ''; // Database username
	$config['db_password'] = ''; // Database password

}


$config['media_url'] = $_SERVER['DOCUMENT_ROOT'].'/media/';
$config['setup_url'] = $_SERVER['DOCUMENT_ROOT'].'/application/config/setup.php';
$config['image_url'] = $config['domain'].'/media/';
$config['default_controller'] = 'home'; // Default controller to load
$config['error_controller'] = 'error'; // Controller used for errors (e.g. 404, 500 etc)
$config['version'] = '1.0.0';


// PASSWORD SALT
$config['salt'] = 'zX{=_Ukb7La;GrU_Dzg_';
$config['token'] = 'K1fKTUsDelnjYXpCNnS8MoYPJxtoSY';

// MAIL 
/*
$config['mail_reply'] = "mailtest@tmvg-tools.de";
$config['mail_name'] = "BURGENERATOR";
$config['mail_use_smtp'] = true;
$config['mail_smtp_auth'] = true;
$config['mail_host'] = "dedi2802.your-server.de";
$config['mail_smtp_username'] = "mailtest@tmvg-tools.de";
$config['mail_smtp_password'] = "k7rQt5UxP36TV4Mh";
$config['mail_smtp_encryption'] = "tls";
$config['mail_smtp_port'] = "587";
*/

$config['mail_reply'] = "no-reply@a-f-e.biz";
$config['mail_name'] = "Alles fürs Event";
$config['mail_use_smtp'] = true;
$config['mail_smtp_auth'] = true;
$config['mail_host'] = "mail.your-server.de";
$config['mail_smtp_username'] = "no-reply@burgenerator.de";
$config['mail_smtp_password'] = "";
$config['mail_smtp_encryption'] = "tls";
$config['mail_smtp_port'] = "587";

$config['mail_template_header'] =  'views/mail/header.php';
$config['mail_template_footer'] =  'views/mail/footer.php';

$config['mail_template_path'] =  'views/mail/';

/* HTACCESS BACKUP
	
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{HTTP_HOST} !^burgenerator\.de$ [NC,OR]
RewriteCond %{HTTPS} =off
RewriteRule ^(.*)$ https://burgenerator.de/$1 [R=301,L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d 
RewriteRule . index.php [L]
</IfModule>

# Prevent file browsing
Options -Indexes

*/



?>