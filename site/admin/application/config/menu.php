<?php

// Menu Arrays


// Role SALES
$config_menu['role']['sales'] = array("slug"=>"sales", "name"=>"Sales", "permissions"=>array("sales", "admin"));
$config_menu['role']['sales']['menu'] = array(
	
	"Dashboard: Aufträge"=>array("dashboardsales", "fa fa-euro"),
	"neuer Auftrag"=>array("neworder", "fa-plus"),
	"Reporting"=>array("order", "fa-bar-chart "),

);


// Role Faktura
$config_menu['role']['faktura'] = array("slug"=>"faktura", "name"=>"Faktura", "permissions"=>array("faktura", "sales", "admin"));
$config_menu['role']['faktura']['menu'] = array(
	"Dashboard: Faktura"=>array("dashboardfaktura", "fa fa-credit-card"),
);

// Role CRM
$config_menu['role']['crm'] = array("slug"=>"crm", "name"=>"CRM", "permissions"=>array("crm", "sales", "admin"));
$config_menu['role']['crm']['menu'] = array(
	"Dashboard: CRM"=>array("dashboard", "fa fa-dashboard"),
	"Aktivität"=>array("offer", "fa fa-rocket"),
	"Kunden"=>array("clients", "fa fa-users"),

);

// Role Admin
$config_menu['role']['admin'] = array("slug"=>"admin", "name"=>"Admin", "permissions"=>array("admin"));
$config_menu['role']['admin']['menu'] = array(

	"Admins"=>array("admin", "fa-star"),
	

);

?>