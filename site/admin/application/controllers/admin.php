<?php


error_reporting(1);
// Melde alle Fehler außer E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

class Admin extends Controller {
	
	function __construct() {
		if($this->isLogged() != "logged") $this->redirect('error');
	}

	function index()
	{
		if(!$this->checkRole(array("admin"))) $this->redirect('/error/permission');

		$template = $this->loadView('admin/admin_view');
		$model = $this->loadModel('AdminModel');
		$allAdmins = $model->getAllAdmins();
		$template->set("title", "BurgerMeister");
		$template->set("admins", $allAdmins);
		$template->render();
	}
	
    public function newAdmin() {
	    
		if(!$this->checkRole(array("admin"))) $this->redirect('/error/permission');

	    
   	    $template = $this->loadView('admin/admin_view');
	    $model = $this->loadModel('AdminModel');
		$auth = $this->loadHelper('auth_helper');
	    
	    if($_POST) {
			$check = $this->checkRequiredFields($_POST, "email,firstname,lastname");
			
			if($check) {
				$template->info("Admin konnte nicht angelegt werden: ", "Achtung", "danger", $check, "li");
			} else {
				// Email is unique
				if($model->checkAdminByEmail($_POST["email"])) {

					$permission = array();
					if(is_array($_POST["permission"])){
						foreach($_POST["permission"] as $key => $p){
							if($p == "on") $permission[] = $key;
						}
						
					}
					$session = $this->loadHelper('session_helper');

					$insertData = array(
	                    "userEmail" => $_POST["email"],
	                    "userPass" => $auth->makePassword($_POST["password"]),
	                    "userStatus" => "1",
	                    "userFirstName" => $_POST["firstname"],
	                    "userLastName" => $_POST["lastname"],
	                    "userRole" => $_POST["role"],

	                    "gp" => $_POST["gp"],
						"honorar" => $_POST["honorar"],
						"tel" => $_POST["tel"],
						"permission" => serialize($permission),


	                    
					);
					
					
		
					$modelout = $model->insertAdmin($insertData);
					
					
					if($modelout) {
						if($_POST["sendUserInfo"] == "on") {
							
							$this->sendLoginData($modelout, $_POST["password"]);
							$template->info("Admin wurde gespeichert. $mailstatus", "Info", "success");
						} else {
							$template->info("Admin wurde gespeichert.", "Info", "success");
						}
						
						
					} else {
						$template->info("Admin konnte nicht angelegt werden: Unbekannter Fehler", "Achtung", "danger");
					}
					
					$template->info("Admin wurde gespeichert.", "Info", "success");
				} else {
					$template->info("Admin konnte nicht angelegt werden. E-Mail Adresse (".$_POST["email"].") ist schon vorhanden.", "Achtung", "danger");
				}
			}
		} else {
		    $template->info("Admin konnte nicht angelegt werden. Keine Daten vorhanden.", "Achtung", "danger");
	    }	
			
	   	$allAdmins = $model->getAllAdmins();
		$template->set("admins", $allAdmins); 
	    
		$template->render();
	    
    }
    
    
	function edit($id = false)
	{

		$template = $this->loadView('admin/admin_edit');

		if(!$this->checkRole(array("admin")) && ($id != $_SESSION["userID"])) $this->redirect('/error/permission');

		if(!$this->checkRole(array("admin"))) $template = $this->loadView('admin/admin_user_edit');

		if($id) {
			
			$model = $this->loadModel('AdminModel');
			
			if($_POST) {
				

				$check = $this->checkRequiredFields($_POST, "email,firstname,lastname");
				
				
		
				if($check) {
					$template->info("Daten wurden nicht gespeichert: ", "Achtung", "danger", $check, "li");
				} else {
						
						if(is_array($_POST["permission"])){
							$permission = array();
							foreach($_POST["permission"] as $key => $p){
								if($p == "on") $permission[] = $key;
							}
						}
						$updateData = array();
						
							if(array_key_exists("status", $_POST)) $updateData["userStatus"] = $_POST["status"];
							$updateData["userFirstName"] = $_POST["firstname"];
							$updateData["userLastName"] = $_POST["lastname"];
							if(array_key_exists("role", $_POST)) $updateData["userRole"] = $_POST["role"];
							$updateData["gp"] = $_POST["gp"];
							$updateData["tel"] = $_POST["tel"];
							if(array_key_exists("permission", $_POST)) $updateData["permission"] = serialize($permission);
							if(array_key_exists("clickMe", $_POST)) $updateData["clickMe"] = ($_POST["clickMe"] == "on")?1:0;
							if(array_key_exists("autoLive", $_POST)) $updateData["autoLive"] = $_POST["autoLive"];


					
						
						$where = array("userID" => $_POST["id"]); 
	
						$modelout = $model->updateAdmin($updateData, $where);
						
						if($modelout) {
							$template->info("Admin wurde gespeichert.", "Info", "success");
						} else {
							$template->info("Daten wurden nicht gespeichert: Unbekannter Fehler", "Achtung", "danger");
						}
						
						$template->info("Admin wurde gespeichert.", "Info", "success");
				}
			} 		
			

			
			$admin = $model->getAdminById($id);
			$template->set("admin", $admin);
			$template->render();
		} else {
			$this->redirect('admin');
		}
	}
	
	function newPass()
	{
		if(!$this->checkRole(array("admin"))) $this->redirect('/error/permission');

		if($_REQUEST["id"]) {
			
			$template = $this->loadView('admin/admin_edit');
			$model = $this->loadModel('AdminModel');
			$auth = $this->loadHelper('auth_helper');
			
			if($_POST) {


					$updateData = array(
	                    "userPass" => $auth->makePassword($_POST["password"])
					);
					
					$where = array("userID" => $_POST["id"]); 

					$modelout = $model->updateAdmin($updateData, $where);

					if($modelout) {

						if($_POST["sendUserInfo"] == "on") {

							$this->sendLoginData($_POST["id"], $_POST["password"]);							
							$template->info("Admin wurde gespeichert und per Mail gesendet.", "Info", "success");
						} else {
							$template->info("Admin wurde gespeichert.", "Info", "success");
						}
						
						
					} else {
						$template->info("Daten wurden nicht gespeichert: Unbekannter Fehler", "Achtung", "danger");
					}
					
					//$template->info("Admin wurde gespeichert.", "Info", "success");
			}
			

			
			$admin = $model->getAdminById($_REQUEST["id"]);
			$template->set("admin", $admin);
			$template->render();
		} else {
			$this->redirect('admin');
		}
	}	
	
	
	
	function userNewPass()
	{
		if($_REQUEST["id"]) {
			
			$template = $this->loadView('admin/admin_user_edit');
			$model = $this->loadModel('AdminModel');
			$auth = $this->loadHelper('auth_helper');
			$authModel = $this->loadModel('AdminLoginModel');

			if($_POST) {
				try{
					
					if($_POST["old_password"] != $_POST["old_password_sec"]) throw new Exception ("Alte Passwortdaten stimmen nicht überein");

					$login = $authModel->loginAdmin($_SESSION["email"], $auth->makePassword($_POST["old_password"]));

					if(count($login) == 0) throw new Exception ("Eingegebenes Passwort stimmt nicht mit dem Passwort überein");

					if(strlen($_POST["password"] <= 5)) throw new Exception ("Passwort zu kurz");

					$updateData = array(
	                    "userPass" => $auth->makePassword($_POST["password"])
					);
					
					$where = array("userID" => $_POST["id"]); 

					$modelout = $model->updateAdmin($updateData, $where);

					if($modelout) {

						if($_POST["sendUserInfo"] == "on") {

							$this->sendLoginData($_POST["id"], $_POST["password"]);							
							$template->info("Admin wurde gespeichert und per Mail gesendet.", "Info", "success");
						} else {
							$template->info("Admin wurde gespeichert.", "Info", "success");
						}
						
						
					} else {
						$template->info("Daten wurden nicht gespeichert: Unbekannter Fehler", "Achtung", "danger");
					}
				}catch(Exception $e){
							$template->info($e->getMessage(), "Achtung", "danger");
				}	
					//$template->info("Admin wurde gespeichert.", "Info", "success");
			}
			

			
			$admin = $model->getAdminById($_REQUEST["id"]);
			$template->set("admin", $admin);
			$template->render();
		} else {
			$this->redirect('admin');
		}
	}
	
	
	function sendLoginData($userID, $pass) {
		

		
		$model = $this->loadModel('AdminModel');
		$userData = $model->getAdminById($userID);
		$mail = $this->loadHelper('mail_helper');
		
		$mailtemplate = $this->loadView('mail/admin_register');
		$mailtemplate->set("title", TITLE);
		$mailtemplate->set("path", DOMAIN);
		$mailtemplate->set("adminpath", BASE_URL);
		
		$mailtemplate->set("email", $userData["userEmail"]);
		$mailtemplate->set("firstname", $userData["userFirstName"]);
		$mailtemplate->set("lastname", $userData["userLastName"]);
		$mailtemplate->set("pass", $pass);
		$mailout = $mail->sendMail($userData["userEmail"], $userData["userFirstName"], $userData["userLastName"], "Ihre Zugangsdaten", $mailtemplate->getRendered());
		return true;
		}
    
}

?>
