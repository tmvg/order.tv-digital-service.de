<?php
	
class Ajax extends Controller {


	function updateWarehouse()
	{
		if($this->isLogged() != "logged" || !$this->checkRole(array("kitchen", "admin"))) return "[Fehler] Berechtigung fehlt."; else {
			$model = $this->loadModel('BurgerModel');		
			$result = $model->saveUpdateWarehouseData($_REQUEST['id'], $_REQUEST['action'], $_REQUEST['value']);
			
			if($result)
			return "Ok, das haben wir gespeichert!";
			else
			return "[Fehler] Wert konnte nicht gespeichert werden.";
		}

	}
	
	
	function updateBurgerStatus() {
		if($this->isLogged() != "logged" || !$this->checkRole(array("kitchen", "admin"))) return "[Fehler] Berechtigung fehlt."; else {
			$model = $this->loadModel('DashboardModelNew');		
			$result = $model->setProductionStatus($_POST["orderID"], $_REQUEST['status']);
			
			if($result)
			return true;
			else
			return "[Fehler] Wert konnte nicht gespeichert werden.";
		}

	}
	
	function getBurger(){
		$model = $this->loadModel('DashboardModelNew');	
		try{
			if(!isset($_REQUEST["orderID"])) throw new Exception("Order ID Fehlt");

			$template = $this->loadView('dashboard/burger_preview_new');

			$burger = $model->getOrderById($_REQUEST["orderID"]); 	

			$template->set("orders", $burger);
			$burgerView = $template->getRendered();
		
			echo(json_encode(array("status" => "success", "id" => $_REQUEST["orderID"],"burger" => $burgerView )));
		}catch(Exception $e){
			echo(json_encode(array("status" => "error", "message" => $e->getMessage())));
		}
		die();
	
	}
	
	
	function insertEvent()
	{
		if($this->isLogged() != "logged" || !$this->checkRole(array("admin"))) return "[Fehler] Berechtigung fehlt."; else {
			$model = $this->loadModel('EventModel');		
			
			$day = strtotime($_REQUEST["day"]);
			$from = strtotime($_REQUEST["day"]." ".$_REQUEST["from"]);
			$to = strtotime($_REQUEST["day"]." ".$_REQUEST["end"]);
			$slottime = $_REQUEST["slottime"];
			$burgerInSlot = $_REQUEST["burgerInSlot"];
			
			
			echo "<pre>";
			print_r($from);
			echo "</pre>";
			
			$result = $model->insertEvent($day, $from, $to, $slottime, $burgerInSlot);
			
			if($result)
			return "Ok, das haben wir gespeichert!";
			else
			return "[Fehler] Burger konnte nicht gespeichert werden.";
			
		}

	}	
	
}
	
?>