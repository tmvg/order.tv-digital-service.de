<?php
	
	//Warengruppen

class bildupload extends Controller {
	
	var $user = false;
	
	function __construct(){
		if($this->isLogged() != "logged") $this->redirect('login');
		$user = $this->loadHelper('auth_helper');
		$this->user = $user->getUser();
		
		if($this->user["userRole"] == "producer"){
			$permission = unserialize($this->user["permission"]);
			if(!is_array($permission)) $this->redirect('error/permission');
			if(! in_array(strtolower(get_class()), $permission)) $this->redirect('error/permission');
			
		}
	}
	
	function index(){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('bildupload/bildupload_view');

		$template->render();

	}

	
	function edit($id = NULL , $callback = false){
		global $config;
		

		if($this->isLogged() != "logged") $this->redirect('login');
		
		$model = $this->loadModel('BilduploadModel');

		$template = $this->loadView('bildupload/single_bildupload_view');

		$data["prozessing"] = -1;
		
		try{
			if (isset($_POST['submit'])){

				$voting = $_POST["voting"];
				
								
				$data["title"] = 	$voting["title"];
				$data["element"] = $voting["element"];
				$data["note"] = $voting["note"];
				$data["irRessort"] = $voting["irRessort"];			
				$data["user"] = $_SESSION["userID"];			
		
				
				
				$report = "<strong>Titel Bilderstrecke</strong>: ".	$voting["title"] ."<br />";
				$report = "<strong>Anzahl Bilder</strong>: ".	count($voting["element"]). "<br />";

				$report .= "--------------------<br />";

		
				if(is_array($voting["element"])){
					foreach($voting["element"] as $el){
						
					$report .= "<strong>BildID</strong>: ".	$el["image"] ."<br />";
					$report .= "<strong>Überschrift</strong>: ".		$el["ueberschrift"] ."<br />";
					$report .= "<strong>Bildtext</strong>: ".		$el["bildtext"] ."<br />";
					$report .= "<strong>Rechteinhaber</strong>: ".		$el["rechteinhaber"] ."<br />";
					$report .= "<strong>Fotograf</strong>: ".		$el["fotograf"] ."<br />";
						
						$report .= "--------------------<br />";
						
					}
				}
		
				
				
				if(isset($voting["id"]) && strlen($voting["id"]) > 0) $id = $model->updateVoting($voting["id"],$data);
				else {
					$id = $model->insertBilderstrecke($data);
							$mail = $this->loadHelper('mail_helper');

										
					$mail->sendMail($this->user["userEmail"], $this->user["userFirstName"], $this->user["userLastName"],  "Neuer MDB Upload", $report);
					
					$mail->sendMail("d.vilter@tmvg.de", "David", "Vilter",  "Neuer MDB Upload", $report);

					
				}
				if($callback) return $id;

				$this->redirect('bildupload/');

			
			}
			
			if($id){
				$data = $model->getVotingById($id);
				if(!in_array($this->user["userRole"],array("admin","editor"))){
					if($this->user["userID"] != $data["user"]) $this->redirect('bildupload');
				}
				
			}
			
		}catch(Exception $e){

			$template->set("error", $e->getMessage());
			$data = $model->getVotingById($id);

		}
		
		if($callback) return $id;
		
		$template->set("user",$this->user);
		$template->set("irRessorts",$model->getIRRessorts());	
		$template->set("data", $data);
		
		$template->render();
	}
	
	function produce($id = NULL){
		
		$id = $this->edit($id,true);
		
		$model = $this->loadModel('BilduploadModel');
		
		try{
			if($this->user["autoLive"] == 1) $model->produce($id);		
		}catch(Exception $e){

			$template = $this->loadView('bildupload/single_bildupload_view');

			$template->set("error", $e->getMessage());
			$data = $model->getVotingById($id);
			$template->set("irRessorts",$model->getIRRessorts());	
			$template->set("data", $data);
			
			$template->render();
		}
		
		$this->redirect('bildupload/edit/'.$id);

	}
	
		
	function delete($id){
		global $config;
		if($this->isLogged() != "logged") $this->redirect('login');

		$model = $this->loadModel('ProductModel');
		
		try{
			
			$model->deleteProduct($id);
			
		}catch(Exception $e){
			echo($e->getMessage());
			die();
		}

		$this->redirect('products/index');

	}
	

	
	public function ajax_votingTable($data = null){
		
		global $config;
	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		
		$table = "bilderstrecke`,`admin";
		
		$primaryKey = 'id';
		
		$whereAll[] = "type = 'bildupload'";
		
		if($this->user["userRole"] == "producer"){
			$whereAll[] = "user = ".$this->user["userID"];
		}
		
		$whereAll = array(implode(" AND ", $whereAll));
		$columns = array(

		    array( 'db' => 'id',  'dt' => 0  ),
		    array( 'db' => 'title',   'dt' => 1  ),
   		    array( 'db' => 'created', 'dt' => 2 ,
   		    'formatter' => function( $d, $row ) {
		            return date("d.m.Y H:i",$d);
		        } ),
   			array( 'db' => 'id',     'dt' => 3,
		        'formatter' => function( $d, $row ) {
		            return json_encode($row);
		        } 
		       ),
		       
		    array( 'db' => 'userFirstName',   'dt' => 4  ),
		    array( 'db' => 'userLastName',   'dt' => 5  ),
		    array( 'db' => 'prozessing',   'dt' => 6  ),
 
 
		   );
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 
		 
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
				 $where = "bilderstrecke.user = admin.userID";
 
		 
		echo json_encode(
		    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,$where, $whereAll )
		);
		

	}

	
	public function ajax_generate_productID(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		$group_id = $_REQUEST["group_id"];
		
		$model = $this->loadModel('ProductModel');

		$oldProductId = $model->getLastProductInGroup($group_id);
		
		$newProductId = $oldProductId + 1;
		
		$product_id = str_pad($newProductId, 4 ,'0', STR_PAD_LEFT);

// 		$product_id = $this->generateProductNumber();
		
		while($model->getProductByProductIdAndGroupId($product_id,$group_id)){

			$newProductId = $oldProductId + 1;
 
			$product_id = str_pad($newProductId, 4 ,'0', STR_PAD_LEFT);
		}
		
		echo json_encode(array('status' => "true","product_id" => $product_id));							


		exit;
	}




}

?>
