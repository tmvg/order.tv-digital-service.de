<?php
	
	//Warengruppen

class clients extends Controller {
	
	var $user = false;
	
	function __construct(){
		if($this->isLogged() != "logged") $this->redirect('login');
		$user = $this->loadHelper('auth_helper');
		$this->user = $user->getUser();
		
		if($this->user["userRole"] == "crm"){
			$permission = unserialize($this->user["permission"]);
			if(!is_array($permission)) $this->redirect('error/permission');
			if(! in_array(strtolower(get_class()), $permission)) $this->redirect('error/permission');
			
		}
	}
	
	function index(){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('crm/client/clients_view');
		
	
		$template->render();

		
		

	}
	
	function edit($id = false){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('crm/client/clients_edit_view');
		
		$data = array();

		$model = $this->loadModel('ClientModel');
		$data = array();
		if($id) $data = $model->getClientByClientId($id);

		$template->set("data", $data );
	
		$template->render();
		
	}
	
	function save(){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$model = $this->loadModel('ClientModel');

		$id = false;
	
		if(array_key_exists("id",$_POST)) $id = $_POST["id"];
	
	
		$data = array();
		
		if(array_key_exists("company",$_POST)) $data["company"]		= $_POST["company"];
		if(array_key_exists("gp",$_POST)) $data["gp"]			= $_POST["gp"];
		if(array_key_exists("salutation",$_POST)) $data["salutation"]	= $_POST["salutation"];
		if(array_key_exists("firstname",$_POST)) $data["firstname"]	= $_POST["firstname"];
		if(array_key_exists("lastname",$_POST)) $data["lastname"]		= $_POST["lastname"];
		if(array_key_exists("zip",$_POST)) $data["zip"]			= $_POST["zip"];
		if(array_key_exists("city",$_POST)) $data["city"]			= $_POST["city"];
		if(array_key_exists("email",$_POST)) $data["email"]		= $_POST["email"];
		if(array_key_exists("phone",$_POST)) $data["phone"]		= $_POST["phone"];
		if(array_key_exists("mobile",$_POST)) $data["mobile"]		= $_POST["mobile"];
		if(array_key_exists("road",$_POST)) $data["road"]			= $_POST["road"];
		if(array_key_exists("nr",$_POST)) $data["nr"]			= $_POST["nr"];
		if(array_key_exists("country",$_POST)) $data["country"] = $_POST["country"];
		if(array_key_exists("status",$_POST)) $data["status"] = $_POST["status"];


			
		if($id){
			//update
			$model->updateClient($id , $data);
		}else{
			//insert 
			$id = $model->insertClient($data);
		}
	
		if($id) $this->redirect('clients/edit/'.$id);
		exit;		
	}

	
	
	public function ajax_clientTable($data = null){
		
		global $config;
	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		$table = "client";
		$primaryKey = 'id';
		$columns = array(
		    array( 'db' => 'company', 'dt' => 0 ),
		    array( 'db' => 'gp',   'dt' => 1  ),
		    array( 'db' => 'firstname',   'dt' => 2  ),
			array( 'db' => 'lastname',     'dt' => 3  ), 
   		    array( 'db' => 'zip', 'dt' => 4  ),
   		    array( 'db' => 'city', 'dt' => 5  ),
   			array( 'db' => 'id', 'dt' => 6  ),
   		    

 
		   );
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		 $where = "status=1";
		echo json_encode(
		    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where )
		);
		

	}
	
	
	
}