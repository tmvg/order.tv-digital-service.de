<?php
	
	//Warengruppen

class dashboard extends Controller {
	
	var $user = false;
	
	function __construct(){
		if($this->isLogged() != "logged") $this->redirect('login');
		$user = $this->loadHelper('auth_helper');
		$this->user = $user->getUser();
		
		if($this->user["userRole"] == "crm"){
			$permission = unserialize($this->user["permission"]);
			if(!is_array($permission)) $this->redirect('error/permission');
			if(! in_array(strtolower(get_class()), $permission)) $this->redirect('error/permission');
			
		}
	}
	
	function index(){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('crm/dashboard_view');
		
		$data = array();

		$model = $this->loadModel('OfferModel');
	
		
		$template->set("data", $data);
		$template->set("crm_types", $model->getCRMType());
		
		$template->set("kpi_1", $model->getKPI(1));
		$template->set("kpi_2", $model->getKPI(2));
		$template->set("kpi_3", $model->getKPI(3));
		$template->set("kpi_0", $model->getKPI(-1));
		
		$template->render();

		
		

	}
	
	public function ajax_crmTable($data = null){
		
		global $config;
		
		$model = $this->loadModel('OfferModel');

	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		$table = "crm_dashboard";
		$primaryKey = 'id';
		$columns = array(
		    array( 'db' => 'company', 'dt' => 0 ),
		    array( 'db' => 'gp', 'dt' => 1  ),
		    array( 'db' => 'crm_title',   'dt' => 2  ),
		    array( 'db' => 'status',   'dt' => 3  ),
		    array( 'db' => 'price',   'dt' => 4 ),
			array( 'db' => 'date',     'dt' => 5  ), 
   		    array( 'db' => 'crm_final_date', 'dt' => 6  ),
   		    array( 'db' => 'type', 'dt' => 7  ),
   		    array( 'db' => 'project_name', 'dt' => 8  ),
   		    array( 'db' => 'id', 'dt' => 9  ),

 
		   );
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 
		$where = array();
		if(array_key_exists("status_value",$_REQUEST)){
			if($_REQUEST["status_value"] != 9999) $where[] = "status_value  = ".$_REQUEST["status_value"]; 
 		}
 		$range[0] = 0;
 		$range[1] = 0;
 		

		if(array_key_exists("daterange",$_REQUEST)){

			if(is_array($_REQUEST["daterange"])){
				if(count($_REQUEST["daterange"]) == 2){
					if($_REQUEST["daterange"][0] > 0 AND $_REQUEST["daterange"][1] > 0){
		
						$range[0] = $_REQUEST["daterange"][0];
						$range[1] = $_REQUEST["daterange"][1];
						
						$where[] = "date between '". $_REQUEST["daterange"][0] ."' and '". $_REQUEST["daterange"][1] ."'";
					}
				}
			}
			

 		}

 		$where = implode(" AND ", $where);

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		$data = (
			SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
			
			);


		if($range[0] > 0){
			$kpi[1] = intval($model->getKPI(1,$range[0],$range[1]));
			$kpi[2] = intval($model->getKPI(2,$range[0],$range[1]));
			$kpi[3] = intval($model->getKPI(3,$range[0],$range[1]));
			$kpi[0] = intval($model->getKPI(-1,$range[0],$range[1]));
			$data["kpi"] = $kpi;	
		}else{
			$kpi[1] = intval($model->getKPI(1));
			$kpi[2] = intval($model->getKPI(2));
			$kpi[3] = intval($model->getKPI(3));
			$kpi[0] = intval($model->getKPI(-1));
			$data["kpi"] = $kpi;
		}
		
		echo(json_encode($data));

	}

	
	
	
}