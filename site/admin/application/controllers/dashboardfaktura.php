<?php
	
	//Warengruppen

class dashboardfaktura extends Controller {
	
	var $user = false;
	
	function __construct(){
		if($this->isLogged() != "logged") $this->redirect('login');
		$user = $this->loadHelper('auth_helper');
		$this->user = $user->getUser();
		
		if($this->user["userRole"] == "producer"){
			$permission = unserialize($this->user["permission"]);
			if(!is_array($permission)) $this->redirect('error/permission');
			if(! in_array(strtolower(get_class()), $permission)) $this->redirect('error/permission');
			
		}
	}

	function index(){
		global $config;
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('order/order_dashboard_faktura_view');
		
		$template->set("data", $data);
		
		$template->render();
	}
	
	public function ajax_find_vermittler(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		$model = $this->loadModel('OrderModel');
		
		if(!is_null($_GET["id"])){
			$data = $model->get_vermittler_by_id($_GET["id"]);
		}else{
			$data = $model->query_vermittler($_GET["search"]);
		}
		
		$result = new \stdClass;
		
		foreach ($data as $d){
			$result->result[] = (object)array("id" => $d["beraterID"],"text" => ($d["vorname"]." ". $d["nachname"]));
		}
				
		echo json_encode($result);							
		exit;
	}


	public function ajax_find_client(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		$model = $this->loadModel('OrderModel');
		
		if(!is_null($_GET["search"])){
			$data = $model->query_client($_GET["search"]);
		}else{
			$data = $model->query_client();
		}
		
		$result = new \stdClass;
		if(is_array($data)){
			foreach ($data as $d){
				$result->result[] = (object)array("id" => $d["id"],"text" => $d["company"] ." (".$d["gp"].")","firstname"  => $d["firstname"], "lastname"  => $d["lastname"], "phone"  => $d["phone"], "mobile"  => $d["mobile"], "email"  => $d["email"], "salutation"  => $d["salutation"], "zip"  => $d["zip"], "city"  => $d["city"], "road"  => $d["road"], "nr"  => $d["nr"]);
			}
		}
		echo json_encode($result);							
		exit;
	}


	public function ajax_get_new_position(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
	
		$model = $this->loadModel('OrderModel');

	
		$template = $this->loadView('order/single_position_templatePart');
		
		$template->set("rand",$this->generateRandomString());
		$template->set("orderTypes", $model->getOrderTypes());

		echo json_encode(array('status' => "true","content" => $template->getRendered()));							

		exit;
	}


	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

	    return "a_".$randomString;
	}
	
	
	public function ajax_add_client(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}

		$model = $this->loadModel('OrderModel');

		$pData = $_POST["data"];
			
		ob_start();
		var_dump($_POST["data"]);
		$return = ob_get_contents();
		ob_end_clean();
		
		$data = array();
		if(array_key_exists("company", $pData)) $data["company"] = $pData["company"];
		if(array_key_exists("firstname", $pData)) $data["firstname"] = $pData["firstname"];
		if(array_key_exists("lastname", $pData)) $data["lastname"] = $pData["lastname"];
		if(array_key_exists("salutation", $pData)) $data["salutation"] = $pData["salutation"];
		if(array_key_exists("road", $pData)) $data["road"] = $pData["road"];
		if(array_key_exists("nr", $pData)) $data["nr"] = $pData["nr"];
		if(array_key_exists("zip", $pData)) $data["zip"] = $pData["zip"];
		if(array_key_exists("city", $pData)) $data["city"] = $pData["city"];
		if(array_key_exists("gp", $pData)) $data["gp"] = $pData["gp"];
		if(array_key_exists("email", $pData)) $data["email"] = $pData["email"];
		if(array_key_exists("phone", $pData)) $data["phone"] = $pData["phone"];
		if(array_key_exists("mobile", $pData)) $data["mobile"] = $pData["mobile"];
		if(array_key_exists("country", $pData)) $data["country"] = $pData["country"];
		
		if(count($data) <= 0)	{
			echo(json_encode(array("status" => false,"error" => "Keine Daten übermittelt"))); die();

		}
		
		$id = $model->insert_client($data) ;		
		
		if($id == 0){
			echo(json_encode(array("status" => false,"error" => "Ein unbekannter Fehler ist aufgetreten"))); die();

		}
		
		$data["id"] = $id;												
		echo(json_encode(array("status" => true, "data" => $data)));
		die();
	}
	
	
	
	public function ajax_orderTable($data = null){
		
		global $config;
	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		$table = "order";
		$primaryKey = 'orderID';
		$columns = array(
		    array( 'db' => 'orderID', 'dt' => 0 ),
		    array( 'db' => 'kunde',   'dt' => 1  ),
		    array( 'db' => 'gp',   'dt' => 2  ), 
		   );
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		 $where = "status >= 0";
		echo json_encode(
		    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where )
		);
		

	}
	
	
	public function save(){
		
		$model = $this->loadModel('OrderModel');

		if(!$client = $model->get_client_by_id($_POST["o_kunde"])) throw new Exception ("Kunde existiert nicht.");

		$order = array();
		
		$id = false;
		if(array_key_exists("orderID", $_POST)) $id = $_POST["orderID"];
		

		
		$order["kunde"] = $client["company"];
		$order["gp"] = $client["gp"];
		$order["client_id"]  = $_POST["o_kunde"];
		$order["beraterID_1"] = $_POST["beraterID_1"];
		$order["beraterID_2"] = $_POST["beraterID_2"];
		$order["vermittler_alt"] = "";
		$order["status"] = "";
		$order["import"] = 0;
		$order["import_note"] = "";
		$order["SAP_jobnr"] = $_POST["SAP_jobnr"];
		$order["invoice_date"] = $_POST["invoice_date"];
		
		$positions = array();
		
		if(array_key_exists("positon",$_POST)){
		
			if(is_array($_POST["positon"])){
				foreach ($_POST["positon"] as $pos){
					
					$rubrik = $pos["o_rubrik"];
					if(strlen($pos["o_rubrik_sonstiges"]) > 0) $rubrik[] = $pos["o_rubrik_sonstiges"];

					$format = $pos["o_format"];
					if(strlen($pos["o_format_sonstiges"]) > 0) $format[] = $pos["o_format_sonstiges"];
					
					$posId = false;
					if(array_key_exists("id", $pos)) $posId = $pos["id"];

					
					$positions[] = array(
						"id" => $posId,
						"auftragsart" => $pos["o_auftrag_art"],
						"status" => $pos["o_status"],
						"von" => $pos["o_von"],
						"bis" => $pos["o_bis"],
						"gesamtpreis" => $pos["o_preis"],
						"rabatt" => $pos["o_rabatt"],
						"abrechnungsart" => $pos["o_berechnung_art"],
						"abrechnungsintervall" => $pos["o_berechnung_intervall"],
						"wiedervorlage" => $pos["o_berechnung_wiedervorlage"],
/*
						"SAP_BE" => $pos["o_sap_be"],
						"SAP_IKO" => $pos["o_sap_iko"],
						"SAP_FORMAT" => $pos["o_sap_format"],
*/
						"SAP_STICHWORT" => $pos["o_sap_stichzeile"],
// 						"SAP_JOBNR" => $pos["o_sap_jobnummer"],
						"bemerkung" => $pos["o_bemerkung"],
						
						"reichweite" => $pos["o_tkp"],
						"clickUrl" => $pos["o_url"],
						"portal" => $pos["o_portal"],
						"rubrik" => $rubrik,
						"format" => $format,						
						"werbemittel_anlieferung" => $pos["o_werbemittel_erstellung"],						
						
					);
				}				
				
			}	
			
		}
		
		$order["positions"] = $positions;
		
		if(!$id){
			// insert
			$id = $model->insertOrder($order);
			
		}else{
			//update
			$id = $model->updateOrder($id,$order);
		}
		
		 $this->redirect('order/edit/'.$id);
		


	}
	
}

?>
