<?php

class Error extends Controller {
	
	function index()
	{
		$this->error404();
	}
	
	function error404()
	{
		$template = $this->loadView('_error');
		$error .= '<h1>404 Error</h1>';
		$error .= '<p>Diese Seite existiert nicht!</p>';
		$template->set("error", $error);
		$template->render();
	}
	
	function permission()
	{
		global $config;
	
	
		$template = $this->loadView('_errorLoggedIn');
		
		if($this->isLogged() != "logged") $template = $this->loadView('_error');

		$error .= '<h1>Hoppla!</h1>';
		$error .= '<p>Hierzu reicht Ihre Berechtigung nicht aus!</p>';
		$error .= '<p><a href="'.$config['base_url'].'" class="btn btn-success">zur Startseite</a></p>';
		$template->set("error", $error);
		$template->render();
		
	}
    
}

?>
