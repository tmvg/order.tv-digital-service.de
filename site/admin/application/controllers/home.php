<?php

class Home extends Controller {
	
	function index()
	{
		if($this->isLogged() != "logged") $this->redirect('login');
		global $config;
		$template = $this->loadView('home_view');
		$template->render();
	}
	
    
}

?>
