<?php

class Login extends Controller {
	
	function index()
	{
		global $config;	
	
		$template = $this->loadView('login_view');
		$model = $this->loadModel('AdminLoginModel');
		$session = $this->loadHelper('session_helper');
		
		
		if($_POST) {
			$auth = $this->loadHelper("auth_helper");
			$login = $model->loginAdmin($_POST["email"], $auth->makePassword($_POST["password"]));

			if(count($login) == 0) {
				$template->info("Nutzername oder Passwort stimmt nicht!", "Achtung", "danger");
			} else {
				
				if($login[0]["userStatus"] == 0) {
					
					$template->info("Dieser Account ist inaktiv. Bitte wenden Sie sich an den Administrator.", "Achtung", "danger");
					
				} else {
					$session->set("login", true);
					$session->set("name", $login[0]["userFirstName"]." ".$login[0]["userLastName"]);
					$session->set("email", $login[0]["userEmail"]);
					$session->set("userID", $login[0]["userID"]);
					$session->set("userRole", $login[0]["userRole"]);
					$session->set("permission", unserialize($login[0]["permission"]));
					
					$this->redirect($config['default_controller']);
				}
			} 
			
			
		}
		
		$template->render();
		
	}
	
	function logout() {
		$session = $this->loadHelper('session_helper');
		$session->destroy();
		$this->redirect("login");
		
	}
    
}

?>
