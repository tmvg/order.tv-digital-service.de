<?php

class Media extends Controller {
	
	function __construct() {
		if($this->isLogged() != "logged") $this->redirect('login');
		//if(!$this->checkRole()) ;
		//$role = $this->getRole("role");
	}

	function index()
	{
		$template = $this->loadView('media/media_view');
		$model = $this->loadModel('MediaModel');
		$medias = $model->getAllMediaData("media");
		$template->set("medias", $medias);
		$template->render();
	}
	
	public function insertMedia() {
		
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
		
		$model = $this->loadModel('MediaModel');
		
		$path_parts = explode("/", $_POST['mediaPath']);
		$path = $path_parts[1];
		
		$data = array("mediaFile" => $_POST['mediaFile'], "mediaPath" => $path, "mediaType" => $_POST['mediaType'], "mediaVersion" => $_POST['mediaVersion']);
		
		$value = $model->insertData("media", $data);
		
		if($value){
			echo(json_encode(array("success" => true,"id" => $value)));
			exit;
		}
		echo(json_encode(array("success" => false)));
			
		
	}
	
    public function upload() {
   	    $template = $this->loadView('media/upload_view');
		$template->render();
    }
    
    
	function edit()
	{
		
		$model = $this->loadModel('MediaModel');

		if($_POST["delete"] && $_POST["delete"]  === "delete"){
			
			if($this->getRole("role") !== "admin") throw new Exception("Löschen nur für Admins");
			
			try{
				$model->deleteMedia($_REQUEST['id']);
				$this->redirect('media');

			}catch(Exception $e){
				echo($e->getMessage());
			}
			return false;
	
		}


		if(array_key_exists("id", $_POST)){
			// Update Image in DB
			$send = 0;
			if($_POST["send"] == "on") $send = 1;
			$model->updateMediaData($_POST["id"],array("send" => $send));
			$this->redirect('media');
			return false;

		}

				
		if($_REQUEST["id"]) {
			
			$template = $this->loadView('media/media_edit');

			$template->set("media",$model->getImageById($_REQUEST['id']));
			$template->set("used",$model->imageIsUsed($_REQUEST['id']));
			$template->set("role",$this->getRole("role"));
			
			
			$template->render();	
				
			} else {
				$this->redirect('media');
			}
			
			
			
	}

	public function ajax_mediaByID($id=null){
		global $config;

		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		$model = $this->loadModel('MediaModel');

		
		$image = $model->getImageById($_REQUEST["id"]);

		echo json_encode(array('status' => "true","image" => $image));							


	}

	
	public function ajax_MediaTable($data=null){
		
		global $config;
	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		$table = "media";
		$primaryKey = 'mediaID';
		$columns = array(
		    array( 'db' => 'mediaID', 'dt' => 0 ),
		  array( 'db' => 'mediaVersion',     'dt' => 1,
		        'formatter' => function( $d, $row ) {
			        $d = unserialize($d);
			        		global $config;

		            return "<img src='".$config["image_url"].$d["thumbnailUrl"]."' class='img-responsive' style='max-height:100px' >";
		        } 
		       ),   
		    array( 'db' => 'mediaFile',  'dt' => 2 ),
		    array( 'db' => 'mediaPath',   'dt' => 3 ),
		    array( 'db' => 'mediaVersion',     'dt' => 4,
		        'formatter' => function( $d, $row ) {
			        $d = unserialize($d);
			        			        		global $config;

		            return $config["image_url"].$d["thumbnailUrl"];
		        } 
		       ),
		     array( 'db' => 'mediaVersion',     'dt' => 5)
		   );
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		 
		echo json_encode(
		    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);

	} 
	
	
		public function ajax_MediaTablePublic($data=null){
		
		global $config;
	
		if($this->isLogged() != "logged"){
			header('content-type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		
		require( 'application/helpers/ssp.class.php' );
		$table = "media";
		$primaryKey = 'mediaID';
		$columns = array(
		    array( 'db' => 'mediaID', 'dt' => 0 ),
		  array( 'db' => 'mediaVersion',     'dt' => 1,
		        'formatter' => function( $d, $row ) {
			        $d = unserialize($d);
			        		global $config;

		            return "<img src='".$config["image_url"].$d["thumbnailUrl"]."' class='img-responsive' style='max-height:100px' >";
		        } 
		       ),   
		    array( 'db' => 'mediaFile',  'dt' => 2 ),
		    array( 'db' => 'mediaPath',   'dt' => 3 ),
		    array( 'db' => 'mediaVersion',     'dt' => 4,
		        'formatter' => function( $d, $row ) {
			        $d = unserialize($d);
			        			        		global $config;

		            return $config["image_url"].$d["thumbnailUrl"];
		        } 
		       ),
		     array( 'db' => 'mediaVersion',     'dt' => 5)
		   );
		
		// SQL server connection information
		$sql_details = array(
		    'user' => $config["db_username"],
		    'pass' => $config["db_password"],
		    'db'   => $config["db_name"],
		    'host' => $config["db_host"]
		);
		 
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		$where = "send = 1";
		 
		echo json_encode(
		    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where )
		);

	} 
	
	
}

?>
