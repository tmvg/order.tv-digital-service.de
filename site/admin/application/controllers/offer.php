<?php
	
	//Warengruppen

class offer extends Controller {
	
	var $user = false;
	
	function __construct(){
		if($this->isLogged() != "logged") $this->redirect('login');
		$user = $this->loadHelper('auth_helper');
		$this->user = $user->getUser();
		
		if($this->user["userRole"] == "crm"){
			$permission = unserialize($this->user["permission"]);
			if(!is_array($permission)) $this->redirect('error/permission');
			if(! in_array(strtolower(get_class()), $permission)) $this->redirect('error/permission');
			
		}
	}
	



	function index(){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$this->redirect('offer/edit');
		
		$template = $this->loadView('crm/offer_view');
		
		$data = array();

		$model = $this->loadModel('OfferModel');

		$template->set("data", $data);
		$template->set("crm_types", $model->getCRMType());
	
		$template->render();

		
		

	}
	
	function edit($id = false){
		global $config;
		
		if($this->isLogged() != "logged") $this->redirect('login');

		$template = $this->loadView('crm/offer_view');
		$model = $this->loadModel('OfferModel');

		$data = array();
		
		if($id) $data = $model->getOfferByID($id);
		
		$template->set("data", $data);
		$template->set("crm_types", $model->getCRMType());

		
		$template->render();

		
		

	}

	public function ajax_crm_status(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		$model = $this->loadModel('CrmModel');
		
		if(!is_null($_GET["id"])){
			$data = $model->get_crm_status_id($_GET["id"]);
		}else{
			$data = $model->query_crm_status($_GET["search"]);
		}
		
		
		$data = $model->query_crm_status();
		
		$result = new \stdClass;
		
		foreach ($data as $d){
			$result->result[] = (object)array("id" => $d["id"],"text" => ($d["status"]));
		}
				
		echo json_encode($result);							
		exit;
	}



	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

	    return "a_".$randomString;
	}
	
	
	public function ajax_add_client(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}

		$model = $this->loadModel('OrderModel');

		$pData = $_POST["data"];
			
		ob_start();
		var_dump($_POST["data"]);
		$return = ob_get_contents();
		ob_end_clean();
		
		$data = array();
		if(array_key_exists("company", $pData)) $data["company"] = $pData["company"];
		if(array_key_exists("firstname", $pData)) $data["firstname"] = $pData["firstname"];
		if(array_key_exists("lastname", $pData)) $data["lastname"] = $pData["lastname"];
		if(array_key_exists("salutation", $pData)) $data["salutation"] = $pData["salutation"];
		if(array_key_exists("zip", $pData)) $data["zip"] = $pData["zip"];
		if(array_key_exists("city", $pData)) $data["city"] = $pData["city"];
		if(array_key_exists("gp", $pData)) $data["gp"] = $pData["gp"];
		
		if(count($data) <= 0)	{
			echo(json_encode(array("status" => false,"error" => "Keine Daten übermittelt"))); die();

		}
		
		$id = $model->insert_client($data) ;		
		
		if($id == 0){
			echo(json_encode(array("status" => false,"error" => "Ein unbekannter Fehler ist aufgetreten"))); die();

		}
		
		$data["id"] = $id;												
		echo(json_encode(array("status" => true, "data" => $data)));
		die();
	}
	
	
	public function save($id = false){
		
		$model = $this->loadModel('OfferModel');

		$id = false;
		
		if(! array_key_exists("crm", $_POST)) throw new Exception("Keine Daten übermittelt");
		
		if(array_key_exists("id",  $_POST["crm"])) $id = $_POST["crm"]["id"];
		
		$offerData["company_id"] = $_POST["crm"]["client"];
		$offerData["status"] = $_POST["crm"]["status"];
		$offerData["crm_title"]  = $_POST["crm"]["crm_title"];
		$offerData["price"]  = $_POST["crm"]["price"];
		$offerData["crm_user"] = $this->user["userID"];
		$offerData["date"] = date("Y-m-d H:i:s",strtotime($_POST["crm"]["date"])); 
		$offerData["crm_type"]  = $_POST["crm"]["type"];
		$offerData["crm_final_date"] =date("Y-m-d H:i:s",strtotime($_POST["crm"]["final_date"])); 
		$offerData["project_id"] = $_POST["crm"]["project"];
		$offerData["note"] = $_POST["crm"]["note"];
		$offerData["crm_files"] = array();
		if(array_key_exists("media", $_POST)) $offerData["crm_files"]  = serialize($_POST["media"]);

		if(!$id){
 			$id = $model->insertOffer($offerData);
		}else{

			//update
			$model->updateOffer($id,$offerData);
		}
		
		$this->redirect('offer/edit/'.$id);

	}
	
	
	public function ajax_add_project(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}

		$model = $this->loadModel('OfferModel');

		$pData = $_POST["data"];
			
		ob_start();
		var_dump($_POST["data"]);
		$return = ob_get_contents();
		ob_end_clean();
		
		$data = array();
		if(array_key_exists("project_name", $pData)) $data["project_name"] = $pData["project_name"];
		
		if(count($data) <= 0)	{
			echo(json_encode(array("status" => false,"error" => "Keine Daten übermittelt"))); die();

		}
		
		$id = $model->insert_project($data) ;		
		
		if($id == 0){
			echo(json_encode(array("status" => false,"error" => "Ein unbekannter Fehler ist aufgetreten"))); die();

		}
		
		$data["id"] = $id;												
		echo(json_encode(array("status" => true, "data" => $data)));
		die();
	}
	
	public function ajax_find_project(){
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
	
		if($this->isLogged() != "logged"){
			echo json_encode(array('status' => "false","error" => "No Authentication"));						
			exit;	
		}
		
		$model = $this->loadModel('OfferModel');
		
		if(!is_null($_GET["search"])){
			$data = $model->query_project($_GET["search"]);
		}else{
			$data = $model->query_project();
		}
		
		$result = new \stdClass;
		if(is_array($data)){
			foreach ($data as $d){
				$result->result[] = (object)array("id" => $d["id"],"text" => $d["project_name"] );
			}
		}
		echo json_encode($result);							
		exit;
	}
	
	
	
}