<?php

class Setup extends Controller {
	
	function index()
	{
		$template = $this->loadView('setup_view');
		$model = $this->loadModel("SetupModel");

		if($_POST["setSetup"] == 1) {
			
			foreach($_POST as $key => $value) {
				$model->updateSetup(["value"=>$value], ["slug"=>$key]);
	
			}
			
			$this->writeSetupFile();
		}
				
		$setupData = $model->getSetupData();
		$template->set("setupData", $setupData); 
		$template->render();
		
	}
	
	
	public function writeSetupFile() {
		$model = $this->loadModel("SetupModel");
		$setupData = $model->getSetupData();
		global $config;
		
		$output = "";
		
		foreach($setupData as $set) {
			
			$output .= "\$setup['".$set['slug']."'] = '".$set['value']."';\n";
					
		}
		
		$output = "<?php ".$output." ?>";
		
		file_put_contents($config['setup_url'], $output);
	}
	

    
}

?>
