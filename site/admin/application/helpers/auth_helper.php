<?php

class Auth_helper extends Model {

	public function makePassword($pass) {
		return md5(md5($config["salt"].$pass));
	}
	
	public function isLogged() {
		if($_SESSION["login"] == "login") {
			return true;
		} else {
			return false;
		}
	}
	
	public function getRole() {
		global $database;
		$role = $database->select("admin", "userRole", array("userID" => $_SESSION["userID"]));
		return $role[0];
	}
	
	public function getUser(){
		global $database;
		return $database->get("admin", "*", array("userID" => $_SESSION["userID"]));
	}


}

?>