<?php



include(APP_DIR .'config/config.php');	
include(APP_DIR .'plugins/PHPMailer/PHPMailerAutoload.php');

define("MAIL_REPLY", $config['mail_reply']);
define("MAIL_NAME", $config['mail_name']);

/*
define("COOKIE_RUNTIME", 1209600);
define("COOKIE_DOMAIN", $config['domain']);
define("COOKIE_SECRET_KEY", "1gp@TMPS{+$78sfpMJFe-92s");
*/
define("EMAIL_USE_SMTP", $config['mail_use_smtp']);
define("EMAIL_SMTP_HOST", $config['mail_host']);
define("EMAIL_SMTP_AUTH", $config['mail_smtp_auth']);
define("EMAIL_SMTP_USERNAME", $config['mail_smtp_username']);
define("EMAIL_SMTP_PASSWORD", $config['mail_smtp_password']);
define("EMAIL_SMTP_ENCRYPTION", $config['mail_smtp_encryption']);
define("EMAIL_SMTP_PORT", $config['mail_smtp_port']);

define("EMAIL_TEMPLATE_HEADER", $config['mail_template_header']);
define("EMAIL_TEMPLATE_FOOTER", $config['mail_template_footer']);
define("EMAIL_TEMPLATE_PATH", $config['mail_template_path']);



class Mail_helper extends PHPMailer {


	function __construct($exceptions = false){
		parent::__construct();
	
		
		if(! EMAIL_USE_SMTP){
			$this->IsSendmail(); 								// set Mailer to Sendmail
		}else{
			$this->isSMTP(); 									// set Mailer to SMTP  
			$this->Host = EMAIL_SMTP_HOST;  				  	// Specify main and backup SMTP servers
			$this->SMTPSecure = EMAIL_SMTP_ENCRYPTION;          // Enable TLS encryption, `ssl` also accepted
			$this->Mailer = "smtp"; 
			$this->SMTPKeepAlive = true;  
	
			if(EMAIL_SMTP_AUTH) $this->SMTPAuth = true;        // Enable SMTP authentication
			$this->Username = EMAIL_SMTP_USERNAME;             // SMTP username
			$this->Password = EMAIL_SMTP_PASSWORD;             // SMTP password
			$this->Port = EMAIL_SMTP_PORT;                     // TCP port to connect to	                                 
		}
		$this->From = MAIL_REPLY;
		$this->FromName = MAIL_NAME;

		$this->AddReplyTo(MAIL_REPLY, MAIL_NAME);
		$this->CharSet = 'UTF-8';
		$this->isHTML(true);  

		/*
		$this->database = new database([
			'database_type' => 'mysql',
			'database_name' => DB_NAME,
			'server' => DB_HOST,
			'username' => DB_USER,
			'password' => DB_PASS,
		]); 
		*/
	}
	
	function sendMail($sendto, $firstname, $lastname,  $subject, $mailcontent, $attachment = NULL) {
		
		$this->clearAttachments();
		$this->MessageID = "<".md5(time().'_'.$mailto).COOKIE_DOMAIN;
		$this->AddAddress($sendto, "$firstname $lastname");
		
		$htmltext = $mailcontent;
		
		$this->Subject = $subject;
		$this->Body    = trim( ($htmltext));
		$this->AltBody  = strip_tags($htmltext);

		//if($attachment) $this->AddStringAttachment($attachment[0], $attachment[1], $attachment[2], $attachment[3]);
		
		ob_start();
		if(! $this->send()) {
		
			$this->clearAllRecipients();
			$this->clearAttachments();

			$mailContent = ob_get_contents();

			ob_end_clean();	
			//$this->saveMail($mailto,$mailtoname,$subject,$htmltext,array($this->ErrorInfo,$inhalte));

			return $this->ErrorInfo;
		} else {
			$this->copyToFolder("Sent"); // Will save into Sent folder

			$mailContent = ob_get_contents();

		ob_end_clean();	

			//$this->saveMail($mailto,$mailtoname,$subject,$htmltext,$inhalte);
			$this->clearAllRecipients();
			$this->clearAttachments();
			

			return "Mail wurde an $mailto versendet.";
		}

		
	}
	
	
	public function copyToFolder($folderPath = null) {
        $message = $this->MIMEHeader . $this->MIMEBody;
        $path = "INBOX" . (isset($folderPath) && !is_null($folderPath) ? ".".$folderPath : ""); // Location to save the email
        $imapStream = imap_open("{" . $this->Host . "}" . $path , $this->Username, $this->Password);
        imap_append($imapStream, "{" . $this->Host . "}" . $path, $message);
        imap_close($imapStream);
    }


/*

	function sanitize_output($buffer) {
	
		$search = array(
				'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
				'/[^\S ]+\</s',  // strip whitespaces before tags, except space
				'/(\s)+/s'       // shorten multiple whitespace sequences
			);
		
		$replace = array(
				'>',
				'<',
				'\\1'
			);
		
		$buffer = preg_replace($search, $replace, $buffer);
		
		return $buffer;
	}

	
	function buildMail($data){
		require_once(__DIR__."/inliner/Emogrifier.php");	

		$emogrifier = new \Pelago\Emogrifier();

		require_once 'mail_templates/class/Smarty.class.php';
		
		
		$smarty = new Smarty;
		$smarty->setTemplateDir(__DIR__.'/mail_templates/templates/');
		$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].'/data/cache/templates/mail_templates_c');
		$smarty->setCacheDir(__DIR__.'/mail_templates/cache');
		$smarty->setConfigDir(__DIR__.'/mail_templates/configs');
		
		if(!is_array($data)) throw new Exception ("Mail Data is now Array");
		
		if(!array_key_exists("template", $data)) throw new Exception ("No Mail Template definde");
		
		if(! file_exists(__DIR__.'/mail_templates/templates/'.$data["template"].'.tpl')) throw new Exception ("Definiertes Template existiert nicht");

		$template = $data["template"];
		unset($data["template"]);
		
		$smarty->debugging = false;
		$smarty->caching = false;
		
		foreach($data as $key => $value){
			$smarty->assign($key, $value);
		}
		
		// Assign Static Values START
		$smarty->assign("header_background_color",MAIL_HEADER_BG);
		$smarty->assign("header_font_color",MAIL_HEADER_COLOR);
		$smarty->assign("header_button_background_color",MAIL_BUTTON_BG);
		$smarty->assign("header_button_color",MAIL_BUTTON_COL);
		$smarty->assign("global_domain",DOMAIN);

		$smarty->assign("header_logo_url",DOMAIN."/".LOGO);

		$smarty->assign("footer_mail_inpressum",PARTNER."<br>".PARTNER_ADRESS."<br>E-Mail: ".PARTNER_EMAIL." | Telefon: ".PARTNER_TELEFON);
		$smarty->assign("footer_PARTNER_DISCLAIMER",PARTNER_DISCLAIMER);
		// Assign Static Values END


		
		$html = $smarty->fetch($template.'.tpl');
		$css = file_get_contents($_SERVER["DOCUMENT_ROOT"].'/classes/mail_templates/style.css');

		$emogrifier->setHtml($html);
		$emogrifier->setCss($css);
		$mergedHtml = $this->sanitize_output($emogrifier->emogrify());

		
		return $mergedHtml;
	}
	
	
	//function sendMail($mailto, $mailtoname, $subject, $htmltext) {
	function sendMail($mailto, $mailtoname, $subject, $mailData,$attachment = NULL,$attachment2 = NULL){
		global $_MAIL_HEADER;
		global $_MAIL_FOOTER;
		global $_MAIL_BUTTON_BG;
		global $_MAIL_BUTTON_COL;

		$this->clearAttachments();

		$this->MessageID = "<".md5(time().'_'.$mailto).'@auktion.volksfreund.de>';

		$this->AddAddress($mailto, $mailtoname);
	
		$htmltext = $this->buildMail($mailData);
		
		
		$this->Subject = $subject;
		$this->Body    = trim( ($htmltext));
		

		$this->AltBody  = strip_tags($htmltext);
		//$mail->AltBody = $plaintext

	
		//$this->SMTPDebug = 2; 

		if($attachment) $this->AddStringAttachment($attachment[0], $attachment[1], $attachment[2], $attachment[3]);
		if($attachment2) $this->AddStringAttachment($attachment2[0], $attachment2[1], $attachment2[2], $attachment2[3]);
		
		ob_start();
		if(! $this->send()) {
		
			$this->clearAllRecipients();
			$this->clearAttachments();

			$inhalte = ob_get_contents();

			ob_end_clean();	
			$this->saveMail($mailto,$mailtoname,$subject,$htmltext,array($this->ErrorInfo,$inhalte));

			return $this->ErrorInfo;
		} else {
			
			$inhalte = ob_get_contents();

		ob_end_clean();	

			$this->saveMail($mailto,$mailtoname,$subject,$htmltext,$inhalte);
			$this->clearAllRecipients();
			$this->clearAttachments();

			return true;
		}

		
	}
	
	
	function saveMail($email,$recipient,$subject,$content,$error){
		$this->database->insert("mail_log",[
			date => time(), 
			email => $email, 
			recipient => $recipient, 
			subject => $subject, 
			content => $content, 
			error => serialize($error)
			]);	

	}
	*/
	
}

$mail = new Mail_helper();


