<?php

class AdminModel extends Model {
	
	
	
    public function insertAdmin($data)
    {
	    global $database;
	    $insert = $database->insert("admin", $data);
	    if($insert > 0) return $insert; else return $false;	    
    }
    
    public function updateAdmin($data, $where)
    {
	    global $database;
	    $insert = $database->update("admin", $data, $where);
	    if($insert > 0) return true; else return false;	    
    }    
    

	public function checkAdminByEmail($email) {
		global $database;
		$checkMail = $database->select("admin", "*", array("userEmail" => $email));
	    
	    if(count($checkMail) > 0) {
		    return false;
   	    } else {
	   	    return true;
   	    }
	}

	public function getAllAdmins()
    {
		global $database;
		$result = $database->select("admin", "*");
        return $result;
    }   
    
	public function getAdminById($id)
    {
		global $database;
		$result = $database->select("admin", "*", array("userID" => $id));
        return $result[0];
    }      


}

?>