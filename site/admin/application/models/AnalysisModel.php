<?php

class AnalysisModel extends Model {

    
	public function getAnalysis($id){
	
		global $database;
		
		$analyse = array();
		
		$analyse["data"] = $database->select("votes","*",array("voting" => $id));
		$analyse["valid"] = $database->select("votes","*",array("AND" => array("voting" => $id,"validated" => 1)));
		
		$analyse["count"]["votes"] = count($analyse["data"]);
		$analyse["count"]["valid"] = 0;
		$analyse["history"] = array();
		$analyse["results"] = array();
		
		if(is_array($analyse["data"])){
		
			foreach($analyse["data"] as $key => $value){
				
				
				
				
				if($value["validated"] > 0){
					$analyse["count"]["valid"] ++;

					if(array_key_exists($value["element"], $analyse["results"])) $analyse["results"][$value["element"]] = $analyse["results"][$value["element"]] + 1 ;
					else $analyse["results"][$value["element"]] = 1;

				}
				
				if($value["validateTime"] > 0){
					$day = strtotime(date("d.m.Y",$value["validateTime"]));

					if(!array_key_exists($day, $analyse["history"])) $analyse["history"][$day] = 0;

					$analyse["history"][$day] ++;
				
				}
				
				
				$meta = $database->select("votes_meta","*",array("votes_id" => $value["id"]));
				foreach($meta as $metaValue){
					$analyse["data"][$key]["meta"][$metaValue["votes_key"]] =  unserialize($metaValue["votes_value"]);
				}
				
			}
		}
			
		return $analyse;
	}

	public function getVotingById($id){
		global $database;
		
		$data = $database->get("voting","*",array("id" => $id));
		
		if($data){
			$data["element"] = $database->select("voting_elements","*",array("voting_id" => $id));

		}
		return($data);
		
	}    
}

?>