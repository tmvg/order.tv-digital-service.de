<?php

class BilderstreckeModel extends Model {

    

	
	public function insertBilderstrecke($data)
    {
	    global $database;

	    $elements = $data["element"];
		unset($data["element"]);

		
		
		$data["created"] = time();
		$data["updated"] = time();
		$data["type"] = "bilderstrecke";
		$data["uuid"] = $this->uuid();
		
	    $id = $database->insert("bilderstrecke",$data);
	    

	    if(is_array($elements)){
		    foreach($elements as $element){
			    
			    $el["voting_id"] = $id;
			    
			    $el["rechteinhaber"] = $element["rechteinhaber"];
			    $el["fotograf"] = $element["fotograf"];
			    $el["zeitung"] = $element["zeitung"];
			    $el["fotograf"] = $element["fotograf"];
			    $el["ueberschrift"] = $element["ueberschrift"];
			    $el["personenfreigabe"] = $element["personenfreigabe"];
			    $el["bildtext"] = $element["bildtext"];
			    $el["honorar"] = $element["honorar"];
			    
			    
			    $el["image"] = $element["image"];

			   if(array_key_exists("mdbUpload", $element)) $el["mdbUpload"] = $element["mdbUpload"];  
			    
			    $this->insertVotingElement($el);
		    }
	    }
	    
	    return $id;
	    
    }
    
    public function uuid(){
	    global $database;
	    
	    $uuid = $database->query("SELECT UUID_SHORT()")->fetchAll();
		
		return $uuid[0][0];	
	    
    }
	

	public function insertVotingElement($data){
	    global $database;

		$data["uuid"] = $this->uuid();

	    $id = $database->insert("bilderstrecke_element",$data);

	}
	
	public function updateVotingElement($data,$id){
	    global $database;
	    
	    $id = $database->update("bilderstrecke_element",$data,array("id" => $id));

	}
		
	public function updateVoting($id,$data)
    {
	    global $database;
	    
		$oldElements = $database->select("bilderstrecke_element","*",array("voting_id" => $id));
		
		$oldElementsIds = array();

	    foreach($oldElements as $oldElement){
		    
		    $oldElementsIds[] = $oldElement["id"];
	    }
	    
	
	    
	    $elements = $data["element"];
		unset($data["element"]);
		
		$data["updated"] = time();

		
		$database->update("bilderstrecke",$data,array("id" => $id));
		
	    
	    if(is_array($elements)){
		    foreach($elements as $element){
			    
			    $el["voting_id"] = $id;
			    
			    $el["rechteinhaber"] = $element["rechteinhaber"];
			    $el["fotograf"] = $element["fotograf"];
			    $el["zeitung"] = $element["zeitung"];
			    $el["fotograf"] = $element["fotograf"];
			    $el["ueberschrift"] = $element["ueberschrift"];
			    $el["personenfreigabe"] = $element["personenfreigabe"];
			    $el["bildtext"] = $element["bildtext"];
				$el["honorar"] = $element["honorar"];
 
			    
			    $el["image"] = $element["image"];


			   if(array_key_exists("mdbUpload", $element)) $el["mdbUpload"] = $element["mdbUpload"];

				    
			    if( in_array($element["id"], $oldElementsIds) ){
				    // Element muss upgedated werden

					$this->updateVotingElement($el,$element["id"]);

				    
					if (($key = array_search($element["id"], $oldElementsIds)) !== false) {
					    unset($oldElementsIds[$key]);
					}

			    }else{    
				    $this->insertVotingElement($el);
				}
		    }
		    
		    if(count($oldElementsIds) > 0){
			    foreach($oldElementsIds as $ol){
		    		$database->delete("bilderstrecke_element",array("id" => $ol));
			    }

		    }
		    
	    }

		return $id;

    }
    
    
	public function getVotingById($id){
		global $database;
		
		$data = $database->get("bilderstrecke","*",array("id" => $id));
		
		if($data){
			$data["element"] = $database->select("bilderstrecke_element","*",array("voting_id" => $id));
			foreach($data["element"] as $key => $element){
			 $data["element"][$key]["image_data"] = $this->getImageById($element["image"]);	
			}

		}
		return($data);
		
	}    

	public function getVotingByPermalink($permalink){
		global $database;
		
		$data = $database->get("bilderstrecke","*",array("permalink" => $permalink));
		
		if($data){
			$data["element"] = $database->select("bilderstrecke_element","*",array("voting_id" => $id));

		}

		return($data);
		
	}
	
	public function getIRRessorts($clickme = 0){
		global $database;

		if($clickme == 0) 		return $database->select("irRessorts","*");

		
		return $database->select("irRessorts","*",array("clickMe" => 1));
		
	}

	public function produce($id){
		global $database;
		
		$aktuellerStatus = $database->get("bilderstrecke","prozessing",array("id" => $id));

		if($aktuellerStatus === false) throw new Exception("Bilderstrecke nicht bekannt");
		if($aktuellerStatus >= 0)  throw new Exception("Bilderstrecke schon in Produktion");
		
		
		$database->update("bilderstrecke",array("prozessing" => 0),array("id" => $id));
		
		return true;
		
		
	}

	public function getImageById($id)
    {
	    global $database,$config;
	   
        $result = $database->get("media", "*", array("mediaID" => $id));
        
        if(! $result) return false;
        else $result["mediaVersion"] = unserialize($result["mediaVersion"]);
        
        $result["mediaVersion"]["mediumUrl"] =  $config["image_url"]  .      $result["mediaVersion"]["mediumUrl"];

        $result["mediaVersion"]["minithumbUrl"] =  $config["image_url"] .       $result["mediaVersion"]["minithumbUrl"] ;

        $result["mediaVersion"]["thumbnailUrl"] =  $config["image_url"] .      $result["mediaVersion"]["thumbnailUrl"];

        $result["mediaVersion"]["url"] =     $config["image_url"].    $result["mediaVersion"]["url"];

         
        
        
        
        return $result;
    }  

}

?>