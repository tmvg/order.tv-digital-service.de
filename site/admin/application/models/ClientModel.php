<?php

class ClientModel extends Model {
	
	// Additive
	public function insertClient($data)
    {
	    global $database;
	    
	    if($id = $database->insert("client",$data)) return $id;
	    return false;

    }
    
	public function updateClient($id,$data)
    {
	    global $database;

		if($database->update("client",$data,array("id" => $id))) return $id;
		
		return false;
    }
    
    
    public function getClientByClientId($client_id){
	    global $database;
	    
		if($client = $database->get("client","*",array("id" => $client_id))){
			
			return $client;
		}
		return false;
	    
    }
    
    public function getClientById($id){
	    global $database;
	    
		if($client = $database->get("client","*",array("id" => $id))){
			$client["contact"] = $database->select("contact","*",array("client_id" => $id));

			return $client;
		}
		return false;
	    
    }
    
    
    public function getClients(){
	    global $database;
	    
	    if($clients = $database->select("client","*")){
		    
		    foreach($clients as $key =>  $client){
				$clients[$key]["contact"] = $database->select("contact","*",array("client_id" => $client["id"]));
		    }
		    
		}
		
		return $clients;
	    
	}    
	
	
	public function deleteClient($id){
		global $database;
		
		if(! $database->get("client","*",array("id" => $id))) throw new Exception ("Client $id not found");
		
		$database->delete("client",array("id" => $id));
		$database->delete("contact",array("client_id" => $id));
		
	}
	
	public function getContactsByClientId($id){
		global $database;
		
		$contacts = $database->select("contact","*",array("client_id" => $id));

		return $contacts;
		
	}
	
    
/*
    public function updateData($table, $data, $where)
    {
	    global $database;
	    $update = $database->update($table, $data, $where);
	    if($update > 0) return true; else return false;	    
    }      
	
	public function getAllData($table)
    {
        global $database;
        return $database->select($table, "*");
    } 	
    
    public function getAllMediaData()
    {
        global $database;
        $medias = $database->query("SELECT * FROM media ORDER BY mediaID DESC");
        return $medias;
    } 	
            


	public function getDataById($table, $where)
    {
	    global $database;
        $result = $database->select($table, "*", $where);
        return $result[0];
    }  
*/
 


}

?>