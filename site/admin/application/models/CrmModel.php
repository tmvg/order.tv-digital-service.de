<?php

class CrmModel extends Model {

    
	public function getOrderById ($id){
	    global $database;
	    

		$order = $database->get("order","*",array("id" => $id));	
		
		if($order){
			$order["product"] = $database->select("order_has_product",array("[>]product" => array("product_id"=>"id") ), "*",array("order_has_product.order_id" => $id));
			$order["client"] = $database->get("client","*",array("id" =>$order["client_id"] ));
			$order["contact"] = $database->get("contact","*",array("id" =>$order["contact_id"] ));
		}
		
		
		return $order;	
	}
	
	
	public function insertOrder($data)
    {
	    global $database;
		  
		$products = $data["products"];
		unset($data["products"]);
		
	    $id = $database->insert("order",$data);
	    
	    foreach($products as $product){
			$product["order_id"] = $id;
			$database->insert("order_has_product",$product);

	    }
	    

	    
		return $id; 
    }
	
	
	public function updateProduct($id,$data)
    {
	    global $database;

		$database->update("product",$data,array("id" => $id));
		return $id;
    }
    
    

    
 
    public function get_crm_status_id($status){
	    global $database;

		$data =   $database->select("crm_status", "*", array("status" => $status));
		return  $data;
    }


    public function query_crm_status($data = ""){
	    global $database;

		$data =   $database->select("crm_status", "*", [
			"OR" => [
				"id[~]" => $data,
				"status[~]" => $data
			],

		]);
		
		$data =   $database->select("crm_status", "*");
		
		return  $data;
    }
    
    
    public function insert_client($data){
	    global $database;
	    
	    $id = $database->insert("client",$data);

	    return $id;
	    
    }
    
	public function get_client_by_id($id){
	    global $database;

		$data =   $database->select("client", "*", array("id" => $id));
		return  $data;
    }


    public function query_client($data){
	    global $database;

		$data =   $database->select("client", "*", [
			"OR" => [
				"firstname[~]" => $data,
				"lastname[~]" => $data,
				"company[~]" => $data,
				"gp[~]" => $data,
			],

		]);
		return  $data;
    }
    
    
    
    
}

?>