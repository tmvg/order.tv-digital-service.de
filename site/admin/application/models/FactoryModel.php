<?php

class FactoryModel extends Model {

    

	
    
    
	public function getMedia($type = false ){
		global $database;
		
		if(!$type) throw new Exception("Media Type not defined");
		
		$bilderstrecken = $database->select("bilderstrecke","*",array("AND" => array("prozessing" => 0,"type" => $type)));
		
		if($bilderstrecken){
			
			
			foreach($bilderstrecken as $keyBilderStrecke => $data){
			
			$bilderstrecken[$keyBilderStrecke]["user"] = $database->get("admin","*",array("userID" => $data["user"]));
			
				$elements = $database->select("bilderstrecke_element","*",array("voting_id" => $data["id"]));
				
				foreach($elements as $key => $value){
					
					$image = $database->get("media","*",array("mediaID" => $value["image"]));
					
					if($image){
						$image["mediaVersion"] = unserialize($image["mediaVersion"]);
						$image["iptc"] = unserialize($image["iptc"]);
					}
					
					$elements[$key]["image"] = $image;
				}
				
				$bilderstrecken[$keyBilderStrecke]["element"] = $elements;
			}

		}
		return($bilderstrecken);
		
	}    

	public function setWorkDone($id){
		global $database;
		
		$database->update("bilderstrecke",array("prozessing" => 1),array("id" => $id));
	}

}

?>