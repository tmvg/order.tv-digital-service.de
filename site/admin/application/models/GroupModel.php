<?php

class GroupModel extends Model {

    
    public function getGroups($status = 1){
	    global $database;
	    
	    $groups = $database->select("group","*",array("status" => $status));
	
		
		return $groups;
	    
	}  
	
	public function getGroupById($id){
		global $database;
	    
		if($group = $database->get("group","*",array("id" => $id))){
			if($group["parent"]){ $group["parent_name"] = $database->get("group","name",array("id" => $group["parent"])); }
			return $group;
		}
		return false;

	}  
	
	
	public function insertGroup($data)
    {
	    global $database;
	    
		if($clientIdTest = $this->getGroupByGroupIdAndParent($data["group_id"],$data["parent"])) throw new Exception ("Kundennummer schon vorhanden");  
		  
	    $id = $database->insert("group",$data);
		return $id; 
    }
	
	
	public function updateGroup($id,$data)
    {
	    global $database;

		if($clientIdTest = $this->getGroupByGroupIdAndParent($data["group_id"],$data["parent"])){
			if($clientIdTest["id"] != $id) throw new Exception ("Kundennummer schon vorhanden");
		}

		$database->update("group",$data,array("id" => $id));
		return $id;
    }
    
    
    
	public function getGroupByGroupId($group_id){
	    global $database;
	    
		if($client = $database->get("group","*",array("group_id" => $group_id))){
			return $client;
		}
		return false;
	    
    }
    
    	public function getGroupByGroupIdAndParent($group_id,$parent = null){
	    global $database;
	    
	
		if($client = $database->get("group","*",array("AND" => array("group_id" => $group_id,"parent" => $parent)))){
			file_put_contents("sql.txt",$database->last_query());
			return $client;
		}
			file_put_contents("sql.txt",$database->last_query());
		

		
		return false;
	    
    }
 
	public function deleteGroup($id){
		global $database;
		
		if(! $database->get("group","*",array("id" => $id))) throw new Exception ("Groupe $id not found");
			$database->update("group",array("status" => 0),array("id" => $id));
	}

	
	
	public function getTree($parent = NULL,$levels = NULL,$path = array(),$status = 1){
		global $database;
		
		
		$levels --;
		
		if($parent === null){
			$nodes = $database->select("group",array("id","name(text)","group_id"),array("AND" => array("parent" => NULL,"status" => $status)));
		
		}else{
			$nodes = $database->select("group",array("id","name(text)","group_id"),array("AND" => array("parent" => $parent,"status" => $status)));
		}
		if($levels !== NULL) $levels --;
		

		foreach ($nodes as $key => $node){
			
			$tmpPath = $path;
			$tmpPath[] = $node["group_id"];
			
			$nodes[$key]["tags"]  =  array(implode("-",$tmpPath));
			
			if($levels >= 0 OR $levels === NULL){
				$childNodes = $this->getTree($node["id"],$levels,$tmpPath,$status);
				if($childNodes !== null) $nodes[$key]["nodes"]  = $childNodes;
			}

		}
	
		if(count ($nodes) > 0) return $nodes;

	}


	public function getTreePart($parent = NULL){
		global $database;
		
		if($parent === null){
			$nodes = $database->select("group",array("id","name(text)"),array("parent" => NULL));
		
		}else{
			$nodes = $database->select("group",array("id","name(text)"),array("parent" => $parent));
		}
		foreach ($nodes as $key => $node){
			
			$childNodes = $database->select("group",array("id","name(text)"),array("parent" => $node["id"]));
			
			if(count($childNodes) > 0) $nodes[$key]["nodes"] = $childNodes;


		}
		if(count ($nodes) > 0) return $nodes;

	}

	public function getGroupAncestors($id){
		global $database;
		
		$ancestors = $database->query("SELECT T2.* FROM (SELECT @r AS _id,(SELECT @r := parent FROM `group` WHERE id = _id) AS parent FROM (SELECT @r := $id) vars, `group` m WHERE @r <> 0) T1 JOIN `group` T2 ON T1.parent = T2.id;")->fetchAll();
		array_reverse($ancestors);
		return $ancestors;
		
		
	}

}

?>