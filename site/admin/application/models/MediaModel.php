<?php

class MediaModel extends Model {
	
	// Additive
	public function insertData($table, $data)
    {
	    global $database;
	    $last_id = $database->insert($table, $data);
	    if($last_id > 0) return $last_id; else return false;
    }
    
    public function updateData($table, $data, $where)
    {
	    global $database;
	    $update = $database->update($table, $data, $where);
	    if($update > 0) return true; else return false;	    
    }      
	
	public function getAllData($table)
    {
        global $database;
        return $database->select($table, "*");
    } 	
    
    public function getAllMediaData()
    {
        global $database;
        $medias = $database->query("SELECT * FROM media ORDER BY mediaID DESC");
        return $medias;
    } 	
            


	public function getImageById($id)
    {
	    global $database,$config;
	   
        $result = $database->get("media", "*", array("mediaID" => $id));
        
        if(! $result) return false;
        else $result["mediaVersion"] = unserialize($result["mediaVersion"]);
        
        $result["mediaVersion"]["mediumUrl"] =  $config["image_url"]  .      $result["mediaVersion"]["mediumUrl"];

        $result["mediaVersion"]["minithumbUrl"] =  $config["image_url"] .       $result["mediaVersion"]["minithumbUrl"] ;

        $result["mediaVersion"]["thumbnailUrl"] =  $config["image_url"] .      $result["mediaVersion"]["thumbnailUrl"];

        $result["mediaVersion"]["url"] =     $config["image_url"].    $result["mediaVersion"]["url"];
        
        return $result;
    }  
	
	public function updateMediaData($id,$data){
		global $database;
		
		$database->update("media",$data,array("mediaID" => $id));
	}

	
	
	public function imageIsUsed($id){
		global $database;
		
 		$data["productBild"] = array();
		$data["produktMedia"] = array();
		$data["cmsMedia"] = array();		
		$data["produktGallerie"] = array();	
 		
		$productBild = $database->select("product","*",array("image" => $id));
		$produktMedia = $database->select("product","*",array("media" => $id));
		$cmsMedia = $database->select("cms","*",array("media" => $id));
		$produktGallerie =  $database->select("product", "*", ["gallery[~]" => '"'.$id.'"']);
		

		
		
		if((count($productBild) + count($produktMedia) + count($produktGallerie) +count($cmsMedia)) == 0) return false;
		
		$data["productBild"] = $productBild;
		$data["produktMedia"] = $produktMedia;
		$data["cmsMedia"] = $cmsMedia;		
		$data["produktGallerie"] = $produktGallerie;		
	
		return $data;
		
	}
	
	
	public function deleteMedia($id){
			
			global $database,$config;

			if($this->imageIsUsed($_REQUEST['id'])) throw new Exception("Bild noch in verwendung");
			
			$image = $this->getImageById($_REQUEST['id']);
			
			if(! array_key_exists("deleteUrl", $image["mediaVersion"])) throw new Exception("Delete URL FEHLT");
			
			$url = $config["domain"]."/admin/static/assets/file-upload/index.php?file=".$image["mediaFile"]."&path=".$image["mediaPath"];
			
			
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $result = curl_exec($ch);
		    $result = json_decode($result);
		    curl_close($ch);
					    
		    if($result->$image["mediaFile"]){
			    $database->delete("media",array("mediaID" => $id));
		    }else{
			    
			    if(!$result->file_path_exits) throw new Exception("Originaldatei nicht gefunden. Konnte Bild nicht löschen.");

			    
			    throw new Exception("Datei konnte nicht gelöscht werden");
		    }
					
		
	}

	
}

?>