<?php

class OfferModel extends Model {


    public function insert_project($data){
	    global $database;
	    
	    $id = $database->insert("crm_project",$data);

	    return $id;
	    
    }


    public function getKPI($status,$start = false, $end = false) {
	    global $database;
	    
	    if($start && $end){
			$data = $database->query("SELECT count(o.id) AS anzahl FROM crm_offer o LEFT JOIN crm_status s ON s.id = o.status WHERE s.status_value = '$status' AND o.date BETWEEN '$start' AND '$end'" )->fetchAll();

	    }else{
		    $data = $database->query("SELECT count(o.id) AS anzahl FROM crm_offer o LEFT JOIN crm_status s ON s.id = o.status WHERE s.status_value = '$status'" )->fetchAll();
	    }	    
		return $data[0][anzahl];
    }

	public function query_project($data = null){
	    global $database;
		
		if(is_null($data)){
					$data =   $database->select("crm_project", "*",["LIMIT" => 10]);
		return  $data;

		}

		$data =   $database->select("client", "*", [
			"OR" => [
				"project_name[~]" => $data,
			],

		]);
		return  $data;
    }
    
 
	public function insertOffer($data){
		global $database;
		
		
		if(array_key_exists("company_id", $data)) $crm_offer["company_id"] = $data["company_id"]; 
		if(array_key_exists("status", $data)) $crm_offer["status"] = $data["status"] ;
		if(array_key_exists("date", $data)) $crm_offer["date"] = $data["date"] ;
		if(array_key_exists("price", $data)) $crm_offer["price"] = $data["price"]; 
		if(array_key_exists("crm_title", $data)) $crm_offer["crm_title"] = $data["crm_title"]; 
		if(array_key_exists("crm_user", $data)) $crm_offer["crm_user"] = $data["crm_user"] ;
		if(array_key_exists("crm_type", $data)) $crm_offer["crm_type"] = $data["crm_type"] ;
		if(array_key_exists("crm_final_date", $data)) $crm_offer["crm_final_date"] = $data["crm_final_date"] ;
		if(array_key_exists("project_id", $data)) $crm_offer["project_id"] = $data["project_id"] ;
		if(array_key_exists("crm_type", $data)) $crm_offer["crm_type"] = $data["crm_type"] ;
		if(array_key_exists("crm_files", $data)) $crm_offer["crm_files"] = $data["crm_files"] ; 


		$crm_offer_id = $database->insert("crm_offer",$crm_offer);
		
		if(strlen($data["note"]) > 0){
			$crm_activity = array();		
			$crm_activity["crm_offerID"] = $crm_offer_id ;
			if(array_key_exists("note", $data)) $crm_activity["note"] = $data["note"] ; 
			$crm_activity["date"] = date("Y-m-d H:i:s");
			if(array_key_exists("crm_user", $data)) $crm_activity["crm_user"] = $data["crm_user"] ;
			$crm_activity_id = $database->insert("crm_activity",$crm_activity);
		}
		return $crm_offer_id;

	}
       
	public function updateOffer($id,$data){
		global $database;
		
		
		if(array_key_exists("company_id", $data)) $crm_offer["company_id"] = $data["company_id"]; 
		if(array_key_exists("status", $data)) $crm_offer["status"] = $data["status"] ;
		if(array_key_exists("date", $data)) $crm_offer["date"] = $data["date"] ;
		if(array_key_exists("price", $data)) $crm_offer["price"] = $data["price"]; 
		if(array_key_exists("crm_user", $data)) $crm_offer["crm_user"] = $data["crm_user"] ;
		if(array_key_exists("crm_title", $data)) $crm_offer["crm_title"] = $data["crm_title"]; 
		if(array_key_exists("crm_type", $data)) $crm_offer["crm_type"] = $data["crm_type"] ;
		if(array_key_exists("crm_final_date", $data)) $crm_offer["crm_final_date"] = $data["crm_final_date"] ;
		if(array_key_exists("project_id", $data)) $crm_offer["project_id"] = $data["project_id"] ;
		if(array_key_exists("crm_type", $data)) $crm_offer["crm_type"] = $data["crm_type"] ;
		if(array_key_exists("crm_files", $data)) $crm_offer["crm_files"] = $data["crm_files"] ; 


		$database->update("crm_offer",$crm_offer,array("id" => $id));
		$crm_offer_id = $id;
		
		if(strlen($data["note"]) > 0){
			$crm_activity = array();		
			$crm_activity["crm_offerID"] = $crm_offer_id ;
			if(array_key_exists("note", $data)) $crm_activity["note"] = $data["note"] ; 
			$crm_activity["date"] = date("Y-m-d H:i:s");
			if(array_key_exists("crm_user", $data)) $crm_activity["crm_user"] = $data["crm_user"] ;
			$crm_activity_id = $database->insert("crm_activity",$crm_activity);
		}
		
		return $crm_offer_id;

	}
       

    public function getCRMType(){
	    global $database;
	    
	    $orderTypes = $database->select("crm_type","*");
	    
	    if(is_array($orderTypes)) return $orderTypes;
	 
	    return array();
	    
    }


	public function getOfferByID($id){
		global $database;
		
		$output = $database->get("crm_offer","*",array("id" => $id));
		
		$output["notes"] = $database->select("crm_activity","*",array("crm_offerID" => $id, "ORDER" => array("date" => "DESC")));
		$output["company"] = $database->get("client","*",array("id" => $output["company_id"]));
		$output["status"] = $database->get("crm_status","*",array("id" => $output["status"]));
		
		$output["crm_type"] = $database->get("crm_type","*",array("id" => $output["crm_type"]));
		$output["project"] = $database->get("crm_project","*",array("id" => $output["project_id"]));

		$crm_files = unserialize($output["crm_files"]);
		$output["crm_files"] = array();
		if(is_array($crm_files)){
			foreach($crm_files as $file){
				$output["crm_files"][] = $database->get("media","*",array("mediaID" => $file));
			}
		}
		
		if(is_array($output["notes"] )){
			foreach($output["notes"]  as $key => $note){
				$output["notes"][$key]["crm_user"] =  $database->get("admin","*",array("userID" => $note["crm_user"]));
			}
		}
		
		return $output;	
	}
}

?>