<?php

class OrderModel extends Model {

    
	public function getOrderById ($id){
	    global $database;
	    
		$order = $database->get("order","*",array("orderID" => $id));	

		if($order){
			$order["client"] = $database->get("client","*",array("id" => $order["client_id"]));	
			$order["berater_1"] = $database->get("order_berater","*",array("beraterID" => $order["beraterID_1"]));	
			$order["berater_2"] = $database->get("order_berater","*",array("beraterID" => $order["beraterID_2"]));	
			$order["positionen"] = $database->select("order_position","*",array("orderid" => $order["orderID"]));	
			
			
			$files = unserialize($order["media"]);
			$order["media"] = array();
			if(is_array($files)){
				foreach($files as $file){
					$order["media"][] = $database->get("media","*",array("mediaID" => $file));
				}
			}
			
			if(is_array($order["positionen"])){
				foreach($order["positionen"] as $key => $pos){
					$oldKats = unserialize($pos["import_kat"]);
					
					if(is_array($oldKats)){
						foreach($oldKats as $ok){
							$order["positionen"][$key]["import_kat_text"][] =	$database->get("streuplan_kategorien",array("seite","ebene1","ebene2","ebene3","ebene4"),array("id" => $ok));
						}
					}
				}
			}
		}
				
		return $order;	
	}
	
	/*
	public function importProduct()
    {
	    global $database;

		$order = $database->select("order_import", "*");
		
		
		foreach($order as $d) {
			
			$query = array();
			
			$query[orderID] = $d[id];
			$query[kunde] = $d[kunde];
			$query[gp] = $d[gp_nr];
			$query[import] = 1;
			$query[import_note] = $d[note];
			$query[vermittler_alt] = $d[vermittler];
			$query[status] = 1;
			$query[invoice_date] = $d[bis];
			$query[SAP_jobnr] = $d[jobnr];
			$query[orderID] = $d[id];
			
			$position = array();
			
			$position[orderid] = $d[id];
			$position[von] = $d[ab];
			$position[bis] = $d[bis];
			$position[gesamtpreis] = $d[preis];
			$position[rabatt] = $d[rabatt];
			$position[abrechnungsart] = $d[abrechnungsart];
			$position[abrechnungsintervall] = $d[berechnung];
			$position[wiedervorlage] = $d[wiedervorlage];
			$position[SAP_STICHWORT] = $d[sap_stichwort];
			$position[import_kat] = $d[knr];

			$database->insert("order_position",$position);

			
		}		
		
		return $order;
    }
	*/


	public function loadOrderData($id)
    {
	    global $database;

		$data = $database->query("SELECT * FROM `order` o LEFT JOIN `client` c ON o.client_id = c.id WHERE o.orderID=$id GROUP BY o.orderID")->fetchAll();
		if(is_array($data)) return $data[0];
		return array();
	
    }
    
	public function loadOrderPositionData($id)
    {
	    global $database;

		$data = $database->query("SELECT * FROM `order_position` WHERE orderid=$id")->fetchAll();
		if(is_array($data)) return $data;
		return array();
	
    }    
	
	
	
	
	public function updateProduct($id,$data)
    {
	    global $database;

		$database->update("product",$data,array("id" => $id));
		return $id;
    }
    
 
     public function get_vermittler_by_id($id){
	    global $database;

		$data =   $database->select("order_berater", "*", array("beraterID" => $id));
		return  $data;
    }


    public function query_vermittler($data){
	    global $database;

		$data =   $database->select("order_berater", "*", [
			"OR" => [
				"vorname[~]" => $data,
				"nachname[~]" => $data,
				"gp[~]" => $data,
				"email[~]" => $data,
			],

		]);
		return  $data;
    }
    
    
    public function insert_client($data){
	    global $database;
	    
	    $id = $database->insert("client",$data);

	    return $id;
	    
    }
    
	public function get_client_by_id($id){
	    global $database;

		$data =   $database->get("client", "*", array("id" => $id));
		return  $data;
    }


    public function query_client($data = null){
	    global $database;
		
		if(is_null($data)){
					$data =   $database->select("client", "*",["status" => 1, "LIMIT" => 10]);
		return  $data;

		}

		$data =   $database->select("client", "*", [
			
		"AND" => [
			"OR" => [
				"firstname[~]" => $data,
				"lastname[~]" => $data,
				"company[~]" => $data,
				"gp[~]" => $data,
			],
		"status" => "1"
		]
			

		]);
		return  $data;
    }
    
    
    
    public function getOrderTypes(){
	    global $database;
	    
	    $orderTypes = $database->select("order_type","*", ["ORDER" => ["t_order" => "ASC"],]);
	    
	    if(is_array($orderTypes)) return $orderTypes;
	 
	    return array();
	    
    }
   
	public function getOrderStatus(){
	    global $database;
	    
	    $order_status = $database->select("order_status","*");
	    
	    if(is_array($order_status)) return $order_status;
	 
	    return array();
	    
    } 
    
    public function getOrderPortal(){
	    global $database;
	    
	    $order_portal = $database->select("order_portal","*");
	    
	    if(is_array($order_portal)) return $order_portal;
	 
	    return array();
	    
    } 
    
	public function insertOrder($order){
		global $database;
		
		$positions = array();
	
		if(array_key_exists("positions", $order)){
			$positions = $order["positions"];
			unset($order["positions"]);
		}
		if(!$orderId = $database->insert("order",$order)) throw new Exception ("Auftrag konnte nicht angelegt werden.");
		
		foreach($positions as $pos){
			
			$pos["orderid"] = $orderId;
			$database->insert("order_position",$pos);
		}
		return $orderId;
		
	}    

	public function updateOrder($id,$order){
		global $database;
		
		$positions = array();

		$oldOrder = $this->getOrderById ($id);
		
		if(array_key_exists("status", $oldOrder) && $oldOrder["import"] == 1) $order["import"] = -1;
	
		if(array_key_exists("positions", $order)){
			$positions = $order["positions"];
			unset($order["positions"]);
		}
		$database->update("order",$order,array("orderID" => $id));
		
		foreach($positions as $pos){
			$pos["orderid"] = $id;
			
			$posID = $pos["id"];
			unset($pos["id"]);
			
			if($posID){
				$database->update("order_position",$pos,array("positionID" => $posID));
			}else{
				$database->insert("order_position",$pos);

			}
		}

		
		return $id;
		
	}    

    
}

?>