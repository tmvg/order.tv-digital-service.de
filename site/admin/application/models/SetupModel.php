<?php

class SetupModel extends Model {
	

	public function getSetupData()
    {
        global $database;
        return $database->select("setup", "*");
    } 	
    
    public function updateSetup($data, $where) {
	    global $database;
        $database->update("setup", $data, $where);
    
    }
    
    /*
    public function getAllMediaData()
    {
        global $database;
        $medias = $database->query("SELECT * FROM media ORDER BY mediaID DESC");
        return $medias;
    } 	
            


	public function getDataById($table, $where)
    {
	    global $database;
        $result = $database->select($table, "*", $where);
        return $result[0];
    }  
 */


}

?>