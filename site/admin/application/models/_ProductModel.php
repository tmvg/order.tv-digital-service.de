<?php

class ProductModel extends Model {

    
    public function getGroups($status = 1){
	    global $database;
	    
	    $groups = $database->select("group","*",array("status" => $status));
	
		
		return $groups;
	    
	}  
	
	public function getProductById($id){
		global $database;
	    
		if($product = $database->get("product","*",array("id" => $id))){
			
			if($product["group_id"]){ $product["group_name"] = $database->get("group","name",array("id" => $product["group_id"])); }
			return $product;
		}
		return false;

	}  
	
	
	public function insertProduct($data)
    {
	    global $database;
	    
// 		if($clientIdTest = $this->getGroupByGroupIdAndParent($data["group_id"],$data["parent"])) throw new Exception ("Kundennummer schon vorhanden");  
		  
	    $id = $database->insert("product",$data);
		return $id; 
    }
	
	
	public function updateProduct($id,$data)
    {
	    global $database;

/*
		if($clientIdTest = $this->getGroupByGroupIdAndParent($data["group_id"],$data["parent"])){
			if($clientIdTest["id"] != $id) throw new Exception ("Kundennummer schon vorhanden");
		}
*/

		$database->update("product",$data,array("id" => $id));
		return $id;
    }
    
    
    
	public function getGroupByGroupId($group_id){
	    global $database;
	    
		if($client = $database->get("group","*",array("group_id" => $group_id))){
			return $client;
		}
		return false;
	    
    }
    
	public function getProductByProductIdAndGroupId($product_id,$group_id = null){
	    global $database;
	
		if($client = $database->get("product","*",array("AND" => array("product_id" => $product_id,"group_id" => $group_id)))){
					file_put_contents("sql.txt",$database->last_query());
			return $client;
		}
			file_put_contents("sql.txt",$database->last_query());

		return false;
	    
    }
 
	public function deleteProduct($id){
		global $database;
		
		if(! $database->get("product","*",array("id" => $id))) throw new Exception ("Groupe $id not found");
			$database->update("product",array("status" => 0),array("id" => $id));
	}

	
	
	public function getTree($parent = NULL,$levels = NULL,$path = array(),$status = 1){
		global $database;
		
		
		$levels --;
		
		if($parent === null){
			$nodes = $database->select("group",array("id","name(text)","group_id"),array("AND" => array("parent" => NULL,"status" => $status)));
		
		}else{
			$nodes = $database->select("group",array("id","name(text)","group_id"),array("AND" => array("parent" => $parent,"status" => $status)));
		}
		if($levels !== NULL) $levels --;
		
		foreach ($nodes as $key => $node){
			
			$tmpPath = $path;
			$tmpPath[] = $node["group_id"];
			
			$nodes[$key]["tags"]  =  array(implode("-",$tmpPath));
			
			if($levels >= 0 OR $levels === NULL){
				$childNodes = $this->getTree($node["id"],$levels,$tmpPath,$status);
				if($childNodes !== null) $nodes[$key]["nodes"]  = $childNodes;
			}

		}
	
		if(count ($nodes) > 0) return $nodes;

	}


	public function getTreePart($parent = NULL){
		global $database;
		
		if($parent === null){
			$nodes = $database->select("group",array("id","name(text)"),array("parent" => NULL));
		
		}else{
			$nodes = $database->select("group",array("id","name(text)"),array("parent" => $parent));
		}
		foreach ($nodes as $key => $node){
			
			$childNodes = $database->select("group",array("id","name(text)"),array("parent" => $node["id"]));
			
			if(count($childNodes) > 0) $nodes[$key]["nodes"] = $childNodes;

			
/*
			$childNodes = $this->getTree($node["id"]);
			if($childNodes !== null) $nodes[$key]["nodes"]  = $childNodes;
*/
		}
		if(count ($nodes) > 0) return $nodes;

	}


	public function getLastProductInGroup($group_id = NULL){
		global $database;
		
// 		SELECT * FROM product where group_id = 33 ORDER BY product_id DESC LIMIT 1

		if( $group_id == "") $group_id = NULL;

		$nodes = $database->get("product",array("product_id"),array("group_id" => $group_id,"ORDER" => array("product_id"  => "DESC")));
		

		if(count ($nodes) > 0) return intval($nodes["product_id"]);
		
		return 1;
	}	
	
	


	public function getProductGroupAncestors($id){
		global $database;
		
		$data =  $database->get("group",array("parent","group_id"),array("id" => $id));
		$grp[] = $data["group_id"];

		if($data["parent"] !== NULL){
			$data2 = $this->getProductGroupAncestors($data["parent"]);
			
			foreach($data2 as $value){
				$grp[] = $value;
			}

		}
		return $grp;
		
		
	}

}

?>