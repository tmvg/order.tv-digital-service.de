</div>
<footer class="main-footer">
    <div class="container-fluid">
        <div class="row" style="margin: 15px 0 0;">
            <div class="col-lg-6">
                <p class="text-muted">Copyright &copy; <?php echo date('Y'); ?> | <a href="http://www.tv-digital-service.de/" target="_blank"><b>volksfreund Digital Service</b></a> | Version: <?= $config['version']; ?> </p>
            </div>
            <div class="col-lg-6">
                <p class="text-muted pull-right">
                    <?php if(DEBUG) { ?>
                    <small><!-- DO NOT DELETE! - Profiler --></small>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div>
</footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>


<script src="<?php echo BASE_URL; ?>static/assets//bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets//plugins/iCheck/icheck.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
<script src="<?php echo BASE_URL; ?>static/assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/datepicker/locales/bootstrap-datepicker.de.js"></script>

<script src="<?php echo BASE_URL; ?>static/js/form-validator/jquery.form-validator.min.js"></script>


<script src="<?php echo BASE_URL; ?>static/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/fastclick/fastclick.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/dist/js/app.min.js"></script>

<script src="<?php echo BASE_URL; ?>static/js/admin.js"></script>

<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/locale/de.js"></script>

<!-- Select2 -->
<script src="<?php echo BASE_URL; ?>static/assets/plugins/select2/select2.full.min.js"></script>



</body>
</html>
