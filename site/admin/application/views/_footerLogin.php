<footer class="footer">
    <div class="container-fluid" align="center">
        <div class="row" style="margin: 15px 0 0;">
            <div class="col-lg-12">
                <p class="text-muted">Copyright &copy; <?php echo date('Y'); ?> | <a href="http://www.tv-digital-service.com/" target="_blank"><b>volksfreund Digital Service</b></a> | Version: <?= $config['version']; ?> </p>
            </div>

        </div>
    </div>
</footer>


<script src="<?php echo BASE_URL; ?>static/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo BASE_URL; ?>static/js/onscreenkeyboard/jsKeyboard.js"></script>

<script>

$("#showKeyboard").click(function(){
	if(	$("#virtualKeyboard").hasClass("hidden")){
		$("#virtualKeyboard").removeClass("hidden");
$("#showKeyboard").html('<i class="fa fa-keyboard-o" aria-hidden="true"></i> Tastatur verstecken');
		
		
	} 	else{ $("#virtualKeyboard").addClass("hidden");
		$("#showKeyboard").html('<i class="fa fa-keyboard-o" aria-hidden="true"></i> Tastatur anzeigen');

}
});	

     $(function () {
         jsKeyboard.init("virtualKeyboard");

         //first input focus
         var $firstInput = $(':input').first().focus();
         jsKeyboard.currentElement = $firstInput;
         jsKeyboard.currentElementCursorPosition = 0;
     });	
	
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>



</body>
</html>

