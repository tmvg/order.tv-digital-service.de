<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $config["title"] ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/font-awesome.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/font-awesome-animation.min.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/dist/css/skins/_all-skins.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/iCheck/flat/blue.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/morris/morris.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/datepicker/datepicker3.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/daterangepicker/daterangepicker.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/animate.css" type="text/css" media="screen" />
	<!-- fullCalendar 2.2.5-->
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/fullcalendar.print.css" media="print">
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/dist/css/AdminLTE.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/bootstrap-treeview.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/style.css" type="text/css" media="screen" />
	<script src="<?php echo BASE_URL; ?>static/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="<?php echo BASE_URL; ?>static/js/form-validator/jquery.form-validator.min.js"></script>
	<script src="<?php echo BASE_URL; ?>static/js/bootstrap-treeview.js"></script>
	<script src="<?php echo BASE_URL; ?>static/js/js.cookie.js"></script>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="/admin/static/assets/plugins/datatables/dataTables.bootstrap.css">
	<script src="/admin/static/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/admin/static/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	<!--  DataTable Extension -->
	<link rel="stylesheet" type="text/css" href="/admin/static/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css"/>
	 
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/JSZip/jszip.js"></script>
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/pdfmake/pdfmake.js"></script>
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/pdfmake/vfs_fonts.js"></script>
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
	<script type="text/javascript" src="/admin/static/assets/plugins/datatables/extensions/Buttons/js/buttons.print.js"></script>	
	

</head>
<body class="hold-transition skin-blue sidebar-mini">

<?php include(APP_DIR."views/_menu.php"); ?>  
 
<div class="content-wrapper">

 <?= $PAGEINFO ?> 