<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?php ?></title>
    
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/dist/css/AdminLTE.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/iCheck/square/blue.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/js/onscreenkeyboard/css/jsKeyboard.css" type="text/css" media="screen" />

</head>
<body class="hold-transition login-page">