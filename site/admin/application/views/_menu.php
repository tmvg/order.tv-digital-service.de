<?php
$role = $_SESSION['userRole'];	
?>
<div class="wrapper">
	  <header class="main-header">
    <!-- Logo -->
    <a href="<?= $config['base_url'] ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?= $config['title_short'] ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?= $config['title'] ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
	        
	        <li>
	        <a href="#" class="dropdown-toggle bigclock" data-toggle="dropdown" >
              <i class="fa fa-clock-o"></i> <span id="clock"><?= date("H:i", time()) ?></span>&nbsp;<?= date("(d.m.Y)", time()) ?>
            </a>
	        </li>
			<li>
			            
            <a href="/admin/admin/edit/<?= $_SESSION["userID"] ?>" >
              <i class="fa fa-user"></i> <?= $_SESSION["name"] ?> (<?= strtoupper($role) ?>)
            </a>
			</li>
          <li>
            <a href="" onclick="window.location.assign('/admin/login/logout')" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
<ul class="sidebar-menu">
	
	<?php
	
	
	foreach($config_menu['role'] as $r) {
		
		if(in_array($role, $r['permissions'])) {
			$menu .= '<li class="header">'.$r['name'].'</li>';
			foreach($config_menu['role'][$r['slug']]['menu'] as $key => $m) {
				
				if(!in_array($role,array("admin","editor"))){  if (  !in_array($m[0], $_SESSION['permission']))	continue; }
				
				$menu .= '<li class="treeview">';
				$menu .= '<a href="/admin/'.$m[0].'"><i class="fa '.$m[1].'"></i> <span>'.$key.'</span></a>';
				$menu .= '</li>';
			
				
			}
			
		}
		
		
		
	}	
	
	echo $menu;
		
	?>





</ul>

    </section>
    <!-- /.sidebar -->
  </aside>