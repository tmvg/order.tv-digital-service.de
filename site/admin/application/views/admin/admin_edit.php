<?php include(APP_DIR.'/views/_header.php'); ?>
	
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Admin bearbeiten <a href="/admin/admin/" class="btn btn-success pull-right">zur Übersicht</a></h1>  
    </section>
    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Admin bearbeiten</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal"  method="post" id="registration">
	            <input type="hidden" name="id" value="<?= $admin["userID"] ?>" />
	            
              <div class="box-body">
	              
              
				<div class="form-group">
					<label class="col-sm-2 control-label">Vorname</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="firstname" value="<?= $admin["userFirstName"] ?>" placeholder="Vorname" required="required" >
					</div>
					<label class="col-sm-2 control-label">Nachname</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="lastname" placeholder="Nachname" value="<?= $admin["userLastName"] ?>" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">GP ID</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="gp" value="<?= $admin["gp"] ?>" placeholder="" >
					</div>
					
					<label class="col-sm-2 control-label">E-Mail</label>

                  <div class="col-sm-4">
                    <input type="email" class="form-control" name="email" placeholder="E-Mail" data-validation="email" value="<?= $admin["userEmail"] ?>" readonly="" >
                  </div>
					
				</div>

				
				
				                
                <div class="form-group">


                  <label class="col-sm-2 control-label">Status</label>
                  
                  <div class="col-sm-4">
					<select name="status" class="form-control">
						<option value="1" <?php if($admin["userStatus"] == "1") echo 'selected=""'; ?>>Online</option>
						<option value="0" <?php if($admin["userStatus"] == "0") echo 'selected=""'; ?>>Offline</option>
					</select>
					
                  </div>
                  
                  
                  

                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">Rolle</label>
				  	
                  <div class="col-sm-4">
					<select name="role" class="form-control" id="roleSelector">
						<option value="admin" <?php if($admin["userRole"] == "admin") echo 'selected=""'; ?>>Superadmin</option>
						<option value="editor" <?php if($admin["userRole"] == "crm") echo 'selected=""'; ?>>CRM</option>
						<option value="producer" <?php if($admin["userRole"] == "sales") echo 'selected=""'; ?>>Sales</option>
					</select>
					
							
                  </div>
                  				 
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Speichern</button>
              </div>
              <!-- /.box-footer -->
            </form>
         
         
         <script>
						$.validate({
						  form : '#registration',
						  modules : 'html5',
						  lang : 'de'
						});
						


					// Set Default Password
					
					function generateNewPassword() {
						$("#password").val(getPassword());
						$("#password").attr("type", "text");
					}
					
				
			
			</script>
         
         
        </div>
      </div>
      </div></section>

<section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Neues Passwort erstellen</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="/admin/admin/newpass" method="post" id="registration">
	            <input type="hidden" name="id" value="<?= $admin["userID"] ?>" />
	            
              <div class="box-body">
	              
	            <div class="form-group">
                  <label class="col-sm-2 control-label">Passwort</label>
				  	
                  <div class="col-sm-4">
                    <div class="input-group">
                    <span class="input-group-addon" onclick="generateNewPassword()" style="cursor: pointer">
					<i class="fa fa-key"></i>
					</span>
					<input type="text" class="form-control" id="password" name="password" value="" placeholder="Passwort">
                  </div>
                  </div>
                 
                
                <label class="col-sm-2 control-label">Logindaten</label>
                  <div class="col-sm-4">
	                  
	                  <div class="checkbox">
                      <label>
                        <input type="checkbox" name="sendUserInfo" checked=""> Zugangsdaten per E-Mail senden
                      </label>
                    </div>
	                
                    
                  </div>
	              </div> 
              </div>
              
               <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Speichern</button>
              </div>
            </form></div></div></div></section>


<?php include(APP_DIR.'/views/_footer.php'); ?>