<?php include(APP_DIR.'/views/_header.php'); ?>
	
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Konto bearbeiten</h1>  
    </section>
    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nutzerdaten</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal"  method="post" id="registration" action="/admin/admin/edit/<?= $admin["userID"] ?>" >
	            <input type="hidden" name="id" value="<?= $admin["userID"] ?>" />
	            
              <div class="box-body">
	              
              
				<div class="form-group">
					<label class="col-sm-2 control-label">Vorname</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="firstname" value="<?= $admin["userFirstName"] ?>" placeholder="Vorname" required="required" >
					</div>
					<label class="col-sm-2 control-label">Nachname</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="lastname" placeholder="Nachname" value="<?= $admin["userLastName"] ?>" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">GP ID</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="gp" value="<?= $admin["gp"] ?>" placeholder="" >
					</div>
					
					
					
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Alias</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="alias" value="<?= $admin["alias"] ?>"   >
					</div>
					<label class="col-sm-2 control-label">Mobilnummer</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="tel"  value="<?= $admin["tel"] ?>" >
					</div>
				</div>
				
				                
                <div class="form-group">
                  <label class="col-sm-2 control-label">E-Mail</label>

                  <div class="col-sm-4">
                    <input type="email" class="form-control" name="email" placeholder="E-Mail" data-validation="email" value="<?= $admin["userEmail"] ?>" readonly="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Speichern</button>
              </div>
              <!-- /.box-footer -->
            </form>
         
         
         <script>
						$.validate({
						  form : '#registration',
						  modules : 'html5',
						  lang : 'de'
						});
						


					// Set Default Password
					
					function generateNewPassword() {
						$("#password").val(getPassword());
						$("#password").attr("type", "text");
					}
					
				
			
			</script>
         
         
        </div>
      </div>
      </div></section>

<section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Passwort ändern</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="/admin/admin/userNewPass" method="post" id="registration">
	            <input type="hidden" name="id" value="<?= $admin["userID"] ?>" />
	            
              <div class="box-body">
			  	
				<div class="form-group">
					<label class="col-sm-2 control-label">altes Passwort</label>
					<div class="col-sm-4">
						<input type="text" class="form-control"  name="old_password" value="" placeholder="Passwort">
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-2 control-label">altes Passwort wiederholen</label>
					<div class="col-sm-4">
						<input type="text" class="form-control"  name="old_password_sec" value="" placeholder="Passwort">
					</div>
				</div> 
 
	            <div class="form-group">
                  <label class="col-sm-2 control-label">Passwort</label>
				  	
                  <div class="col-sm-4">
                    <div class="input-group">
                    <span class="input-group-addon" onclick="generateNewPassword()" style="cursor: pointer">
					<i class="fa fa-key"></i>
					</span>
					<input type="text" class="form-control" id="password" name="password" value="" placeholder="Passwort">
                  </div>
                  </div>
                 
                
                <label class="col-sm-2 control-label">Logindaten</label>
                  <div class="col-sm-4">
	                  
	                  <div class="checkbox">
                      <label>
                        <input type="checkbox" name="sendUserInfo" checked=""> Zugangsdaten per E-Mail senden
                      </label>
                    </div>
	                
                    
                  </div>
	              </div> 
              </div>
              
               <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Speichern</button>
              </div>
            </form></div></div></div></section>


<?php include(APP_DIR.'/views/_footer.php'); ?>