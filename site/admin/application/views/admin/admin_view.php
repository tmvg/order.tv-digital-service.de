<?php include(APP_DIR.'/views/_header.php'); ?>
	
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Admin anlegen</h1>  
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Neuer Admin</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="/admin/admin/newadmin" method="post" id="registration">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Vorname</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="firstname" placeholder="Vorname" required="required">
                  </div>

                  <label class="col-sm-2 control-label">Nachname</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="lastname" placeholder="Nachname" required="required">
                  </div>
                </div>
                
                
                
				<div class="form-group">
					<label class="col-sm-2 control-label">GP ID</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="gp" value="<?= $admin["gp"] ?>" placeholder="" >
					</div>
 <label class="col-sm-2 control-label">E-Mail</label>

                  <div class="col-sm-4">
                    <input type="email" class="form-control" name="email" placeholder="E-Mail" data-validation="email" required="required">
                  </div>
				</div>

             
                
                
                <div class="form-group">
                 

                  <label class="col-sm-2 control-label">Passwort</label>

                  <div class="col-sm-4">
                    <div class="input-group">
                    <span class="input-group-addon" onclick="generateNewPassword()" style="cursor: pointer">
					<i class="fa fa-key"></i>
					</span>
					<input type="text" class="form-control" id="password" name="password" readonly="" placeholder="Passwort" required="required">
                  </div>
                  </div>
                  
                  <label class="col-sm-2 control-label">Rolle</label>
				  	
                  <div class="col-sm-4">
					<select name="role" class="form-control" id="roleSelector">
						<option value="admin" <?php if($admin["userRole"] == "admin") echo 'selected=""'; ?>>Superadmin</option>
						<option value="editor" <?php if($admin["userRole"] == "crm") echo 'selected=""'; ?>>CRM</option>
						<option value="producer" <?php if($admin["userRole"] == "sales") echo 'selected=""'; ?>>Sales</option>
					</select>
                </div>

				 
				  <label class="col-sm-2 control-label">Logindaten</label>
                  <div class="col-sm-4">
	                  
	                  <div class="checkbox">
                      <label>
                        <input type="checkbox" name="sendUserInfo" checked=""> Zugangsdaten per E-Mail senden
                      </label>
                    </div>
	                
                    
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Speichern</button>
              </div>
              <!-- /.box-footer -->
            </form>
         
         
         <script>
						$.validate({
						  form : '#registration',
						  modules : 'html5',
						  lang : 'de'
						});
						


					// Set Default Password
					
					function generateNewPassword() {
						$("#password").val(getPassword());
					}
					
					$(function() {
						generateNewPassword();
					});
			
			</script>
         
         
        </div>
      </div>
      </div></section>



  	<section class="content-header">
      <h1>Admin<small>Übersicht</small>   </h1>  

  	</section>
  	
  	<section class="content">
	  	<div class="row">
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Admin-Übersicht</h3>

                          </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Rolle</th>
                  <th>E-Mail</th>
                  <th>Status</th>
                  <th>#</th>
                </tr>
                
				<?php
				
				foreach($admins as $a) {
					
					if($a["userStatus"] == "1") $statusColor = '<span class="label label-success">online</span>'; else $statusColor = '<span class="label label-danger">offline</span>';
					echo '<tr><td>'.$a["userID"].'</td><td>'.$a["userLastName"].", ".$a["userFirstName"].'</td><td>'.$a["userRole"].'</td><td>'.$a["userEmail"].'</td><td>'.$statusColor.'</td><td><a href="/admin/admin/edit/'.$a["userID"].'">bearbeiten</a></td></tr>';
				}
				
				?>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  	</div></section>
      
      


<?php include(APP_DIR.'/views/_footer.php'); ?>