<?php include(APP_DIR.'/views/_header.php'); ?>

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/fullcalendar.min.css">
	

<!-- Event Modal -->
<div class="modal fade" id="eventModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Öffnungszeiten bearbeiten</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
	        <div class="row"> 
	        <div class="col-xs-4">  
            <label for="recipient-name" class="control-label">Datum</label>
            <input type="text" id="event-Date" class="form-control" readonly="">
	        </div>
	        
	        <div class="col-xs-4"> 
            <label for="message-text" class="control-label">von</label>
            <input type="text" class="form-control" id="event-Start">
	        </div>
	        
	        <div class="col-xs-4"> 
            <label for="message-text" class="control-label">bis</label>
            <input type="text" class="form-control" id="event-End">
	        </div>
	        
	        <div class="col-xs-6"> 
            <label for="message-text" class="control-label">Slot in Minuten</label>
            <input type="text" class="form-control" id="event-Start">
	        </div>
	        
	        <div class="col-xs-6"> 
            <label for="message-text" class="control-label">Burger pro Slot</label>
            <input type="text" class="form-control" id="event-End">
	        </div>
          </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">schließen</button>
        <span class="btn btn-primary" onclick="insertEventData()">speichern</span>
      </div>
    </div>
  </div>
</div>

<!-- Event Modal -->
<div class="modal fade" id="newEventModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Öffnungszeiten hinzufügen</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
	        <div class="row"> 
	        <div class="col-xs-4">  
            <label for="recipient-name" class="control-label">Datum</label>
            <input type="text" id="eventDate" class="form-control" placeholder="<?= date("d.m.Y", time()); ?> dd.mm.YYYY">
	        </div>
	        
	        <div class="col-xs-4"> 
            <label for="message-text" class="control-label">von</label>
            <input type="text" class="form-control" id="eventStart" placeholder="12:00">
	        </div>
	        
	        <div class="col-xs-4"> 
            <label for="message-text" class="control-label">bis</label>
            <input type="text" class="form-control" id="eventEnd" placeholder="15:00">
	        </div>
	        
	        	        <div class="col-xs-6"> 
            <label for="message-text" class="control-label">Slot in Minuten</label>
            <input type="text" class="form-control" id="eventSlot" value="<?= $setup['timeslot'] ?>">
	        </div>
	        
	        <div class="col-xs-6"> 
            <label for="message-text" class="control-label">Burger pro Slot</label>
            <input type="text" class="form-control" id="eventBurger" value="<?= $setup['burgerInTimeslot'] ?>">
	        </div>
	        
          </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">schließen</button>
        <span class="btn btn-primary" onclick="insertEventData()">speichern</span>
      </div>
    </div>
  </div>
</div>
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Time Machine</h1>  
      
     
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Öffnungszeiten verwalten</h3>
              <span class="btn btn-warning pull-right" onclick="$('#newEventModal').modal()">neue Öffnungszeit hinzufügen</span>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                                

                
			   <div id="calendar"></div>


<script src="<?php echo BASE_URL; ?>static/js/moment.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }

    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
        
       
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'July',
 'August', 'September', 'Oktober', 'November', 'Dezember'],
 
 	dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
 	
 	  firstDay: 1,
      
      buttonText: {
        today: 'heute',
        month: 'Monat',
        week: 'Woche',
        day: 'Tag'
      },
      //Random default events
      events: [

	  <?php
		  

		  
		foreach($events as $e) {
		
		
	
			
			$y = date("Y", $e['day']);
			$d = date("d", $e['day']);
			$m = date("m", $e['day'])-1;
			$h_s = date("H", $e['start']);
			$m_s = date("i", $e['start']);
			$h_e = date("H", $e['end']);
			$m_e = date("i", $e['end']);
			
			$title = "(".$e['burgerInSlot']." B in ".$e['slottime']." Min)";
			echo '{id:'.$e['id'].','.'title:"'.$title.'",'.'start:new Date('.$y.', '.$m.', '.$d.', '.$h_s.', '.$m_s.'),end:new Date('.$y.', '.$m.', '.$d.', '.$h_e.', '.$m_e.'),'.'allDay:false,'.'backgroundColor:"green",borderColor:"green"},';
		
		}
		  
	   ?>

      ],
      timeFormat: 'H(:mm)',
      displayEventEnd: true,
      editable: false,
      droppable: false,
      
      /*
      eventClick: function(calEvent, jsEvent, view) {

	  $("#eventModal").modal();
	  
	  $("#eventDate").val(moment(calEvent.start).format("DD.MM.YYYY"));
	  $("#eventStart").val(moment(calEvent.start).format("HH:mm"));
	  $("#eventEnd").val(moment(calEvent.end).format("HH:mm"));

        // change the border color just for fun
        //$(this).css('border-color', 'red');

    }
      */
   });

    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });
    $("#add-new-event").click(function (e) {
      e.preventDefault();
      //Get value and make sure it is not null
      var val = $("#new-event").val();
      if (val.length == 0) {
        return;
      }

      //Create events
      var event = $("<div />");
      event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
      event.html(val);
      $('#external-events').prepend(event);

      //Add draggable funtionality
      ini_events(event);

      //Remove event from text input
      $("#new-event").val("");
    });
  });
  


function insertEventData() {
	
	var day = $("#eventDate").val();
	var eventStart = $("#eventStart").val();
	var eventEnd = $("#eventEnd").val();
	var eventSlot = $("#eventSlot").val();
	var eventBurger = $("#eventBurger").val();
	
	$.post( "ajax/insertEvent/", { day: day, from: eventStart, end: eventEnd, slottime:eventSlot, burgerInSlot:eventBurger })
		.done(function( data ) {
		
		alert("Eintrag wurde gespeichert.");
		
		/*
		if(data.indexOf("[Fehler]") == -1) {
		$("#count-"+id).fadeOut(500).text(newCount).fadeIn(500);
		
		$("#box-"+id).removeClass($("#box-"+id).attr("class"));
		if(newCount > 20) $("#box-"+id).addClass("small-box bg-green"); 
		if(newCount <= 20 && newCount > 10) $("#box-"+id).addClass("small-box bg-orange"); 
		if(newCount <= 10 && newCount > -1) $("#box-"+id).addClass("small-box bg-red"); 
		if(newCount == -1) $("#box-"+id).addClass("small-box bg-green"); 
		
		}
		$("#add-"+id).val("");$("#remove-"+id).val("");$("#value-"+id).val("");
		$("#footer-"+id).text(data);
		$("#footer-"+id).delay(3000).fadeOut(500, function () { $("#footer-"+id).text(""); });
		*/
	});
	
}



</script>

      </div>
        </div>
      </div>
      </div></section>






<?php include(APP_DIR.'/views/_footer.php'); ?>