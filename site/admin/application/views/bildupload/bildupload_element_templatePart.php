<?php $exists = false;
		if($data){
		 foreach($data as $dataKey => $item){
			 if(in_array($dataKey, array("id","client_id","salutation"))) continue;
		if(! is_array($item)) {if(strlen($item) > 0) $exists = true;}
	}} ?>


	<div class="box ansprechpartner <?= ($exists)?"":"collapsed-box" ?>" id="box_<?= rand(0,100000)?>" data-id="<?= $data["id"] ?>">

			<input type="hidden" name="voting[element][<?= $key ?>][id]" value="<?= $data["id"] ?>">
	
	        <div class="box-header">
	          <h3 class="box-title">Element
	            <small></small>
	          </h3>
	          <!-- tools box -->
	          <div class="pull-right box-tools">
	            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
	              <i class="fa <?= ($exists)?"fa-minus":"fa-plus" ?>"></i></button>
	          </div>
	          <!-- /. tools -->
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body pad" style="<?= ($exists)?"display: block":"display: none" ?>">

	
				<div class="row">
					<div class="col-xs-4"> 			
						
						<?php
							$GLOBALS["mediaSelectorCount"] ++;
							$identifyer = "media_".rand()."_"; 
						?>



			
						<div class="box box-primary">
							 <div class="box-header">
						      <h3 class="box-title">Bild
						        <small></small>
						      </h3>
							 </div>
							<div class="box-body box-profile" id="<?= $identifyer ?>image">
								<img class="img-responsive" src="<?= $data["image_data"]["mediaVersion"]["mediumUrl"] ?>" alt="Bild" >
								<input type="hidden" class="form-control imageIdField"  name="voting[element][<?= $key ?>][image]" value="<?= $data["image"]?>" >
							</div>
							<input type="hidden" value="<?= $data["mdbUpload"] ?>" name="voting[element][<?= $key ?>][mdbUpload]" id="<?= $identifyer ?>mdbUpload" />
							<?php if($this->pageVars["mdbUpload"]){ ?>
							<div class="box-footer clearfix">
				    			<button class="btn <?= ($data["mdbUpload"] == 1)?"btn-success":"btn-default" ?>  mdbUpload" data-id="<?= $identifyer ?>" >MDB</button>
				            </div>
				            <?php } ?>
						</div>
						<script>
							
/*
									$(document).ready(function(){
									
									$(".imageIdField").each(function(){
										
										var imageBlockID  = $($(this).parent());
										
										$.ajax({
										  type: "POST",
										  url: "/admin/media/ajax_mediaByID/call",
								          dataType: "json",
										  data: {"id":$(this).val()},
										  success: function(data){
											console.log(data);	
											if(data.image){
											
												imageBlockID.find("img").attr("src",data.image.mediaVersion.thumbnailUrl) ;
												imageBlockID.find("img").removeClass("hidden") ;
											}		  
										  },
										  error: function(){
											  alert("Verbindung nicht möglich");
										  		btn.prop('disabled', false);
										  		btn.html(btnHtml);
							
										  }		
								
										});
										
										
									});
								})
*/
							
						</script>

						
						<?php // $this->get_template_part("media/media_select_image",array("name" => "voting[element][".$key."][image]",'title' => "Bild","image" => $data["image"])) ?>

					</div>
					<div class="col-xs-8"> 		
						
		                
						<div class="form-group">
		                  <label for="exampleInputEmail1">Überschrift</label>
		                  
							<div class="input-group input-group-sm">
								<input class="form-control imageTitle" type="text" placeholder="Überschrift" name="voting[element][<?= $key ?>][ueberschrift]"  value="<?= $data["ueberschrift"] ?>" data-validation="length" data-validation-length="min5">
								<span class="input-group-btn">
									<button type="button" class="btn btn-info btn-flat copyTitle">Inhalt für alle Titel übernehmen</button>
								</span>
							</div>
		                  
		                </div>	
		                
		                
		                
		                
		                
						
						<div class="form-group">
		                  <label for="exampleInputEmail1">Rechteinhaber</label>
		                  <input class="form-control rechteinhaber" type="text" placeholder="Rechteinhaber" name="voting[element][<?= $key ?>][rechteinhaber]"  value="<?= $data["rechteinhaber"] ?>" data-validation="length" data-validation-length="min2">
		                </div>	 
						
						
						<div class="form-group">
		                  <label for="exampleInputEmail1">Fotograf</label>
		                  <input class="form-control fotograf" type="text" placeholder="Fotograf" name="voting[element][<?= $key ?>][fotograf]"  value="<?= $data["fotograf"] ?>" data-validation="length" data-validation-length="min2">
		                </div>	
		                
						<div class="form-group">
		                  <label for="exampleInputEmail1">Honorar ID</label>
		                  <input class="form-control honorar" type="text" placeholder="Honorar ID" name="voting[element][<?= $key ?>][honorar]"  value="<?= $data["honorar"] ?>" data-validation="length" data-validation-length="min5">
		                </div>
<!--
						<div class="form-group">
		                  <label for="exampleInputEmail1">Zeitung</label>
		                  <input class="form-control zeitung" type="text" placeholder="Zeitung" name="voting[element][<?= $key ?>][zeitung]"  value="<?= $data["zeitung"]  ?>" data-validation="length" data-validation-length="min5">
		                </div>			        
-->        
								                        
						<div class="form-group">
		                  <label for="exampleInputEmail1">Abgebildete Personen haben Veröffentlichung zugestimmt</label>
		                  <select class="form-control" name="voting[element][<?= $key ?>][personenfreigabe]" name="answer" data-validation="required">
			                  <option value="">Bitte wählen:</option>

			                  <option value="6" <?= ($data["personenfreigabe"] == 6)?"selected":"" ?>>Personen haben zugestimmt</option>
			                  <option value="4" <?= ($data["personenfreigabe"] == 4)?"selected":"" ?>>keine Personen auf dem Bild</option>
			                  <option value="5" <?= ($data["personenfreigabe"] == 5)?"selected":"" ?>>Personen sind Beiwerk</option>
			                  <option value="7" <?= ($data["personenfreigabe"] == 7)?"selected":"" ?>>Personen haben NICHT zugestimmt</option>
		                  </select>
		                </div>			        		                		                
		                							
						<div class="form-group">

							<label for="exampleInputEmail1">Bildtext</label>
							<button type="button" class="btn btn-info btn-flat pull-right copyText">Inhalt für alle Beschreibungen übernehmen</button>

							<textarea class="textarea imageText" name="voting[element][<?= $key ?>][bildtext]" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $data["bildtext"] ?></textarea>
						</div>
					</div>
				</div>
				
				<a class="remove btn btn-danger pull-right"  data-toggle="modal" data-target="#confirm-removeContact"><i class="fa fa-user-times" aria-hidden="true"></i></a>

	
			</div>
	
		</div>   