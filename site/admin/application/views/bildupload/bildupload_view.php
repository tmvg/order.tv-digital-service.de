<?php include(APP_DIR.'/views/_header.php'); ?>
<div class="col-md-12" style="margin-top: 10px">
	<div class="col-md-9">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">MediaDB</h3>
				<p>Bilder werden direkt in die InterRED Media Datenbank gelanden.</p>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Datum</th>
							<th>-</th>
							<th>-</th>
							<th>-</th>

						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Datum</th>
							<th>-</th>
							<th>-</th>
							<th>-</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		</div>
	<div class="col-md-3">
		<a href="/admin/bildupload/edit" class="btn btn-primary btn-block margin-bottom">neue Bilder hochladen</a>
	</div>

</div>        
<link rel="stylesheet" href="/admin/static/assets/plugins/datatables/dataTables.bootstrap.css">
<script src="/admin/static/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/static/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"processing": true,
		"serverSide": true,
		 "responsive": true,
	        "order": [[ 0, "desc" ]],

		"ajax": "/admin/bildupload/ajax_votingTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		
		 "columnDefs": [
                { "type": "date-de", "targets": [2] },
            ],
		
		"columns" : [

			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{
				"render": function (data, type, row  ){
					return row[4]+" "+row[5];
				}
			},
						{'data':'6'},

			{
				sortable: false,
				"render": function (data, type, row  ){
					return "<a  href='/admin/bildupload/edit/"+row[0]+"' class='select btn btn-primary'>bearbeiten</a>";
				}
			}
		]	
		
		
		

		
    });
  });
</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>