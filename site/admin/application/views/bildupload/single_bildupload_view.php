<?php include(APP_DIR.'/views/_header.php'); ?>


<div class="col-md-12" style="margin-top: 10px">


<form method="post" id="bilderstreckeForm" action="/admin/bildupload/produce/<?= $data['id'] ?>">
<div class="col-md-9">	
	<div class="alert alert-danger alert-dismissible <?= ($error)?"":"hidden"?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i> Alert!</h4>
		<?= $error ?>
	</div>
	
	<div class="callout callout-warning  <?= ($data["prozessing"] == 0 )?"":"hidden"?>">
	    <h4>Export in Arbeit</h4>
	    <p>Bilder werden für Export vorbereitet und im Anschluss zu InterRED überstellt.</p>
	</div>

	<div class="callout callout-success  <?= ($data["prozessing"] == 1 )?"":"hidden"?>">
	    <h4>Export abgeschlossen</h4>
	    <p>Bilder wurden an InterRED überstellt und werden zeitnah in der Mediathek auftauchen.</p>
	</div>
		
	   <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Bilder auswählen
            <small></small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">
            
                <div class="form-group">
                  <label for="exampleInputEmail1">Titel</label>
                  <input class="form-control" type="text" placeholder="Titel" id="group_name" name="voting[title]"  value="<?php if(strlen($data["title"]) > 0){ echo $data["title"];}else{echo "Upload ". $user["userFirstName"] ." ".$user["userLastName"] . " " .date("d.m.Y H:i"); } ?>" data-validation="length" data-validation-length="min5">
                </div>
    
<!--
				<div class="form-group">
                  <label for="exampleInputEmail1">Ressort</label>
                  <select class="form-control" name="voting[irRessort]">
	                  <?php 
		                  foreach($irRessorts as $ressort){
			                  ?>
			                  <option value="<?= $ressort["id"] ?>" <?= ($ressort["id"] ==  $data["irRessort"] )?"selected":"" ?>><?= $ressort["title"] ?></option>
			                  <?php
		                  }
	                  ?>
                  </select>
                </div>
-->
            
				
        </div>
       </div><!-- .box -->


<div id="ansprechpartner">      
		<?php 
			

			if(count($data["element"]) > 0){
				foreach($data["element"] as $key => $element){
					$this->get_template_part("bildupload/bildupload_element_templatePart",$element,$key);
				}
			}else{
				$this->get_template_part("bildupload/bildupload_element_templatePart");			

			}
		?>  
	      
    </div>     




	   <div class="box box-primary <?= ($data["prozessing"] >= 0)?"hidden":"" ?>">
        <div class="box-header">
          <h3 class="box-title">Upload
            <small></small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">


    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/file-upload/css/jquery.fileupload.css" type="text/css" />
  


                
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    
                    
        <div style="border:2px dashed #ccc; text-align: center; padding: 30px;">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
                    
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Datei auswählen...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" multiple>
    </span></div>
    
    
    <br>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>         
         
         
 

      
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.fileupload.js"></script>



      </div>
      
</div>
      




       
	   <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Notizen
            <small>werden nicht veröffentlicht</small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">
            <div class="form-group">
             <textarea class="textarea" name="voting[note]"placeholder="Interne Notiz"  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $data["note"] ?></textarea>
            </div>
          </div>
       </div><!-- .box -->	









</div>


<div class="col-md-3" style="position: relative">
	<input type="hidden"  name="voting[id]" id="id" value="<?= $data["id"] ?>" >

	<input type="button"  data-toggle="modal" data-target="#show-produce" value="<?= ($user["autoLive"] == 1)?"Speichern und an InterRED übermitteln":"Speichern" ?>" class="btn btn-success btn-block margin-bottom"  <?= ($data["prozessing"] >= 0)?"disabled":"" ?> />

	
	




</div>

		<div class="modal fade modal-success" id="show-produce">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Produzieren</h4>
              </div>
              <div class="modal-body">
			  		Die Bilder werden an InterRed übermittelt und für <strong>Print</strong>, <strong>Online</strong> und <strong>APP</strong> freigegeben. Nach der Übermittlung kann <strong>KEINE</strong> Änderung mehr vorgenommen werden.
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Abbrechen</button>
				<input type="submit" name="submit" id="submit" value="<?= ($data["id"])?"aktualisieren":"Speichern" ?>" class="btn btn-outline btn-ok" <?= ($data["prozessing"] >= 0)?"disabled":"" ?> />

              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

		</form>

</div>





		<div class="modal fade modal-danger" id="confirm-delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Kunde löschen</h4>
              </div>
              <div class="modal-body">
                <p>Sind Sie sicher das Sie die Bilderstrecke löschen möchten</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Abbrechen</button>
                <a  class="btn btn-outline btn-ok">Löschen</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>






		<div class="modal fade modal-danger" id="show-form-invalid">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Formular nicht vollständig</h4>
              </div>
              <div class="modal-body">
			  		Bitte prüfen Sie das Formular auf Fehler. Daten können nicht gesendet werden.</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Abbrechen</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>







<script><!-- 

  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea.wysihtml").wysihtml5();
  });
	
	
	
	
	
	
	--></script>
	
	
	
<script>
	<!--
		//remove section
	$('#ansprechpartner').on('click', '.remove', function() {
	    //fade out section
		$("#deleteContactButton").data("id",$(this).parent().parent().attr("id"));

	});
	

	
		$('body').on('click', '#deleteContactButton', function() {
			$('#confirm-removeContact').modal('hide');
			
			console.log($(this).data("id"));
			
			var id = "#"+$(this).data("id");
			$(id).fadeOut(300, function(){
		        //remove parent element (main section)
				$(id).remove();

		        return false;
		    });
	    return false;
			
		});

	
	


	-->
	
</script>


<script>
	
(function($) {

	var honorar = "<?= $user["honorar"] ?>";
	var zeitung = "<?= "TV" ?>";
	var fotograf = "<?= (strlen(trim($user["alias"])) > 0)? $user["alias"] : $user["userFirstName"] ." ".$user["userLastName"]  ?>";
	var rechteinhaber = "<?= (strlen(trim($user["alias"])) > 0)?$user["alias"]: $user["userFirstName"] ." ".$user["userLastName"]  ?>";
	
	
    $.fn.cloner = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            template : $(this).find(">:first-child").clone(),
            sectionsCount : <?= count($data["element"]) ?>,
            self:$(this),
            id: "dynamic_"+makeid(),
            iconAdd : "fa-user-plus"

        }, options);

// 		$(this).after('<div class="row" style="margin-bottom: 20px;padding: 10px;"><div class="col-xs-12"><button class="btn btn-success  pull-right" id="addButton_'+settings.id+'"><i class="fa '+settings.iconAdd+'" aria-hidden="true"></i></button></div></div>');
		$(this).after('<div class="modal fade modal-danger" id="confirm-removeContact"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Ansprechpartner löschen</h4></div><div class="modal-body"><p>Sind Sie sich sicher das Sie den Ansprechpartner löschen möchten.</p></div><div class="modal-footer"><button type="button" class="btn btn-success pull-left" data-dismiss="modal">Abbrechen</button><a  class="btn btn-outline btn-removeContact" id="deleteContactButton" data-id="">Löschen</a></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>');
		
		function makeid() {
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for (var i = 0; i < 10; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
			return text;
		}
	    
        var func =  { 
	        
	        init : function(){

	        },
			add : function(){
			    settings.sectionsCount++;
			
			    //loop through each input
			    var section = settings.template.clone().find(':input').each(function(){
			   		this.name= this.name.replace('[0]', '['+settings.sectionsCount+']');
			   		if(this.type === "text") this.value = "";
			   		if(this.type === "email") this.value = "";
			   		if(this.type === "textarea") this.value = "";
			   		if(this.type === "hidden") this.value = "";
			    }).end();
			    
			    var section_id = "box_"+makeid();
			    
			    section.attr("id",section_id);
				settings.self.append(section);
				console.log($("#"+section_id).find(".honorar"));
 				$("#"+section_id).find(".honorar").val(honorar);
 				$("#"+section_id).find(".zeitung").val(zeitung);
 				$("#"+section_id).find(".fotograf").val(fotograf);
 				$("#"+section_id).find(".rechteinhaber").val(rechteinhaber);
 				eval($("#"+section_id).find("script").text());

 				
 				
				return section_id;
			}
		}
        
        
		$('#addButton_' + settings.id).click(function(e) {
			func.add();
			return false;
		});

	
        
        func.init();
        
        return func;
        
  
    }
    

}(jQuery));                                                          
 
$(document).ready(function(){
	
	
	$('#ansprechpartner').on('click', '.copyTitle', function(event) {
	
		$('.imageTitle').val($(this).parent().parent().find('.imageTitle').val());
	
	});
	
		$('#ansprechpartner').on('click', '.copyText', function(event) {
		$('.imageText').val($(this).parent().find('.imageText').val());
	
	});
	
	$.validate({
		lang: 'de'
	});	
	
	$('#show-produce').on('shown.bs.modal', function (e) {

		// Manually load the modules used in this form
		$.formUtils.loadModules('security, date');
		
		// reset error array
		errors = [];
		if( !$("#bilderstreckeForm").isValid() ) {
			console.log( errors );
			$('#show-produce').modal('hide');
			
			$('#show-form-invalid').modal('show');
		} else {
			// The form is valid
		}
	});
	
		var ansprechpartner = $('#ansprechpartner').cloner();
		
		var firstElement = $("#ansprechpartner").find(">:first-child"); //.remove();

		if(firstElement.data("id")==="") firstElement.remove();
		
		$('.addElement').click(function(e) {
			ansprechpartner.add();
			return false;
		});

	$('#submit').click(function(e) {
		$('#show-produce').modal('hide');
		  
		});

		

		$(function () {
		   // 'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = "<?= BASE_URL; ?>static/assets/file-upload/";
		    
		    
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
			    add: function(e, data) {
		                var uploadErrors = [];
		                var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
		                if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
		                    uploadErrors.push('Not an accepted file type');
		                }
		                if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
		                    uploadErrors.push('Filesize is too big');
		                }
		                if(uploadErrors.length > 0) {
		                    alert(uploadErrors.join("\n"));
		                } else {
		                    data.submit();
		                }
			        },
		        done: function (e, data) {
			        
			        console.log(data.result.files);
			        
		            $.each(data.result.files, function (index, file) {
			            
			            
			            var filename = file.name;
			            var filepath = file.url;
			            var filesize = file.size;
			            var filetype = file.type; 
			            
			            $.post( "/admin/media/insertmedia", { mediaFile: filename, mediaPath: filepath, mediaType: filetype, mediaVersion: file })
						  .done(function(data) {
							  if(data.success == true){
							  	console.log(data);
							  	
							  	
								var newBox = ansprechpartner.add();

								$("#"+newBox).removeClass("collapsed-box");
								$("#"+newBox+ " .box-body").toggle();
								
								
//								imageIdField
								$("#"+newBox+ " .box-profile").find("img").attr("src","<?= $config['image_url'] ?>"+ data.mediaPath +"/"+data.mediaFile);
								$("#"+newBox+ " .box-profile").find("img").removeClass("hidden");
								$("#"+newBox+ " .box-profile").toggle();
								$("#"+newBox+ " .box-profile").find("input").val(data.db_id);

								$("#"+newBox+ " .box-profile").find(".imageDelete").removeClass("hidden") ;
								$("#"+newBox+ " .box-profile").find(".imageInput").addClass("hidden") ;



							  }else{
								  alert("Upload Error");
							  }
		/*
						  	var response = $.parseJSON(data);
						  	console.log(response);
		*/
						});
							            
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
});





</script>




<?php include(APP_DIR.'/views/_footer.php'); ?>

