<?php $exists = false;
		if($data){
		 foreach($data as $dataKey => $item){
			 if(in_array($dataKey, array("id","client_id","salutation"))) continue;
		if(strlen($item)> 0) $exists = true;
	}} ?>
	<div class="box ansprechpartner <?= ($exists)?"":"collapsed-box" ?>" >
	

	
			<input type="hidden" name="client[contact][<?= $key ?>][id]" value="<?= $data["id"] ?>">
	
	        <div class="box-header">
	          <h3 class="box-title">Ansprechpartner
	            <small></small>
	          </h3>
	          <!-- tools box -->
	          <div class="pull-right box-tools">
	            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
	              <i class="fa <?= ($exists)?"fa-minus":"fa-plus" ?>"></i></button>
	          </div>
	          <!-- /. tools -->
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body pad" style="<?= ($exists)?"display: block":"display: none" ?>">
		        
	          	<div class="form-group">
	              <label>Anrede</label>
	              <select class="form-control" name="client[contact][<?= $key ?>][salutation]">
	                <option>Herr</option>
	                <option>Frau</option>
	              </select>
	            </div>
		        
				<div class="row">
					<div class="col-xs-6">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Vorname</label>
		                  <input class="form-control" type="text" placeholder="Vorname" name="client[contact][<?= $key ?>][firstname]"  value="<?= $data["firstname"] ?>">
		                </div>	              
					</div>
					<div class="col-xs-6">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Nachname</label>
		                  <input class="form-control" type="text" placeholder="Nachname" name="client[contact][<?= $key ?>][lastname]"  value="<?= $data["lastname"] ?>">
		                </div>	              
					</div>
				</div>
		        
		        
				<?php 
					$hasData = false;
					if(strlen($data["road"].$data["number"].$data["zip"].$data["city"].$data["country"].$data["phone"].$data["email"].$data["mobile"].$data["fax"]) > 0){
					 $hasData = true;
					}
				?>
		        
				<div class="box <?= ($hasData)?"":"collapsed-box" ?>">
			        <div class="box-header">
			          <h3 class="box-title"><i class="fa fa-address-card-o"></i> abweichende Anschrift
			            <small></small>
			          </h3>
			          <!-- tools box -->
			          <div class="pull-right box-tools">
			            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
			              <i class="fa <?= ($hasData)?"fa-minus":"fa-plus" ?>"></i></button>
			          </div>
			          <!-- /. tools -->
			        </div>
			        <!-- /.box-header -->
			        <div class="box-body pad" style="<?= ($hasData)?"display: block":"display: none" ?>">
						<div class="row">
							<div class="col-xs-8">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Straße</label>
				                  <input class="form-control" type="text" placeholder="Straße" name="client[contact][<?= $key ?>][road]" value="<?= $data["road"] ?>">
				                </div>	              
							</div>
							<div class="col-xs-4">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Hausnummer</label>
				                  <input class="form-control" type="text" placeholder="Hausnummer" name="client[contact][<?= $key ?>][number]" value="<?= $data["number"] ?>">
				                </div>	              
							</div>
						</div>
			              
						<div class="row">
							<div class="col-xs-4">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">PLZ</label>
				                  <input class="form-control" type="text" placeholder="PLZ" name="client[contact][<?= $key ?>][zip]" value="<?= $data["zip"] ?>">
				                </div>	              
							</div>
							<div class="col-xs-8">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Ort</label>
				                  <input class="form-control" type="text" placeholder="Ort" name="client[contact][<?= $key ?>][city]" value="<?= $data["city"] ?>">
				                </div>	              
							</div>
						</div>
			            
			            <div class="form-group">
							<label for="exampleInputEmail1">Land</label>
							<input class="form-control" type="text" placeholder="Land" name="client[contact][<?= $key ?>][country]" value="<?= $data["country"] ?>"> 
						</div>
						
						  <div class="row">
				            <div class="col-xs-6">
				       			<div class="form-group">
					                <label for="exampleInputEmail1">Telefon</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-phone"></i>
										</div>
										<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[contact][<?= $key ?>][phone]" value="<?= $data["phone"] ?>">
									</div>
					            </div>
								<div class="form-group">
									<label for="exampleInputEmail1">Email address</label>
									<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="client[contact][<?= $key ?>][email]" value="<?= $data["email"] ?>">
								</div>		            
				            </div>
				            <div class="col-xs-6">
					            <div class="form-group">
					                <label for="exampleInputEmail1">Mobil</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-mobile"></i>
										</div>
										<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[contact][<?= $key ?>][mobile]" value="<?= $data["mobile"] ?>">
									</div>
					            </div>
							    <div class="form-group">
					                <label for="exampleInputEmail1">Fax</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-fax"></i>
										</div>
										<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[contact][<?= $key ?>][fax]" value="<?= $data["fax"] ?>">
									</div>
					            </div>
					            
				            </div>	            
				        </div>
	
						
					</div>
				</div>      
		        
		        
	          			
				<div class="form-group">
					<label for="exampleInputEmail1">Notiz</label>
					<textarea class="form-control" rows="3" placeholder="Enter ..." name="client[contact][<?= $key ?>][note]"><?= $data["note"] ?></textarea>
				</div>
				
				<a class="remove btn btn-danger pull-right"  data-toggle="modal" data-target="#confirm-removeContact"><i class="fa fa-user-times" aria-hidden="true"></i></a>

	
			</div>
	
		</div>   