<?php 
	$client = false;
	if(is_array($data)){
		if(array_key_exists("client", $data)) $client = $data["client"]; 
	}
?>
<p>
	<div class="row">
		<div class="col-xs-8">
            <div class="form-group">
              <label for="exampleInputEmail1">Firma</label>
              <input class="form-control" type="text" placeholder="Firma" name="company"  value="<?= $client["company"] ?>">
            </div>	              
		</div>
		<div class="col-xs-4">
            <div class="form-group">
              <label for="exampleInputEmail1">GP</label>
              <input class="form-control" type="text" placeholder="" name="gp"  value="<?= $client["gp"] ?>">
            </div>	              
		</div>

	</div>	
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<label>Anrede</label>
				<select class="form-control" name="salutation">
					<option <?= ($client["salutation"] == "Herr")?"selected":"" ?>>Herr</option>
					<option <?= ($client["salutation"] == "Frau")?"selected":"" ?>>Frau</option>
				</select>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Vorname</label>
              <input class="form-control" type="text" placeholder="Vorname" name="firstname"  value="<?= $client["firstname"] ?>">
            </div>	              
		</div>
		<div class="col-xs-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Nachname</label>
              <input class="form-control" type="text" placeholder="Nachname" name="lastname"  value="<?= $client["lastname"] ?>">
            </div>	              
		</div>
	</div>		
	<div class="row">
		<div class="col-xs-8">
            <div class="form-group">
              <label for="exampleInputEmail1">Straße</label>
              <input class="form-control" type="text" placeholder="" name="road"  value="<?= $client["road"] ?>">
            </div>	              
		</div>
		<div class="col-xs-4">
            <div class="form-group">
              <label for="exampleInputEmail1">Nr</label>
              <input class="form-control" type="text" placeholder="" name="nr"  value="<?= $client["nr"] ?>">
            </div>	              
		</div>
	</div>		
	<div class="row">
		<div class="col-xs-4">
            <div class="form-group">
              <label for="exampleInputEmail1">PLZ</label>
              <input class="form-control" type="text" placeholder="54290" name="zip"  value="<?= $client["zip"] ?>">
            </div>	              
		</div>
		<div class="col-xs-8">
            <div class="form-group">
              <label for="exampleInputEmail1">Ort</label>
              <input class="form-control" type="text" placeholder="Tier" name="city"  value="<?= $client["city"] ?>">
            </div>	              
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
            <div class="form-group">
              <label for="exampleInputEmail1">Land</label>
              <input class="form-control" type="text" placeholder="" name="country"  value="<?= $client["country"] ?>">
            </div>	              
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
            <div class="form-group">
              <label for="exampleInputEmail1">Mail</label>
              <input class="form-control" type="email" placeholder="" name="email"  value="<?= $client["email"] ?>">
            </div>	              
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Phone</label>
              <input class="form-control" type="text" placeholder="" name="phone"  value="<?= $client["phone"] ?>">
            </div>	              
		</div>
		<div class="col-xs-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Mobil</label>
              <input class="form-control" type="text" placeholder="" name="mobile"  value="<?= $client["mobile"] ?>">
            </div>	              
		</div>
	</div>		

	<div class="row">
		<div class="col-xs-12">
            <div class="form-group" style="border:3px solid red; padding: 15px;">
              <label for="exampleInputEmail1">Status</label>
              <select name="status" class="form-control">
	              <option value="1" <?php if($client["status"] == 1) echo 'selected=""'; ?>>Online</option>
	              <option value="0" <?php if($client["status"] == 0) echo 'selected=""'; ?>>Offline</option>
              </select>
              
            </div>	              
		</div>
	</div>	

</p>