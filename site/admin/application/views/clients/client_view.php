<?php include(APP_DIR.'/views/_header.php'); ?>


<div class="col-md-12" style="margin-top: 10px">
		<form method="post">

<div class="col-md-9">	
	<div class="alert alert-danger alert-dismissible <?= ($error)?"":"hidden"?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i> Alert!</h4>
		<?= $error ?>
	</div>
		
	   <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Kunden Basisdaten
            <small></small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">
	          	<div class="form-group">
                  <label>Kundenart</label>
                  <select class="form-control" id="client_business" name="client[business]">
                    <option value="1" <?= ($data["business"] == 1)?"selected":"" ?> <?= ($data === NULL)?"selected":"" ?>>Firmenkunde</option>
                    <option value="0" <?= ($data["business"] == 0)?"selected":"" ?>>Privatkunde</option>
                  </select>
                </div>

               <div class="form-group" id="client_id_grp">
                  <label for="exampleInputEmail1">Kundennummer</label>
                  
				<div class="input-group">
					<input class="form-control" type="text" placeholder="Kundennummer" id="client_client_id" name="client[client_id]" value="<?= $data["client_id"] ?>">
					<span class="input-group-btn">
						<button type="button" class="btn btn-info btn-flat" id="generateClientId">Generieren</button>
					</span>
				</div>
                  
                  <span class="help-block" style="display: none">Kundennummer schon vorhanden</span>
                </div>	  
                
                <div id="ansprechpartner_firma" style="<?=  ($data["business"] == 0)?"display: none;":"display: block;" ?>">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Firmenname</label>
	                  <input class="form-control" type="text" placeholder="Firmenname" id="client_name" name="client[name]"  value="<?= $data["name"] ?>">
	                </div>
				</div>
                
                <div id="ansprechpartner_privat" style="<?=  ($data["business"] == 1)?"display: none;":"display: block;" ?>">
					<div class="form-group">
						<label>Anrede</label>
						<select class="form-control" name="client[salutation]">
							<option value="Herr">Herr</option>
							<option value="Frau">Frau</option>
						</select>
					</div>
					
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Vorname</label>
								<input class="form-control" type="text" placeholder="Vorname" name="client[firstname]" value="<?= $data["firstname"] ?>">
							</div>	              
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Nachname</label>
								<input class="form-control" type="text" placeholder="Nachname" name="client[lastname]" value="<?= $data["lastname"] ?>">
							</div>	              
						</div>
					</div>
                </div>
				<div class="row">
					<div class="col-xs-8">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Straße</label>
		                  <input class="form-control" type="text" placeholder="Straße"  id="client_street" name="client[road]" value="<?= $data["road"] ?>">
		                </div>	              
					</div>
					<div class="col-xs-4">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Hausnummer</label>
		                  <input class="form-control" type="text" placeholder="Hausnummer" id="client_number" name="client[number]" value="<?= $data["number"] ?>">
		                </div>	              
					</div>
				</div>
	              
				<div class="row">
					<div class="col-xs-4">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">PLZ</label>
		                  <input class="form-control" type="text" placeholder="PLZ" id="client_zip" name="client[zip]" value="<?= $data["zip"] ?>">
		                </div>	              
					</div>
					<div class="col-xs-8">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Ort</label>
		                  <input class="form-control" type="text" placeholder="Ort" id="client_city" name="client[city]" value="<?= $data["city"] ?>">
		                </div>	              
					</div>
				</div>
				
	            <div class="form-group">
					<label for="exampleInputEmail1">Land</label>
					<input class="form-control" type="text" placeholder="Land" id="client_country" name="client[country]" value="<?= $data["country"] ?>">
				</div>
				
			<div class="row">    
	            <div class="col-xs-6">
           			<div class="form-group">
		                <label for="exampleInputEmail1">Telefon</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-phone"></i>
							</div>
							<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[phone]" value="<?= $data["phone"] ?>">
						</div>
		            </div>
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="client[email]" value="<?= $data["email"] ?>">
					</div>		            
	            </div>
	            <div class="col-xs-6">
		            <div class="form-group">
		                <label for="exampleInputEmail1">Mobil</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-mobile"></i>
							</div>
							<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[mobile]" value="<?= $data["mobile"] ?>">
						</div>
		            </div>
				    <div class="form-group">
		                <label for="exampleInputEmail1">Fax</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fax"></i>
							</div>
							<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" name="client[fax]" value="<?= $data["fax"] ?>">
						</div>
		            </div>
		            
	            </div>	            
            </div>

					  
        </div>
       </div>
			<?php 
				$hasData = false;
				if(strlen($data["billing_road"].$data["billing_number"].$data["billing_zip"].$data["billing_city"].$data["billing_country"]) > 0){
				 $hasData = true;
				}
			?>
	<div class="box collapsed-box">
        <div class="box-header">
          <h3 class="box-title">Rechnungsadresse
            <small>falls abweichend</small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad" style="display: none">
			<div class="row">
				<div class="col-xs-8">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Straße</label>
	                  <input class="form-control" type="text" placeholder="Straße" name="client[billing_road]" value="<?= $data["billing_road"] ?>">
	                </div>	              
				</div>
				<div class="col-xs-4">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Hausnummer</label>
	                  <input class="form-control" type="text" placeholder="Hausnummer" name="client[billing_number]" value="<?= $data["billing_number"] ?>">
	                </div>	              
				</div>
			</div>
              
			<div class="row">
				<div class="col-xs-4">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">PLZ</label>
	                  <input class="form-control" type="text" placeholder="PLZ" name="client[billing_zip]" value="<?= $data["billing_zip"] ?>">
	                </div>	              
				</div>
				<div class="col-xs-8">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Ort</label>
	                  <input class="form-control" type="text" placeholder="Ort" name="client[billing_city]" value="<?= $data["billing_city"] ?>">
	                </div>	              
				</div>
			</div>
			
            <div class="form-group">
				<label for="exampleInputEmail1">Land</label>
				<input class="form-control" type="text" placeholder="Land" name="client[billing_country]" value="<?= $data["billing_country"] ?>">
				</div>	              
			</div>
      </div>
    <div id="ansprechpartner">      
		<?php 
			

			if(count($data["contact"]) > 0){
				foreach($data["contact"] as $key => $contact){
					$this->get_template_part("clients/client_ansprechpartner",$contact,$key);
				}
			}else{
				$this->get_template_part("clients/client_ansprechpartner");			

			}
		?>  
	      
    </div>     
    <div class="row" style="margin-bottom: 20px;padding: 10px;"><div class="col-xs-12">     
  	<button class="btn btn-success pull-right" id="newContact" ><i class="fa fa-user-plus" aria-hidden="true"></i></button>
    </div></div>
          
          
    <div class="box collapsed-box">
        <div class="box-header">
          <h3 class="box-title">Zahlungsinformationen
            <small>UST. / Bankdaten / Rabatte</small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad" style="display: none">
	        
			<div class="form-group">
				<label for="exampleInputEmail1">Umst. Ident Nummer</label>
				<input class="form-control" type="text" placeholder="DE/123/456789" name="client[vat]">
			</div>	      

			<div class="form-group">
				<label for="exampleInputEmail1">Kreditinstitut</label>
				<input class="form-control" type="text" placeholder="Kreditinstitut" name="client[bank_name]">
			</div>
	        
			<div class="form-group">
				<label for="exampleInputEmail1">IBAN</label>
				<input class="form-control" type="text" placeholder="IBAN" name="client[iban]">
			</div>
 	        
			<div class="form-group">
				<label for="exampleInputEmail1">BIC</label>
				<input class="form-control" type="text" placeholder="BIC" name="client[bic]">
			</div>
                         
			<div class="form-group">
				<label for="exampleInputEmail1">Rabatt</label>
				<input class="form-control" type="text" placeholder="Rabatt in %" name="client[discount]">
			</div>		
			
		</div>
	</div>          
          
    <div class="box collapsed-box">
        <div class="box-header">
          <h3 class="box-title">Sonstiges
            <small>Notizen</small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad" style="display: none">
	        

			
			<div class="form-group">
				<label for="exampleInputEmail1">Notiz</label>
				<textarea class="form-control" rows="3" placeholder="Enter ..." name="client[note]"></textarea>
			</div>
			
			
		</div>
	</div>    

		<input type="hidden"  name="client[id]" value="<?= $data["id"] ?>">
</div>
<div class="col-md-3" style="position: relative">
	<input type="submit" name="submit" value="<?= ($data["id"])?"aktualisieren":"Speichern" ?>" class="btn btn-primary btn-block margin-bottom" />
	<input type="button" name="submit" data-href="/admin/clients/delete/<?= $data["id"]?>" data-toggle="modal" data-target="#confirm-delete" value="löschen" class="btn btn-danger btn-block margin-bottom <?= ($data["id"])?"":"hidden" ?>" />
</div>
		</form>

</div>


		<div class="modal fade modal-danger" id="confirm-delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Kunde löschen</h4>
              </div>
              <div class="modal-body">
                <p>Sind Sie sicher das Sie den Kunden "<?=  ($data["business"] == 1)? $data["name"]:$data["firstname"]." ".$data["lastname"] ?>" löschen möchten</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Abbrechen</button>
                <a  class="btn btn-outline btn-ok">Löschen</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

		<div class="modal fade modal-danger" id="confirm-removeContact">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Ansprechpartner löschen</h4>
              </div>
              <div class="modal-body">
                <p>Sind Sie sich sicher das Sie den Ansprechpartner löschen möchten.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Abbrechen</button>
                <a  class="btn btn-outline btn-removeContact" id="deleteContactButton" data-id="">Löschen</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>




<script><!-- 
	
	
	$('#confirm-delete').on('show.bs.modal', function(e) {
	    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
	

	
	
	$("#client_business").change(function(){
		
		$("#ansprechpartner_firma").show();
		$("#ansprechpartner_privat").hide();
		
		if(this.value == 0){
			$("#ansprechpartner_firma").hide();
			$("#ansprechpartner_privat").show();
		}

	});
	
	
/*
	$("#newContact").click(function(){
		console.log("klick");
		$( "#ansprechpartner" ).clone().insertAfter( "#ansprechpartner" );
	});
*/


	//define template
	var template = $('#ansprechpartner_0').clone();

	//define counter
	var sectionsCount = <?= count($data["contact"]) ?>;
	
	//add new section
	$('body').on('click', '#newContact', function() {
	
	    //increment
	    sectionsCount++;
	
	    //loop through each input
	    var section = template.clone().find(':input').each(function(){
	   		this.name= this.name.replace('[0]', '['+sectionsCount+']');
	   		if(this.type === "text") this.value = "";
	   		if(this.type === "email") this.value = "";
	   		if(this.type === "textarea") this.value = "";
	   		if(this.type === "hidden") this.value = "";
	    }).end()
	
	    //inject new section
	    .appendTo( "#ansprechpartner" );
	    return false;
	});
	
		$('body').on('click', '#deleteContactButton', function() {
			$('#confirm-removeContact').modal('hide');
			var id = "#"+$(this).data("id");
			$(id).fadeOut(300, function(){
		        //remove parent element (main section)
				$(id).remove();

		        return false;
		    });
	    return false;
			
		});

	
	//remove section
	$('#ansprechpartner').on('click', '.remove', function() {
	    //fade out section
		$("#deleteContactButton").data("id",$(this).parent().parent().attr("id"));

	});
	
	
	$('#client_client_id').on('input', function() {
		
		var client_id = "<?= $data["client_id"] ?>";	
			
		if( client_id === $(this).val()){
		}else{
			$.ajax({
			  type: "POST",
			  url: "/admin/clients/ajax_check_clientID",
	          dataType: "json",
			  data: {"client_id":$(this).val()},
			  success: function(data){
				  if(!data.unique){
					  $("#client_id_grp").addClass("has-error");
					  $("#client_id_grp .help-block").show();
	
				  }else{ 
					$("#client_id_grp").removeClass("has-error");
					$("#client_id_grp .help-block").hide();
				  }
				  
			  }
	
			});
		}

	});

	$('body').on('click', '#generateClientId', function() {
		console.log("click");
		var btn = $(this);
		btn.prop('disabled', true);
		var btnHtml = btn.html();
		btn.html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
		
		$.ajax({
			  type: "POST",
			  url: "/admin/clients/ajax_generate_clientID",
	          dataType: "json",
			  data: {"client_id":$(this).val()},
			  success: function(data){
				  $('#client_client_id').val(data.client_id);
				  		btn.prop('disabled', false);
				  		btn.html(btnHtml);
					$("#client_id_grp").removeClass("has-error");
					$("#client_id_grp .help-block").hide();

			  }		  
	
			});
		
		
	});
	
	--></script>

            

<?php include(APP_DIR.'/views/_footer.php'); ?>