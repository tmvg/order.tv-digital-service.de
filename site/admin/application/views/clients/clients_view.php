<?php include(APP_DIR.'/views/_header.php'); ?>

<div class="col-md-12" style="margin-top: 10px">
<div class="col-md-9">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Kunden</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="table-responsive mailbox-messages">
				<table id="orders" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Firma</th>
							<th>GP</th>
							<th>Name</th>
							<th>Nachname</th>
							<th>PLZ</th>
							<th>Stadt</th>
						</tr>
					</thead>
				</table>


                <table class="table table-hover table-striped">
                  <tbody>
	                  
	             <?php 
					
					foreach($clients as $client){
						?>
<!--
						<pre>
							<?php var_dump($client) ?>
						</pre>
-->
		                  <tr>
		                    <td class="mailbox-star"><a href="#"><?php if($client["business"] == 1){?><i class="fa fa-building-o" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-user" aria-hidden="true"></i>
<?php } ?> </a></td>
		                    <td class="mailbox-name"><a href="/admin/clients/client/<?= $client["id"] ?>"><?= ($client["business"] == 1)? $client["name"] : $client["firstname"] ." ". $client["lastname"] ?></a></td>
		                    <td class="mailbox-subject"><b><?= $client["road"] ?> <?= $client["number"] ?></b> <?= $client["zip"] ?> <?= $client["city"] ?>
		                    </td>
		                  </tr>					 
					 <?php 
					}
		             
	             ?>     
	              </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
             </div>
          </div>
          <!-- /. box -->
        </div>
	<div class="col-md-3">
		<a href="/admin/clients/client" class="btn btn-primary btn-block margin-bottom">Kunde anlegen</a>
    </div>

</div>        
<?php include(APP_DIR.'/views/_footer.php'); ?>