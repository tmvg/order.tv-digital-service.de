<?php include(APP_DIR.'/views/_header.php'); ?>
<form method="post" action="/admin/clients/save">
	<div class="col-md-12" style="margin-top: 10px">
		<div class="col-md-9">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Kunde bearbeiten</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<?php  $this->get_template_part("clients/client_form",array("client" => $data)) ?>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			</div>
		<div class="col-md-3">
			
			<div class="box box-primary">
		        <div class="box-header">
		          <h3 class="box-title">Aktionen
		            <small></small>
		          </h3>
		        </div>
				<div class="box-body padding ">
					<div class="row text-center">
						<div class="col-xs-12">
							<p>
								<?php if(array_key_exists("id", $data)){ ?><input type="hidden" name="id" value="<?= $data["id"] ?>" /><?php } ?>
								<input type="submit" class="btn btn-success" value="Speichern" style="width: 100%"  />
							</p>
						</div>				
					</div>
				</div>
				<!-- /.box-body -->
			</div>		

			
		</div>
	
	</div>       
</form>
<script>
	$(function () {
		
	 
		  
	
	});
</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>