<?php include(APP_DIR.'/views/_header.php'); ?>
<div class="col-md-12" style="margin-top: 10px">
	<div class="col-md-9">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Kunden</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Firma</th>
							<th>GP</th>
							<th>Name</th>
							<th>Nachname</th>
							<th>PLZ</th>
							<th>Stadt</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		</div>
	<div class="col-md-3">
		<a href="/admin/clients/edit" class="btn btn-primary btn-block margin-bottom">Kunde anlegen</a>
	</div>

</div>        
<script>
$(function () {
	
 
	  
   var table =  $('#orders').DataTable({
		
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
		"ajax": "/admin/clients/ajax_clientTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'},
			{'data':'4'},
			{'data':'5'},
		]
    });
    
    $('#orders').on( 'click', 'tr', function () {
		var id = table.row( this ).data()[6];
		location.href='/admin/clients/edit/'+ id;

	});
    
  });
  

</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>

