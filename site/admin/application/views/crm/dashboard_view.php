<?php include(APP_DIR.'/views/_header.php'); ?>


<style>
.info-box.active, .dateSelector.active{
	border-top: 3px solid red; 
}
</style>
<div class="col-xs-12" style="margin-top: 10px;">
	
	Zeitraum auswählen: <button class="btn btn-default btn-small dateSelector" data-start="<?= date('Y',strtotime("this year") )?>-01-01 00:00:00" data-end="<?= date('Y',strtotime("this year")) ?>-12-31 23:59:59"><?= date("Y", strtotime("this year")); ?></button>
						<button class="btn btn-default btn-small dateSelector" data-start="<?= date('Y',strtotime("last year") )?>-01-01 00:00:00" data-end="<?= date('Y',strtotime("last year")) ?>-12-31 23:59:59"><?= date("Y", strtotime("last year")); ?></button>
						<button class="btn btn-default btn-small dateSelector" data-start="<?= date('Y',strtotime("2 years ago") )?>-01-01 00:00:00" data-end="<?= date('Y',strtotime("2 years ago")) ?>-12-31 23:59:59"><?= date("Y", strtotime("2 years ago")); ?></button>
						<button class="btn btn-default btn-small dateSelector active" data-start="0" data-end="0">all time</button>
	<br>
<br>
</div>	

        <div class="col-md-2 col-sm-6 col-xs-12">
          <div class="info-box" id="OffeneAngebote">
            <span class="info-box-icon bg-aqua"><i class="fa fa-spinner"></i></span>

            <div class="info-box-content" >
              <span class="info-box-text">Offen</span>
              <span class="info-box-number" id="kpi_1"><?= $kpi_1 ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-2 col-sm-6 col-xs-12">
          <div class="info-box" id="AuftragErhalten">
            <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">erhalten</span>
              <span class="info-box-number" id="kpi_2"><?= $kpi_2 ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-2 col-sm-6 col-xs-12">
          <div class="info-box" id="auftragAbgeschlossen">
            <span class="info-box-icon bg-green"><i class="fa fa-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Fertig</span>
              <span class="info-box-number" id="kpi_3"><?= $kpi_3 ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-2 col-sm-6 col-xs-12">
          <div class="info-box" id="AbgelehntAufträge">
            <span class="info-box-icon bg-grey"><i class="fa fa-thumbs-o-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Abgelehnt</span>
              <span class="info-box-number" id="kpi_0"><?= $kpi_0 ?></span>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-md-2 col-sm-6 col-xs-12">
          <div class="info-box active" id="AlleAuftraege">
            <span class="info-box-icon bg-yellow"><i class="fa fa-globe"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Alle</span>
              <span class="info-box-number" id="kpi_all"><?= $kpi_0 + $kpi_1 + $kpi_2 + $kpi_3 ?></span>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        
        <!-- /.col -->


<div class="col-md-12" style="margin-top: 10px">
	
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">CRM Dashboard</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="orders" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Firma</th>
							<th>GP</th>
							<th>Angebot</th>
							<th>Status</th>
							<th>Preis</th>
							<th>Datum</th>
							<th>fertig bis</th>
							<th>Typ</th>
							<th>Projekt</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		</div>

<script>

var jsondata = "";

$(document).ready(function(){
  $('#download').click(function(){
     var data = $('#csvdata').val();
     if(data == '')
      return;
      JSONToCSVConvertor(data, "CSVExport", true);
   });
});
	
function msieversion() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return true
  {
    return true;
  } else { // If another browser,
  return false;
  }
  return false;
}
	
function JSONToCSVConvertor(JSONData,fileName,ShowLabel) {
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var CSV = '';
    if (ShowLabel) {
        var row = "";
        for (var index in arrData[0]) {
            row += index + ',';
        }
        row = row.slice(0, -1);
        CSV += row + '\r\n';
    }
    for (var i = 0; i > arrData.length; i++) {
        var row = "";
        for (var index in arrData[i]) {
            var arrValue = arrData[i][index] == null ? "" : '="' + arrData[i][index] + '"';
            row += arrValue + ',';
        }
        row.slice(0, row.length - 1);
        CSV += row + '\r\n';
    }
    if (CSV == '') {
        growl.error("Invalid data");
        return;
    }
    var fileName = "Result";
    if(msieversion()){
        var IEwindow = window.open();
        IEwindow.document.write('sep=,\r\n' + CSV);
        IEwindow.document.close();
        IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
        IEwindow.close();
    } else {
        var uri = 'data:application/csv;charset=utf-8,' + escape(CSV);
        var link = document.createElement("a");
        link.href = uri;
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}	
	
$(function () {
	
	var status  = 9999;
	var date = [0,0];
	
	getVarsFromCookie();

	function getVarsFromCookie(){
		var savedVars = Cookies.getJSON('<?= md5($this->getTemplate()) ?>');
		if(savedVars){
			status = savedVars.status;
			date = savedVars.date;



			$(".info-box").removeClass("active");
			$(".dateSelector").removeClass("active");

			console.log(date);

			$(".dateSelector").each(function(){
				console.log($(this).data("start"));
				if($(this).data("start") === date[0] && $(this).data("end") === date[1]) $(this).addClass("active");
				
			})
			
			

			if(status == 1) $("#OffeneAngebote").addClass("active");
			if(status == 2) $("#AuftragErhalten").addClass("active");
			if(status == 3) $("#auftragAbgeschlossen").addClass("active");
			if(status == -1) $("#AbgelehntAufträge").addClass("active");
			if(status == 9999) $("#AlleAuftraege").addClass("active");

			
		}
		console.log(savedVars);
	}
	
	function saveVars(){
		Cookies.set('<?= md5($this->getTemplate()) ?>', {status:status,date:date});
	}
	
	$(document).on("click",".dateSelector",function(){

		date = [$(this).data("start"),$(this).data("end")];
		
		$(".dateSelector").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();
	});
 
	
 	$(document).on("click","#OffeneAngebote",function(){
		status = 1;

		$(".info-box").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();


	});
 
 	$(document).on("click","#AuftragErhalten",function(){
		status = 2;
		$(".info-box").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();

	});

 	$(document).on("click","#auftragAbgeschlossen",function(){
		status = 3;
		$(".info-box").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();

	});

 	$(document).on("click","#AbgelehntAufträge",function(){
		status = -1;
		$(".info-box").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();

	});

 	$(document).on("click","#AlleAuftraege",function(){
		status = 9999;
		$(".info-box").removeClass("active");
		$(this).addClass("active");
		$('#orders').DataTable().ajax.reload();
		saveVars();

	});	
	
 
	  
   var table = $('#orders').DataTable({
		"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 4, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        "buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],
        
        "ajax": {
			"url": "/admin/dashboard/ajax_crmTable/call",
			"data": function( d ) {
				d.status_value = status;
				d.daterange = date;
				
    		},
    		  "dataSrc": function ( json ) {
                //Make your callback here.
				
				if(json.kpi[1] !== undefined){$("#kpi_1").html(json.kpi[1])}
				if(json.kpi[2] !== undefined){$("#kpi_2").html(json.kpi[2])}
				if(json.kpi[3] !== undefined){$("#kpi_3").html(json.kpi[3])}
				if(json.kpi[0] !== undefined){$("#kpi_0").html(json.kpi[0])}
				$("#kpi_all").html( json.kpi[0] + json.kpi[1] + json.kpi[2] + json.kpi[3]);
				
                return json.data;
            }   
		},


    		


 

		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		
		

		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'},
			{'data':'4'},
			{'data':'5'},
			{'data':'6'},
			{'data':'7'},
			{'data':'8'},


		]
		

    });
    
/*
	table.on( 'draw', function () {
    	console.log( table.ajax.json());
    	
	});
*/
    
    
    $('#orders').on( 'click', 'tr', function () {
		var id = table.row( this ).data()[9];
		location.href='/admin/offer/edit/'+ id;

	});
    
  });
  




</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>

