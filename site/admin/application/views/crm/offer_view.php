<?php include(APP_DIR.'/views/_header.php'); ?>


<form method="post" action="/admin/offer/save">
	<?php 

		 if(array_key_exists("id",$data)){
			 ?>
			 <input type="hidden" value="<?= $data["id"] ?>" name="crm[id]" />
			 <?php
		 }
		
	?>
	<div class="col-md-12" style="margin-top: 10px">
		
		<h2>Angebot</h2>
<!-- <pre><?php var_dump($data) ?></pre> -->
		<div class="row">
		
			<div class="col-md-9">
				
				<div class="box box-danger">
				    <div class="box-header with-border">
				      <h3 class="box-title">Aktivität eintragen</h3>
				    </div>
				    <div class="box-body">
					    <div class="row">
				            <div class="form-group col-md-6">
				                <label>Kunde</label>
				                <div class="input-group">
					                <select class="form-control select2 select2-hidden-accessible selectClient" style="width: 100%;" tabindex="-1"  name="crm[client]" placeholder="Kunde" >
						                <?php 
							                if(array_key_exists("company",$data)){
								                ?>
								                <option value="<?= $data["company"]["id"] ?>" selected><?= $data["company"]["company"] ?> (<?= $data["company"]["gp"] ?>)</option>
								                <?php
							                }
						                ?>
					                </select>
				                    <span class="input-group-btn">
				                      <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#newClient">Neukunde</button>
				                    </span>
								</div>
				        	</div>
				        	
							<div class="form-group col-md-6">
				                <label>Projekt</label>
				                <div class="input-group">
					                <select class="form-control select2 select2-hidden-accessible selectProject" style="width: 100%;" tabindex="-1"  name="crm[project]" placeholder="Projekt" >
										<?php 
							                if(array_key_exists("project",$data)){
								                ?>
								                <option value="<?= $data["project"]["id"] ?>" selected><?= $data["project"]["project_name"] ?></option>
								                <?php
							                }
						                ?>
					                </select>
				                    <span class="input-group-btn">
				                      <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#newProject">Neues Projekt</button>
				                    </span>
								</div>
				        	</div>
					    </div>
					    
				    <div class="row">    		
					        <div class="form-group col-md-12">
				                <label>Angebotstitel</label>
				                <input class="form-control" name="crm[crm_title]" placeholder="Was wurde angeboten?" type="text" value="<?= $data[crm_title] ?>">
				        	</div>
						        	
						</div>					    
					    
					    <div class="row">    		
					        <div class="form-group col-md-6">
				                <label>letzte Aktualisierung: <?php if($data["date"]) echo date("d.m.Y H:i", strtotime($data["date"]));?></label>
				                <input class="form-control" name="crm[date]" placeholder="dd.mm.YYYY" type="text" value="<?php
					                 echo date("d.m.Y H:i", time());
					                 ?>">
				        	</div>
							<div class="form-group col-md-6">
				                <label>Geplantes Enddatum</label>
				                <input class="form-control" name="crm[final_date]" placeholder="dd.mm.YYYY" type="text" value="<?= (array_key_exists("crm_final_date",$data))?date("d.m.Y", strtotime($data["crm_final_date"])) : date("d.m.Y", time()) ?>">
				        	</div>
				        	
						</div>
			        	<div class="row">

							<div class="form-group col-md-4">
				                <label>Type</label>
					                <select class="form-control" style="width: 100%;" tabindex="-1"  name="crm[type]">
						               <?php foreach($crm_types as $crm_type){
							               $selected = "";
							               
							               if(array_key_exists("crm_type",$data)){
								               if($data["crm_type"]["id"] == $crm_type["id"]) $selected = "selected";
							               }
							               
							               ?>
							               <option value="<?= $crm_type["id"] ?>" <?= $selected ?>><?= $crm_type["type"] ?></option>
							               <?php
						               } ?>
					                </select>
				        	</div>
				        	<div class="form-group col-md-4">
				                <label>Status</label>
								<select class="form-control select2 select2-hidden-accessible selectCRMStatus" style="width: 100%;" tabindex="-1"  name="crm[status]" placeholder="CRM Status" data-id="">
										<?php 
							                if(array_key_exists("status",$data)){
								                ?>
								                <option value="<?= $data["status"]["id"] ?>" selected><?= $data["status"]["status"] ?></option>
								                <?php
							                }
						                ?>
								</select>
				        	</div>
				        	<div class="form-group col-md-4">
				                <label>Auftragswert</label>
				                <input class="form-control" name="crm[price]" placeholder="Auftragswert in EUR" type="text" value="<?= $data["price"] ?>">
				        	</div>
			        	</div>
						<div class="row">
				        	<div class="form-group col-md-12">
				                <label>Notiz</label>
								<textarea class="form-control" name="crm[note]" style="height: 150px"></textarea>
							</div>
						</div>
						
			
					</div>
				</div>
				
				
				<div class="box box-default">
				    <div class="box-header with-border">
				      <h3 class="box-title">Verlauf</h3>
				    </div>
				    <div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<!-- The time line -->
								<ul class="timeline">
									
									<?php 
										
										if(array_key_exists("notes", $data)){
											$lastDate = false;
											
											foreach($data["notes"] as $note){ 
												
												if($lastDate !== date("d.m.Y", strtotime($note["date"]))){
													$lastDate = date("d.m.Y",strtotime($note["date"]));
													?>
												<!-- timeline time label -->
												<li class="time-label">
													<span class="bg-red"><?= date("d.m.Y", strtotime($note["date"])) ?></span>
												</li>
											<?php
												}
											?>
											
												<li>
													<i class="fa fa-comment bg-blue"></i>
													<div class="timeline-item">
														<span class="time"><i class="fa fa-clock-o"></i> <?= date("H:i" , strtotime($note["date"]))?></span>
														<h3 class="timeline-header"><a href="#"><?= $note["crm_user"]["userFirstName"] ?> <?= $note["crm_user"]["userLastName"] ?></a></h3>
														<div class="timeline-body">
															<?= nl2br($note["note"]) ?>
														</div>
													</div>
												</li>
												<!-- END timeline item -->
											
											<?php	
											} 
											
										}
									?>
									
				
								</ul>
							</div>
						</div>

					</div><!-- box-body -->
				</div>
	
	
	
			</div>
	
			<div class="col-md-3">

				<?php if(array_key_exists("id",$data)){ ?>
					<div class="info-box">
						<span class="info-box-icon bg-aqua"><i class="fa fa-hashtag" aria-hidden="true"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">CRM ID</span>
							<span class="info-box-number"><?= $data["id"] ?></span>
						</div>
						<!-- /.info-box-content -->
					</div>
				 <?php } ?>

			<div class="box box-primary">
		        <div class="box-header">
		          <h3 class="box-title">Aktionen
		            <small></small>
		          </h3>
		        </div>
				<div class="box-body padding ">
					<div class="row text-center">
						<div class="col-xs-12">
							<p>
				
								
							</p>
							<p>	
								<input type="submit" class="btn btn-success" value="Speichern" style="width: 100%"/>
							</p>
						</div>				
					</div>
				</div>
				<!-- /.box-body -->
			</div>		


				

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <img class="profile-user-img img-responsive img-circle <?=($data["company"]["salutation"] == "Herr")?"":"hidden" ?>"  src="/static/images/user.png" alt="User profile picture" id="client_contact_herr">
              <img class="profile-user-img img-responsive img-circle <?=($data["company"]["salutation"] == "Frau")?"":"hidden" ?>"  src="/static/images/user_f.png" alt="User profile picture" id="client_contact_frau">
              <img class="profile-user-img img-responsive img-circle <?=($data["company"]["salutation"] != "Herr" && $data["company"]["salutation"] != "Frau")?"":"hidden" ?>"  src="/static/images/user_none.png" alt="User profile picture" id="client_contact_firma">

              <h3 class="profile-username text-center" id="client_contact_name"><?= $data["company"]["firstname"]." ".$data["company"]["lastname"] ?></h3>
              <p class="text-muted text-center" id="client_contact_adress">
	              <?= $data["company"]["road"]." ".$data["company"]["nr"] ?><br>
	              <?= $data["company"]["zip"]." ".$data["company"]["city"] ?>
              </p>
              <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                  <b>Telefon: </b><span  id="client_contact_phone"><?= $data["company"]["phone"] ?></span>
                </li>
                <li class="list-group-item">
                  <b>Mobil: </b><span  id="client_contact_mobile"><?= $data["company"]["mobile"] ?></span>
                </li>
                <li class="list-group-item">
                  <b>E-Mail: </b><span  id="client_contact_email"><?= $data["company"]["email"] ?></span>
                </li>
              </ul>

			  	<?php if($data["company"]["email"]) echo '<a href="mailto:'.$data["company"]["email"].'" class="btn btn-primary btn-block"><b>E-Mail senden</b></a>';  ?>
			  	
			  	
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

   

        
				
				<?php  $this->get_template_part("media/media_select_data",array("name" => "crm[media]",'title' => "Medien","crm_files" => $data["crm_files"]))  ?>
			</div>
		</div>
	</div>
</form>       






<!-- MODALS START -->
	<div class="modal fade " id="newClient">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="newCustomerForm" class="ajaxForm" action="<?= $config["base_url"] ?>order/ajax_add_client/query/" data-callback="updateCustomerForm" data-id="newClient" data-update="selectClient">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">neuer Kunde</h4>
				</div>
				<div class="modal-body">
					<?php  $this->get_template_part("clients/client_form") ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">schlie&szligen</button>
					<button type="submit" class="btn  pull-right btn-success saveContactData" >Speichern</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
		<script>
			
			$(".ajaxForm").submit(function(e) {
				e.preventDefault();
				var formData =  $(this).serializeArray().reduce((a, x) => ({ ...a, [x.name]: x.value }), {});
				var action = $(this).attr("action");
				var senderModal = $(this).data("id");
				var updateTarget = "."+$(this).data("update");
				
				
				$.post(action, {
				    dataType: "json",
				    data:formData,
				}).done(function(data) { 
					
					if(data.status){
						
						$(updateTarget).html("<option value='"+data.data.id+"'>"+data.data.company+" ("+data.data.gp+")</option>");
						
					    $('#'+senderModal).modal('toggle');
						
					}else{

						alert(data.error);
					}
					
				});

				
			});
			
		</script>
		
	</div>


<!-- MODALS END -->




<!-- MODALS START -->
	<div class="modal fade " id="newProject">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="newCustomerForm" class="ajaxFormProject" action="<?= $config["base_url"] ?>offer/ajax_add_project/query/" data-callback="updateProjectForm" data-id="newProject" data-update="selectProject">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">neues Projekt</h4>
				</div>
				<div class="modal-body">
					<p>
						<div class="row">
							<div class="col-xs-12">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Name</label>
				                  <input class="form-control" type="text" placeholder="Projekt Name" name="project_name"  value="">
				                </div>	              
							</div>
						</div>	
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">schlie&szligen</button>
					<button type="submit" class="btn  pull-right btn-success saveContactData" >Speichern</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
		<script>
			
			$(".ajaxFormProject").submit(function(e) {
				e.preventDefault();
				var formData =  $(this).serializeArray().reduce((a, x) => ({ ...a, [x.name]: x.value }), {});
				var action = $(this).attr("action");
				var senderModal = $(this).data("id");
				var updateTarget = "."+$(this).data("update");

				
				$.post(action, {
				    dataType: "json",
				    data:formData,
				}).done(function(data) { 
					
					if(data.status){
						console.log(data);
						$(updateTarget).html("<option value='"+data.data.id+"'>"+data.data.project_name+"</option>");
						
					    $('#'+senderModal).modal('toggle');
						
					}else{

						alert(data.error);
					}
					
				});

				
			});
			
		</script>
		
	</div>


<!-- MODALS END -->




<script>
	


	
	$(function(){
    	$(".selectCRMStatus").select2({
			ajax: {
				url: '<?= $config["base_url"] ?>offer/ajax_crm_status/query',
			    dataType: 'json',

				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
			      // Tranforms the top-level key of the response object from 'items' to 'results'
				  return {
			        results: data.result
			      };
			    }
			}
		});
	});
	
	
	$(function(){
    	$(".selectClient").select2({
		
			ajax: {
				url: '<?= $config["base_url"] ?>order/ajax_find_client/query',
			    dataType: 'json',

				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
			      // Tranforms the top-level key of the response object from 'items' to 'results'
				  return {
			        results: data.result
			      };
			    }
			}
		}).on('select2:select', function(event) {
			var data = event.params.data;
			
			$("#client_contact_name").html(data.firstname + " " + data.lastname);
			$("#client_contact_phone").html(data.phone);
			$("#client_contact_mobile").html(data.mobile);
			$("#client_contact_email").html(data.email);
		
			$(".profile-user-img").addClass("hidden");
			
			if(data.salutation == "Herr"){
				$("#client_contact_herr").removeClass("hidden");
			}else{
				if(data.salutation == "Frau"){
					$("#client_contact_frau").removeClass("hidden");				
				}else{
					$("#client_contact_firma").removeClass("hidden");
				}
			}
			
			$("#client_contact_adress").html(data.road + " " + data.nr + "<br />"+data.zip + " " + data.city )
		
		
		
		});
	});

	
	$(function(){
    	$(".selectProject").select2({
		
			ajax: {
				url: '<?= $config["base_url"] ?>offer/ajax_find_project/query',
			    dataType: 'json',

				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
			      // Tranforms the top-level key of the response object from 'items' to 'results'
				  return {
			        results: data.result
			      };
			    }
			}
		});
	});	
	
	
	
	
	
	
	$(document).on('click', '#add', function(){

		

		var jqxhr = $.getJSON( "<?= $config["base_url"] ?>order/ajax_get_new_position/getData", function() {
			  console.log( "success" );
		})
			  .done(function(data) {
				  
				  if(data.status){
				  		$("#positionen").append(data.content);
					}
			  })
			  .fail(function() {
// 			    console.log( "error" );
			  })
			  .always(function() {
// 			    console.log( "complete" );
			  });
			 
			 
				
	
	});

	

</script>






<?php include(APP_DIR.'/views/_footer.php'); ?>