<?php include(APP_DIR.'/views/_header.php'); ?>


<div class="col-md-12" style="margin-top: 10px">

<form method="post">

<div class="col-md-9">	
	<div class="alert alert-danger alert-dismissible <?= ($error)?"":"hidden"?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i> Alert!</h4>
		<?= $error ?>
	</div>
		
	   <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Warengruppe bearbeiten
            <small></small>
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">

           <div class="form-group" id="group_id_grp">
				<label for="exampleInputEmail1">Warengruppen Nummer</label>
				<div class="input-group">
				<?php  
					$ancestorIdArray = array();
					if(is_array($ancestors)){
						foreach($ancestors as $ancestor){
							$ancestorIdArray[] = $ancestor["group_id"];
						}
					}
				?>
					<span class="input-group-addon " id="ancestors" style="<?= (count($ancestors) <= 0)?"display:none":"" ?>">
						<?= implode("-", $ancestorIdArray) ?>
					</span>
					<input class="form-control" type="text" placeholder="Kundennummer" id="group_id" name="group[group_id]" value="<?= $data["group_id"] ?>">
					<span class="input-group-btn">
						<button type="button" class="btn btn-info btn-flat" id="generateGroupId">Generieren</button>
					</span>
				</div>
				<span class="help-block" style="display: none">Warengruppen Nummer schon vorhanden</span>
            </div>	  
            
            <div id="ansprechpartner_firma">
                <div class="form-group">
                  <label for="exampleInputEmail1">Warengruppe Name</label>
                  <input class="form-control" type="text" placeholder="Firmenname" id="group_name" name="group[name]"  value="<?= $data["name"] ?>">
                </div>
			</div>
            
            
    <div class="box collapsed-box">
        <div class="box-header">
          <h3 class="box-title">Übergeordnete Warengruppe: <span class="strong" id="selectedGroupName"><?= ($data["parent_name"])?$data["parent_name"]:"keine" ?></span>
	         
          </h3>
          <input type="hidden" name="group[parent]" id="parent" value="<?= $data["parent"] ?>" />
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad" style="display: none">
			<div class="row">
				<div class="col-xs-12">
	                <div id="tree"></div>
				</div>
			</div>
		</div>
    </div>
                
                       

					  
        </div>
       </div>
	

</div>


<div class="col-md-3" style="position: relative">
	<input type="hidden"  name="group[id]" id="id" value="<?= $data["id"] ?>">
	<input type="submit" name="submit" value="<?= ($data["id"])?"aktualisieren":"Speichern" ?>" class="btn btn-primary btn-block margin-bottom" />
	<input type="button" name="submit" data-href="/admin/groups/delete/<?= $data["id"]?>" data-toggle="modal" data-target="#confirm-delete" value="löschen" class="btn btn-danger btn-block margin-bottom <?= ($data["id"])?"":"hidden" ?>" />
</div>
		</form>

</div>


		<div class="modal fade modal-danger" id="confirm-delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Kunde löschen</h4>
              </div>
              <div class="modal-body">
                <p>Sind Sie sicher das Sie die Warengruppe "<?= $data["name"] ?>" löschen möchten</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Abbrechen</button>
                <a  class="btn btn-outline btn-ok">Löschen</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


<script><!-- 
	
	
	var tree = <?= json_encode($tree) ?> 

	$('#tree').treeview({data: tree,levels:1,showTags: true,
		
		onNodeSelected: function(event, data) {

			if($("#id").val() === data.id){
				$('#tree').treeview('unselectNode', [ data.nodeId, { silent: true } ]);
			}else{
				$("#selectedGroupName").html(data.text);
				$("#ancestors").html(data.tags);
				$("#ancestors").show();
				$("#parent").val(data.id);
				checkID($("#group_id").val());

			}
		}
	});

	
	
	$('#confirm-delete').on('show.bs.modal', function(e) {
	    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
	

	
	
	$("#client_business").change(function(){
		
		$("#ansprechpartner_firma").show();
		$("#ansprechpartner_privat").hide();
		
		if(this.value == 0){
			$("#ansprechpartner_firma").hide();
			$("#ansprechpartner_privat").show();
		}

	});
	
	
/*
	$("#newContact").click(function(){
		console.log("klick");
		$( "#ansprechpartner" ).clone().insertAfter( "#ansprechpartner" );
	});
*/


	//define template
	var template = $('#ansprechpartner_0').clone();

	//define counter
	var sectionsCount = <?= count($data["contact"]) ?>;
	

	
	//remove section
	$('#ansprechpartner').on('click', '.remove', function() {
	    //fade out section
		$("#deleteContactButton").data("id",$(this).parent().parent().attr("id"));

	});
	
	
	$('#group_id').on('input', function() {
		
		var group_id = "<?= $data["group_id"] ?>";	
			
		if( group_id === $(this).val()){
		}else{
			checkID($(this).val());
/*
			$.ajax({
			  type: "POST",
			  url: "/admin/groups/ajax_check_groupID",
	          dataType: "json",
			  data: {"group_id":$(this).val(),"parent":$("#parent").val()},
			  success: function(data){
				  if(!data.unique){
					  $("#group_id_grp").addClass("has-error");
					  $("#group_id_grp .help-block").show();
	
				  }else{ 
					$("#group_id_grp").removeClass("has-error");
					$("#group_id_grp .help-block").hide();
				  }
				  
			  },
			  error: function(){
				  alert("Verbindung nicht möglich");
			  		btn.prop('disabled', false);
			  		btn.html(btnHtml);

			  }		
	
			});
*/
		}

	});

	function checkID($id){
		
			$.ajax({
			  type: "POST",
			  url: "/admin/groups/ajax_check_groupID",
	          dataType: "json",
			  data: {"group_id":$id,"parent":$("#parent").val()},
			  success: function(data){
				  if(!data.unique){
					  $("#group_id_grp").addClass("has-error");
					  $("#group_id_grp .help-block").show();
	
				  }else{ 
					$("#group_id_grp").removeClass("has-error");
					$("#group_id_grp .help-block").hide();
				  }
				  
			  },
			  error: function(){
				  alert("Verbindung nicht möglich");
			  		btn.prop('disabled', false);
			  		btn.html(btnHtml);

			  }		
	
			});
		
	}

	$('body').on('click', '#generateGroupId', function() {
		console.log("click");
		var btn = $(this);
		btn.prop('disabled', true);
		var btnHtml = btn.html();
		btn.html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
		
		$.ajax({
			  type: "POST",
			  url: "/admin/groups/ajax_generate_groupID",
	          dataType: "json",
			  data: {"group_id":$(this).val()},
			  success: function(data){
				  $('#group_id').val(data.group_id);
			  		btn.prop('disabled', false);
			  		btn.html(btnHtml);
					  $("#group_id_grp").removeClass("has-error");
					  $("#group_id_grp .help-block").hide();

			  },
			  error: function(){
				  alert("Verbindung nicht möglich");
			  		btn.prop('disabled', false);
			  		btn.html(btnHtml);

			  }		  
	
			});
		
		
	});
	
	--></script>

            

<?php include(APP_DIR.'/views/_footer.php'); ?>