<?php include(APP_DIR.'/views/_header.php'); ?>

<div class="col-md-12" style="margin-top: 10px">

<div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Warengruppen</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
					<div class="row">
						<div class="col-xs-12">
							<div id="tree"></div>
						</div>
					</div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
             </div>
          </div>
          <!-- /. box -->


    </div>
	<div class="col-md-3">
		<a href="/admin/groups/group" class="btn btn-primary btn-block margin-bottom">Warengruppe anlegen</a>
    </div>

</div>        

<script><!-- 


	


	var tree = <?= json_encode($tree) ?> 

	$('#tree').treeview({data: tree, showTags: true,

		

		onNodeSelected: function(event, data) {
			console.log(data);
			window.location.href = "/admin/groups/group/"+data.id;

		}

	});

	
	--></script>
<?php include(APP_DIR.'/views/_footer.php'); ?>