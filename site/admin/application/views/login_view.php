<?php include('_headerLogin.php'); ?>
<style>

	.login-box, .register-box {
    width: 360px;
    margin:   7% auto 2% auto;
}
	
</style>	
  <div class="login-box">
  <div class="login-logo">
    <?= $config['title']; ?>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
	  
	   
	  
    <p class="login-box-msg">Bitte melden Sie sich an</p>
	<?= $PAGEINFO ?> 
    <form action="/admin/login" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="E-Mail">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Passwort">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Anmelden</button>
        </div>
        
                        
        <!-- /.col -->
      </div>
    </form>




    <!-- <a href="#">Ich habe mein Passwort vergessen</a> -->

  </div>
  <!-- /.login-box-body -->
  
  
</div>
<div id="virtualKeyboard" class="hidden"></div>

<!-- /.login-box -->
<?php include('_footerLogin.php'); ?>