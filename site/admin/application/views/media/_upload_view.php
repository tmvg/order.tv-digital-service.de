<?php include(APP_DIR.'/views/_header.php'); ?>

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/file-upload/css/jquery.fileupload.css" type="text/css" />
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Mediathek <a href="/admin/media/" class="btn btn-success pull-right">Übersicht</a></h1>  
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Datei hochladen</h3>
            </div>

              <div class="box-body">
                
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    
                    
        <div style="border:2px dashed #ccc; text-align: center; padding: 30px;">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
                    
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Datei auswählen...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" multiple>
    </span></div>
    
    
    <br>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>         
         
         
        </div>
      </div>
      </div></section>

      
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.fileupload.js"></script>


<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
   // 'use strict';
    // Change this to the location of your server-side upload handler:
    var url = "<?= BASE_URL; ?>static/assets/file-upload/";
    
    
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
	            
	            
	            var filename = file.name;
	            var filepath = file.url;
	            var filesize = file.size;
	            var filetype = file.type; 
	            $.post( "/admin/media/insertmedia", { mediaFile: filename, mediaPath: filepath, mediaType: filetype, mediaVersion: file })
				  .done(function(data) {
				    console.log( "Data Loaded: " + data);
				});
					            
                $('<p/>').text(filename + " - " + filesize + " - " + filetype).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
      


<?php include(APP_DIR.'/views/_footer.php'); ?>