<?php include(APP_DIR.'/views/_header.php'); ?>
	
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Mediathek <a href="/admin/media/upload" class="btn btn-success pull-right">Hochladen</a></h1>  
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Übersicht</h3>
            </div>
<form method="post" id="mediaEditor">
	<div class="box-body padding">
		<div class="row">
			<div class="col-sm-4">
					<img src="<?= $media["mediaVersion"]["thumbnailUrl"] ?>" class="img-responsive" />
			</div>
			<div class="col-sm-8">
				<div class="form-group">
	                  <div class="checkbox">
	                    <label>
	                      <input name="send" type="checkbox" <?= ($media["send"] == 1)?"checked":""?>>
	                      Medium kann als Anhang versendet werden
	                    </label>
	                  </div>
				</div>
				<p>
					<h3>URLs:</h3>
					<?= ($media["mediaVersion"]["url"]) ?><br />
					<?= ($media["mediaVersion"]["mediumUrl"]) ?><br />
					<?= ($media["mediaVersion"]["thumbnailUrl"]) ?><br />
					<?= ($media["mediaVersion"]["minithumbUrl"]) ?><br />
				</p>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<input type="hidden" name="id" value="<?= $media["mediaID"] ?>" />
		<input type="hidden" name="delete" value="" id="deleteField" />
		<input type="submit" class="btn btn-success pull-right" value="Speichern" />
		<?php if($role === "admin") {?><input type="button" class="btn btn-danger pull-right <?= ($used)?"disabled":"" ?>" id="delete" value="Löschen" /> <?php }else{ ?>
		<input type="button" class="btn btn-danger pull-right disabled" value="Löschen (ADMIN)" /> 
		<?php } ?>
	</div>
</form>
        </div>
      </div>
      
      </div>
      
      
          <div class="box box-primary <?= (!$used)?"hidden":"" ?>">
            <div class="box-header with-border">
              <h3 class="box-title">Verwendung</h3>
            </div>
<form method="post">
	<div class="box-body padding">
		<div class="row">
			<div class="col-sm-12">
				
				<table class="table table-striped">
				<?php 
					
					foreach($used["productBild"] as $productBild){
						?>
						<tr>
							<td><?= $productBild["talking_id"]?></td>
							<td><?= $productBild["name"]?></td>
							<td>Produktbild</td>
							<td><a href="/admin/products/product/<?= $productBild["id"]?>" target="_blank" class="btn btn-success pull-right btn-sm">Produkt ansehen</a></td>
						</tr>
						<?php
					}
					foreach($used["produktMedia"] as $produktMedia){
						?>
						<tr>
							<td><?= $produktMedia["talking_id"]?></td>
							<td><?= $produktMedia["name"]?></td>
							<td>Medienelement (Produkt)</td>
							<td><a href="/admin/products/product/<?= $produktMedia["id"]?>" target="_blank" class="btn btn-success pull-right btn-sm">Produkt ansehen</a></td>
						</tr>
						<?php
					}
					foreach($used["produktGallerie"] as $produktGallerie){
						?>
						<tr>
							<td><?= $produktGallerie["talking_id"]?></td>
							<td><?= $produktGallerie["name"]?></td>
							<td>Element in Produktgalerie</td>
							<td><a href="/admin/products/product/<?= $produktGallerie["id"]?>" target="_blank" class="btn btn-success pull-right btn-sm">Produkt ansehen</a></td>
						</tr>
						<?php
					}					
					foreach($used["cmsMedia"] as $cmsMedia){
						?>
						<tr>
							<td><?= $cmsMedia["id"]?></td>
							<td><?= $cmsMedia["title"]?></td>
							<td>Beitragsbild</td>
							<td><a href="/admin/cms/post/<?= $cmsMedia["id"]?>" target="_blank" class="btn btn-success pull-right btn-sm">CMS Beitrag ansehen</a></td>
						</tr>
						<?php
					}						
				?>
				</table>
			</div>
		</div>
	</div>
</form>
        </div>

      
      </section>
<script>
	
	$(document).on("click","#delete",function(){
		$("#deleteField").val("delete");
		$("#mediaEditor").submit();
	});
	
</script>


<?php include(APP_DIR.'/views/_footer.php'); ?>