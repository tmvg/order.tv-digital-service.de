<?php 
	if(array_key_exists("crm_files", $data)) $files = $data["crm_files"];
	if(!is_array($files)) $files = array();

	$GLOBALS["mediaSelectorCount"] ++;
	$identifyer = "media_".rand()."_"; 
	
	$limit = 1;
	
	if(array_key_exists("limit", $data)) $limit = $data["limit"];
	
	$isImage = true;
	$gallery = "";
	
	
	if($limit != 1){
		
		$imgIds = array();
		if(is_array($data["image"])){
			foreach($data["image"] as $img){
				$imgIds[] = $img["mediaID"];
				$gallery .= "<div class='galleryItem' style='background-image:url(".$GLOBALS["config"]["image_url"] . $img["mediaVersion"]["thumbnailUrl"] .")' data-id='".$img["mediaID"]."'></div>";
			}
		}
		
		$imageIdField = json_encode($imgIds);
	}else{
		if(is_array($data["image"])) $imageIdField = $data["image"]["mediaID"];
		else $imageIdField = "";
	}
	
	
	if(! is_array($data["image"])) $isImage = false;
	if(is_array($data["image"])){
		if(! array_key_exists("mediaVersion", $data["image"])){
			$isImage = false;
				if(is_array($data["image"]["mediaVersion"])){
				if(! array_key_exists("thumbnailUrl", $data["image"]["mediaVersion"])) $isImage = false;
			}
		}
	} 
	
?>
<style>
	.galleryItem{
		height: 100px;
		width: 100px;
		float: left;
		margin: 5px;
		border: 1px solid lightgray;
		padding: 2px;
		background-position: center center;
		box-shadow: 0px 0px 1px 1px black;
	}

	
	.galleryItem:after{

		content:"\f00d";
		font-family: "FontAwesome";
		color:red;
		line-height: 100px;
		text-align: center;
		width: 100px;
		display: block;
		font-size: 4em;
	  visibility:hidden;
	  opacity:0;
	  transition:opacity 0.3s linear;	}
	
	.galleryItem:hover::after{
		visibility:visible;
  opacity:1;
	}

	.viewImgButton{
		position: absolute;
		right:10px;
		margin: 10px;
	}
</style>

<div class="box box-primary">
	 <div class="box-header">
      <h3 class="box-title"><?= $data["title"]?>
        <small></small>
      </h3>
	 </div>
	<div class="box-body box-profile imageUploadBox" id="<?= $identifyer ?>image" data-limit="<?= $limit ?>">
		
		<div style="border:2px dashed #ccc; text-align: center; padding: 30px; margin-bottom: 10px;" class="directUploadArea <?= ($data["image"])?"hidden":"" ?>">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
			<span class="btn btn-success fileinput-button">
				<i class="glyphicon glyphicon-plus"></i>
				<span>Datei auf Computer auswählen...</span>
				<input class="fileupload" type="file" name="files[]" multiple />
			</span>
		</div>
		<div id="<?= $identifyer ?>files">
			
			<?php 
			
				foreach($files as $file){
					echo "<p>".$file["mediaFile"]. "<input type='hidden' value='".$file["mediaID"]."' name='media[]'> <a class='btn btn-xs btn-danger pull-right deleteMedia '><i class='fa fa-trash' aria-hidden='true'></i></a> </p>";	
				}
			
			?>
		</div>
		<div style="clear: both;"></div>
		
		<i class="fa fa-spinner fa-spin spinner hidden" style="font-size:24px"></i>

		

		<input type="hidden" class="form-control imageIdField"  readonly="" name="<?= $data["name"]?>" value='<?= $imageIdField ?>' />
		<a href="#" class="btn btn-primary btn-block imageInput <?= ($data["image"] && ($limit == 1))?"hidden":"" ?>"><b>Datei aus Mediathek wählen</b></a>

	</div>
</div>

<?php
	
	if($GLOBALS["mediaSelectorCount"] == 1){ ?>

	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/file-upload/css/jquery.fileupload.css" type="text/css" />
	<link rel="stylesheet" href="/admin/static/assets/plugins/datatables/dataTables.bootstrap.css">
	
	<script src="/admin/static/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/admin/static/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/vendor/jquery.ui.widget.js"></script>
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.iframe-transport.js"></script>
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.fileupload.js"></script>
	<script>
		$.fn.dataTable.ext.errMode = 'none';
	</script>
	
	
	
	<div class="modal fade modal-wide" id="mediaModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Bild auswählen</h4>
	      </div>
	      <div class="modal-body">
		  	<div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	              <li class="active"><a href="#mediathek" data-toggle="tab">Mediathek</a></li>
	              <li><a href="#upload" data-toggle="tab">Upload</a></li>
	            </ul>
	            <div class="tab-content">
	              <div class="tab-pane active" id="mediathek">
		              <table id="mediaLibrary" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Bild</th>
								<th>Titel</th>
								<th>Datum</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Beschreibung</th>
								<th>vorrätig</th>
							</tr>
						</tfoot>
					</table>
					              
	              </div>
	              <!-- /.tab-pane -->
	              <div class="tab-pane" id="upload">
	                    
	        <div style="border:2px dashed #ccc; text-align: center; padding: 30px;">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
	                    
	    <span class="btn btn-success fileinput-button">
	        <i class="glyphicon glyphicon-plus"></i>
	        <span>Datei auswählen...</span>
	        <!-- The file input field used as target for the file upload widget -->
	        <input class="fileupload" type="file" name="files[]" multiple />
	    </span></div>
	    
	    
	    <br>
	    <br>
	    <!-- The global progress bar -->
	    <div id="progress" class="progress">
	        <div class="progress-bar progress-bar-success"></div>
	    </div>
	    <!-- The container for the uploaded files -->
	    <div id="files" class="files"></div>      
	              </div>
	              <!-- /.tab-pane -->
	            </div>
	            <!-- /.tab-content -->
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	
	
	
	<script>
		var image = parseInt("<?= $data["image"]?>");
		var <?= $identifyer ?><?= $identifyer ?>sender = null;
		
		$('body').on('click', '.imageInput', function() {
			
			$("#mediaModal").modal();
			<?= $identifyer ?>sender = $(this).parent();
			return false;
		});
	
		$('body').on('click', '.deleteMedia', function() {
			$(this).closest('p').remove();
			return false;
		});
		
		
	$(function () {
		
		 window.mediaTable =  $('#mediaLibrary').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
	        "order": [[ 0, "desc" ]],

			"ajax": "/admin/media/ajax_MediaTable/call",
			"columnDefs": [{
				"targets": -1,
				"data": null,
				"defaultContent": "<span class='select btn btn-primary'>Auswählen</span>"
				}],
			"language": {
				"url": "/admin/static/assets/plugins/datatables/language/german.json"
			}
		});
	 
	
		window.mediaTable.on( 'click', '.select', function () {
			var data = mediaTable.row( $(this).parents('tr') ).data();
			

			$('<p/>').html(data[2] +"<input type='hidden' value='"+data[0]+"' name='media[]'> <a class='btn btn-xs btn-danger pull-right deleteMedia '><i class='fa fa-trash' aria-hidden='true'></i></a> ").appendTo("#<?= $identifyer ?>files");

			$("#mediaModal").modal("hide");
	    });
	
	
	
	  });
	  
	</script>
	
	
	
	
	<script>
	/*jslint unparam: true */
	/*global window, $ */
	$(function () {
	   // 'use strict';
	    // Change this to the location of your server-side upload handler:
	    var url = "<?= BASE_URL; ?>static/assets/file-upload/";
	    
	    $('.fileupload').each(function() {
		    $(this).fileupload({
		        url: url,
		        dataType: 'json',
				dropZone: $(this).closest(".directUploadArea"),
		        formData: {userID: '<?= $_SESSION["userID"] ?>'},
 	            maxChunkSize: 10000000,
		        done: function (e, data) {

	   				var imageBlockID = $($(this).closest(".imageUploadBox"));
			        
		            $.each(data.result.files, function (index, file) {
			            
			            var filename = file.name;
			            var filepath = file.url;
			            var filesize = file.size;
			            var filetype = file.type; 
			            
			            
						$('<p/>').html(filename +"<input type='hidden' value='"+file.id+"' name='media[]'> <a class='btn btn-xs btn-danger pull-right deleteMedia '><i class='fa fa-trash' aria-hidden='true'></i></a> ").appendTo("#<?= $identifyer ?>files");

						
						$(".spinner").addClass('hidden');

						
							            
		            });
		            
		          window.mediaTable.ajax.reload();
		          $(".tab-pane").removeClass("active");
		          $("#mediathek").addClass("active");
		          

		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
					
					var  id = $(this).closest(".imageUploadBox").attr("id");
					$("#"+id).children(".spinner").removeClass('hidden');
					
					$("#"+id).children(".progress-bar").css('width', progress + '%');

		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	});
	</script>

<?php } ?>
     