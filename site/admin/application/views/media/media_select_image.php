<?php
	$GLOBALS["mediaSelectorCount"] ++;
	$identifyer = "media_".rand()."_"; 
	
	$limit = 1;
	
	if(array_key_exists("limit", $data)) $limit = $data["limit"];
	
	$isImage = true;
	$gallery = "";
	
	
	if($limit != 1){
		
		$imgIds = array();
		if(is_array($data["image"])){
			foreach($data["image"] as $img){
				$imgIds[] = $img["mediaID"];
				$gallery .= "<div class='galleryItem' style='background-image:url(".$GLOBALS["config"]["image_url"] . $img["mediaVersion"]["thumbnailUrl"] .")' data-id='".$img["mediaID"]."'></div>";
			}
		}
		
		$imageIdField = json_encode($imgIds);
	}else{
		if(is_array($data["image"])) $imageIdField = $data["image"]["mediaID"];
		else $imageIdField = "";
	}
	
	
	if(! is_array($data["image"])) $isImage = false;
	if(is_array($data["image"])){
		if(! array_key_exists("mediaVersion", $data["image"])){
			$isImage = false;
				if(is_array($data["image"]["mediaVersion"])){
				if(! array_key_exists("thumbnailUrl", $data["image"]["mediaVersion"])) $isImage = false;
			}
		}
	} 
	
?>
<style>
	.galleryItem{
		height: 100px;
		width: 100px;
		float: left;
		margin: 5px;
		border: 1px solid lightgray;
		padding: 2px;
		background-position: center center;
		box-shadow: 0px 0px 1px 1px black;
	}

	
	.galleryItem:after{

		content:"\f00d";
		font-family: "FontAwesome";
		color:red;
		line-height: 100px;
		text-align: center;
		width: 100px;
		display: block;
		font-size: 4em;
	  visibility:hidden;
	  opacity:0;
	  transition:opacity 0.3s linear;	}
	
	.galleryItem:hover::after{
		visibility:visible;
  opacity:1;
	}

	.viewImgButton{
		position: absolute;
		right:10px;
		margin: 10px;
	}
</style>

<div class="box box-primary">
	 <div class="box-header">
      <h3 class="box-title"><?= $data["title"]?>
        <small></small>
      </h3>
	 </div>
	<div class="box-body box-profile imageUploadBox" id="<?= $identifyer ?>image" data-limit="<?= $limit ?>">
		
		<?php if($limit == 1){ ?>
			<div style="border:2px dashed #ccc; text-align: center; padding: 30px; margin-bottom: 10px;" class="directUploadArea <?= ($data["image"])?"hidden":"" ?>">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
				<span class="btn btn-success fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Datei auf Computer auswählen...</span>
					<input class="fileupload" type="file" name="files[]" multiple />
				</span>
			</div>
		<?php } ?>
		<?= $gallery ?>
		<div style="position: relative;">
			
			<?php if($isImage ) { ?> 
				<a href="#" onClick='window.open("<?= $GLOBALS["config"]["image_url"] .$data["image"]["mediaVersion"]["url"] ?>", "myWin_<?= rand() ?>", "scrollbars=yes,width=400,height=650"); return false;' class="btn btn-primary viewImgButton" ><i class="fa fa-eye" aria-hidden="true"></i></a>
			<?php } ?>
			
			<img class="img-responsive <?= ($isImage)?"":"hidden" ?>" src="<?= $GLOBALS["config"]["image_url"] ?><?= $data["image"]["mediaVersion"]["mediumUrl"] ?>" alt="Bild" />
			<a class="downloadButton <?= ($isImage)?"hidden":"" ?>" href="<?= $GLOBALS["config"]["image_url"] ?><?= (is_array($data["image"]))?$data["image"]["mediaVersion"]["url"]:"" ?>" target="_blank" ><?= (is_array($data["image"]))?$data["image"]["mediaFile"]:"" ?></a>
		</div>
		<div style="clear: both;"></div>
		
		<i class="fa fa-spinner fa-spin spinner hidden" style="font-size:24px"></i>

		

		<input type="hidden" class="form-control imageIdField"  readonly="" name="<?= $data["name"]?>" value='<?= $imageIdField ?>' />
		<a href="#" class="btn btn-primary btn-block imageInput <?= ($data["image"] && ($limit == 1))?"hidden":"" ?>"><b>Bild aus Mediathek wählen</b></a>
		<a href="#" class="btn btn-danger btn-block imageDelete <?= ($data["image"] && ($limit == 1))?"":"hidden" ?>"><b>Bild löschen</b></a>

	</div>
</div>

<?php
	
	if($GLOBALS["mediaSelectorCount"] == 1){ ?>

	<link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/file-upload/css/jquery.fileupload.css" type="text/css" />
	<link rel="stylesheet" href="/admin/static/assets/plugins/datatables/dataTables.bootstrap.css">
	
	<script src="/admin/static/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/admin/static/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/vendor/jquery.ui.widget.js"></script>
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.iframe-transport.js"></script>
	<script src="<?php echo BASE_URL; ?>static/assets/file-upload/js/jquery.fileupload.js"></script>
	<script>
		$.fn.dataTable.ext.errMode = 'none';
	</script>
	
	
	
	<div class="modal fade modal-wide" id="mediaModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Bild auswählen</h4>
	      </div>
	      <div class="modal-body">
		  	<div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	              <li class="active"><a href="#mediathek" data-toggle="tab">Mediathek</a></li>
	              <li><a href="#upload" data-toggle="tab">Upload</a></li>
	            </ul>
	            <div class="tab-content">
	              <div class="tab-pane active" id="mediathek">
		              <table id="mediaLibrary" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Bild</th>
								<th>Titel</th>
								<th>Datum</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Beschreibung</th>
								<th>vorrätig</th>
							</tr>
						</tfoot>
					</table>
					              
	              </div>
	              <!-- /.tab-pane -->
	              <div class="tab-pane" id="upload">
	                    
	        <div style="border:2px dashed #ccc; text-align: center; padding: 30px;">Datei zum Hochladen in dieses Feld ziehen oder <br><br>
	                    
	    <span class="btn btn-success fileinput-button">
	        <i class="glyphicon glyphicon-plus"></i>
	        <span>Datei auswählen...</span>
	        <!-- The file input field used as target for the file upload widget -->
	        <input class="fileupload" type="file" name="files[]" multiple />
	    </span></div>
	    
	    
	    <br>
	    <br>
	    <!-- The global progress bar -->
	    <div id="progress" class="progress">
	        <div class="progress-bar progress-bar-success"></div>
	    </div>
	    <!-- The container for the uploaded files -->
	    <div id="files" class="files"></div>      
	              </div>
	              <!-- /.tab-pane -->
	            </div>
	            <!-- /.tab-content -->
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	
	
	
	<script>
		var image = parseInt("<?= $data["image"]?>");
		var <?= $identifyer ?><?= $identifyer ?>sender = null;
		
		$('body').on('click', '.imageInput', function() {
			
			$("#mediaModal").modal();
			<?= $identifyer ?>sender = $(this).parent();
			return false;
		});
	
		$('body').on('click', '.imageDelete', function() {
			<?= $identifyer ?>sender = $(this).parent();
			
			<?= $identifyer ?>sender.find("input").val("");
			<?= $identifyer ?>sender.find("img").attr("src","#") ;
			<?= $identifyer ?>sender.find("img").addClass("hidden") ;
			
			<?= $identifyer ?>sender.find(".directUploadArea").removeClass("hidden") ;
			
			<?= $identifyer ?>sender.find(".imageDelete").addClass("hidden") ;
			<?= $identifyer ?>sender.find(".imageInput").removeClass("hidden") ;
			<?= $identifyer ?>sender.find("a.downloadButton").addClass("hidden") ;
			<?= $identifyer ?>sender.find(".viewImgButton").addClass("hidden") ;

					return false;
	
		});
		$('body').on('click', '.galleryItem', function() {
			
			var removeElement = $(this).data("id");	
		
			var mediaIds = [];
		
			var imageBlockID  = $($(this).closest(".imageUploadBox"));

			try {
				mediaIds = jQuery.parseJSON(imageBlockID.find(".imageIdField").val());
			} catch (e) {}	
			
			mediaIds = jQuery.grep(mediaIds, function(value) {
			 	return value != removeElement;
			});
			
			imageBlockID.find(".imageIdField").val( JSON.stringify(mediaIds) );
			
			$(this).remove();
				
		});

		
	$(function () {
		
		 window.mediaTable =  $('#mediaLibrary').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
	        "order": [[ 0, "desc" ]],

			"ajax": "/admin/media/ajax_MediaTable/call",
			"columnDefs": [{
				"targets": -1,
				"data": null,
				"defaultContent": "<span class='select btn btn-primary'>Auswählen</span>"
				}],
			"language": {
				"url": "/admin/static/assets/plugins/datatables/language/german.json"
			}
		});
	 
	
		window.mediaTable.on( 'click', '.select', function () {
			var data = mediaTable.row( $(this).parents('tr') ).data();
			
			
			if(<?= $identifyer ?>sender.data("limit") != 1){
				
				var mediaIds = [];
				
				try {
					var currentMediaData = jQuery.parseJSON(<?= $identifyer ?>sender.find(".imageIdField").val());
				} catch (e) {
					var currentMediaData = "";
				}
				
			
				if($.isArray(currentMediaData)){
					mediaIds = currentMediaData;
				}
				
				
				
				if( $.inArray(data[0], mediaIds) == -1){

					mediaIds.push(data[0]);
					$( "<div class='galleryItem' style='background-image:url("+data[4]+")' data-id='"+data[0]+"'></div>" ).insertBefore(<?= $identifyer ?>sender.find("img") );
					<?= $identifyer ?>sender.find(".imageIdField").val( JSON.stringify(mediaIds) );

				}

				
				
				
			

			
			
			}else{
				
				<?= $identifyer ?>sender.find(".imageIdField").val( data[0] );
				<?= $identifyer ?>sender.find(".imageDelete").removeClass("hidden") ;
				<?= $identifyer ?>sender.find(".imageInput").addClass("hidden") ;
				
				<?= $identifyer ?>sender.find("img").attr("src",data[4]) ;
				<?= $identifyer ?>sender.find("img").removeClass("hidden");

			}

			
			<?= $identifyer ?>sender.find(".directUploadArea").addClass("hidden") ;

			$("#mediaModal").modal("hide");
	    });
	
	
	
	  });
	  
	</script>
	
	
	
	
	<script>
	/*jslint unparam: true */
	/*global window, $ */
	$(function () {
	   // 'use strict';
	    // Change this to the location of your server-side upload handler:
	    var url = "<?= BASE_URL; ?>static/assets/file-upload/";
	    
	    $('.fileupload').each(function() {
		    $(this).fileupload({
		        url: url,
		        dataType: 'json',
				dropZone: $(this).closest(".directUploadArea"),
		        formData: {userID: '<?= $_SESSION["userID"] ?>'},
 	            maxChunkSize: 10000000,
		        done: function (e, data) {

	   				var imageBlockID = $($(this).closest(".imageUploadBox"));
			        
		            $.each(data.result.files, function (index, file) {
			            
			            var filename = file.name;
			            var filepath = file.url;
			            var filesize = file.size;
			            var filetype = file.type; 
						
						imageBlockID.find(".imageIdField").val(file.id);
					
						console.log(file.name);

							
						if (typeof file.mediumUrl !== "undefined") {
							imageBlockID.find("img").attr("src","<?= $GLOBALS["config"]["image_url"] ?>"+file.mediumUrl) ;
							imageBlockID.find("img").removeClass("hidden") ;
						}else{
							imageBlockID.find(".downloadButton").attr("href","<?= $GLOBALS["config"]["image_url"] ?>"+filepath) ;
							imageBlockID.find(".downloadButton").text(filename);
							imageBlockID.find(".downloadButton").removeClass("hidden") ;
							imageBlockID.find(".viewImgButton").removeClass("hidden") ;
							
							

						}

						imageBlockID.find(".imageDelete").removeClass("hidden") ;
						imageBlockID.find(".imageInput").addClass("hidden") ;
						imageBlockID.find(".directUploadArea").addClass("hidden") ;
						
						$(".spinner").addClass('hidden');

						
							            
		                $('<p/>').text(filename + " - " + filesize + " - " + filetype).appendTo('#files');
		            });
		            
		          window.mediaTable.ajax.reload();
		          $(".tab-pane").removeClass("active");
		          $("#mediathek").addClass("active");
		          

		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
					
					var  id = $(this).closest(".imageUploadBox").attr("id");
					$("#"+id).children(".spinner").removeClass('hidden');
					
					$("#"+id).children(".progress-bar").css('width', progress + '%');

		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	});
	</script>

<?php } ?>
     