<?php include(APP_DIR.'/views/_header.php'); ?>
	
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Mediathek <a href="/admin/media/upload" class="btn btn-success pull-right">Hochladen</a></h1>  
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Übersicht</h3>
            </div>

<div class="box-body padding">


         <?php
	         
	         foreach($medias as $m) {
		         $version = false;
		         $version = unserialize($m["mediaVersion"]);

		         echo '<div class="col-lg-3 col-xs-12 "><div class="box box-default">';
		         echo '<div class="box-header with-border"><h3 class="box-title"><a href="/admin/media/edit/?id='.$m['mediaID'].'" class="right-pull"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></h3></div>';
				 echo '<div class="attachment-block clearfix">';
				 if(array_key_exists("minithumbUrl", $version))     echo '<img src="'.DOMAIN.'/media/'.$version["minithumbUrl"].'" class="attachment-img" ><br>';

echo'
                <div class="attachment-pushed">

                  <div class="attachment-text">
                    '.$m['mediaFile'].'
                  </div>
                </div>
              </div>';
		        // echo '<div style="overflow:hidden; whitespace:no-wrap;"></div>';
		         echo '</div></div>';
		         
	         }
	         
	         ?>
</div>
        </div>
      </div>
      
      </div></section>

      
      


<?php include(APP_DIR.'/views/_footer.php'); ?>