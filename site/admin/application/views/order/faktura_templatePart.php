	
	<div class="col-md-8">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">Kundendaten</h3>
		    </div>
		    <div class="box-body">
			    <table class="table">
				    <tr><td><b>Kunde</b></td><td><?= $orderData[company] ?></td></tr>
				    <tr><td><b>GP Kunde</b></td><td><?= $orderData[gp] ?></td></tr>
				    <tr><td><b>Rechnungsdatum</b></td><td><?= date("d.m.Y", strtotime($orderData[invoice_date])) ?></td></tr>
				    <tr><td><b>Vermittler 1</b></td><td><?= $orderData[beraterID_1] ?></td></tr>
				    <?php if($orderData[beraterID_2]) { echo '<tr><td><b>Vermittler 2</b></td><td>'.$orderData[beraterID_2].'</td></tr>'; } ?>
				    <tr><td><b>Abrechnung</b></td><td><?= $orderData[abrechnungsart].", ".$orderData[abrechnungsintervall].", Wiedervorlage: ".$orderData[wiedervorlage] ?></td></tr>
			    </table>
			    
     	
	        </div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="box box-danger">
		    <div class="box-header with-border">
		      <h3 class="box-title">SAP Informationen</h3>
		    </div>
		    <div class="box-body">
			    
				    <form>
					    <table class="table">
				    <tr><td><b>Jobnummer</b></td><td><input type="text" value="<?= $orderData[o.SAP_jobnr] ?>" class="form-control" /></td></tr>
				    <tr><td><b>Notiz</b></td><td><textarea class="form-control"></textarea></td></tr>
				    </table>
				    <span class="btn btn-danger pull-right" id="save">Speichern</span>
				    
				    </form>
			    
			    
     	
	        </div>
		</div>
	</div>	
	
	<div class="col-md-12">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">Positionen</h3>
		    </div>
		    <div class="box-body">
			<table class="table">    
			<?php
				$p = 1;
				foreach($positionData as $position) {	
					
					echo "<tr><td colspan='2'><i>Pos. $p</i></td></tr>";
					    
			    
			    
				    echo '<tr><td><b>Buchungszeitraum</b></td><td>'. date("d.m.Y", strtotime($position[von]))."  - ".date("d.m.Y", strtotime($position[bis])) .'</td></tr>
				    <tr><td><b>Buchungsinfo</b></td><td>'.$position[portal] .' | '.$position[auftragsart] .'</td></tr>';
				    if($position[gesamtpreis]) echo '<tr><td><b>Preis</b></td><td>'. $position[gesamtpreis] .'</td></tr>';
				    if($position[rabatt]>0) echo '<tr><td><b>Rabatt</b></td><td>'. $position[rabatt] .'</td></tr>';
				    if($position[SAP_STICHWORT]) echo '<tr><td><b>Stichwort</b></td><td>'. $position[SAP_STICHWORT] .'</td></tr>';
				    if($position[bemerkung]) echo '<tr><td><b>Notiz</b></td><td>'. $$position[bemerkung] .'</td></tr>';
			    
					$p++; 
				} 
			
			?>	    
			</table>
	        </div>
		</div>
	</div>	
	

	
	


