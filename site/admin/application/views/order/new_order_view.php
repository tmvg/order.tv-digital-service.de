<?php include(APP_DIR.'/views/_header.php'); ?>


<form method="post" action="/admin/order/save/">

<div class="col-md-12" style="margin-top: 10px">
<h2>Auftragserfassung</h2>

<div class="row">
	<div class="col-md-9">
		<div class="box box-danger">
		    <div class="box-header with-border">
		      <h3 class="box-title">Kundendaten</h3>
		    </div>
		    <div class="box-body">
			    <div class="row">
		            <div class="form-group col-md-12">
		                <label>Kunde</label>
		                <div class="input-group">
			                <select class="form-control select2 select2-hidden-accessible selectClient" style="width: 100%;" tabindex="-1"  name="o_kunde" placeholder="Kunde" name="client" >
							<?php
							
								if($data["client"]){
									?>
									<option value="<?= $data["client"]["id"] ?>" selected=""><?= $data["client"]["company"] ?> (<?= $data["client"]["gp"] ?>)</option>
									<?
								}	
							?>
			                </select>
		                    <span class="input-group-btn">
		                      <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#newClient">Neukunde</button>
		                    </span>
						</div>
		        	</div>
			    </div>
				<div class="row">
		        	<div class="form-group col-md-6">
		                <label>Vermittler 1</label>
		                <select class="form-control select2 select2-hidden-accessible selectVermittler" style="width: 100%;" tabindex="-1" placeholder="Vermittler 1" data-id="<?= $data["beraterID_1"] ?>" name="beraterID_1">
							<?php
							
								if($data["berater_1"]){
									?>
									<option value="<?= $data["berater_1"]["beraterID"] ?>" selected=""><?= $data["berater_1"]["vorname"] ?> <?= $data["berater_1"]["nachname"] ?> (<?= $data["berater_1"]["gp"] ?>) </option>
									<?
								}	
							?>			                
		                </select>
		        	</div>
		        	<div class="form-group col-md-6">
		                <label>Vermittler 2</label>
						<select class="form-control select2 select2-hidden-accessible selectVermittler" style="width: 100%;" tabindex="-1" placeholder="Vermittler 2"  data-id="<?= $data["beraterID_2"] ?>" name="beraterID_2">
							<?php
							
								if($data["berater_2"]){
									?>
									<option value="<?= $data["berater_2"]["beraterID"] ?>" selected=""><?= $data["berater_2"]["vorname"] ?> <?= $data["berater_2"]["nachname"] ?> (<?= $data["berater_2"]["gp"] ?>) </option>
									<?
								}	
							?>		
						</select>
		        	</div>    
				</div>  	  
				<div class="row">
		        	<div class="form-group col-md-4">
		                <label>SAP Jobnummer</label>
		                <input type="text" class="form-control" value="<?= $data["SAP_jobnr"] ?>"  name="SAP_jobnr" />
		        	</div>
		        	<div class="form-group col-md-4">
		                <label>Rechnungsdatum</label>
		                <input type="text" class="form-control datepicker" value="<?= ($data)?date("d.m.Y",strtotime($data["invoice_date"])):"" ?>" name="invoice_date"  />
		        	</div>    
		        	<div class="form-group col-md-4">
		                <label>Status</label>
		                <select class="form-control" name="status">
			                
			                <?php 
				                foreach($orderStatus as $orderStat){
					                ?>
	              	                    <option value="<?= $orderStat["status_value"] ?>" <?= ($data["status"] == $orderStat["status_value"])?"selected":"" ?>><?= $orderStat["status_name"] ?></option>
					                <?php
				                }
			                ?>
	
		                </select>
		        	</div>
				</div>
				
				
				<div class="row">
		        	<div class="form-group col-md-4">
		                <label>Abrechnungsart</label>
		                <select class="form-control" name="o_berechnung_art">
		                    <option value="Rechnung" <?= ($data["abrechnungsart"] == "Rechnung")?"selected":"" ?>>Rechnung</option>
		                    <option value="Lastschrift" <?= ($data["abrechnungsart"] == "Lastschrift")?"selected":"" ?>>Lastschrift</option>
		                    <option value="keine Berechnung" <?= ($data["abrechnungsart"] == "keine Berechnung")?"selected":"" ?>>keine Berechnung</option>
		                </select>
		        	</div>
		        	
		        	<div class="form-group col-md-4">
		                <label>Abrechnungsintervall</label>
		                <select class="form-control" name="o_berechnung_intervall">
		                    <option value="einmalig" <?= ($data["abrechnungsintervall"] == "einmalig")?"selected":"" ?>>einmalig</option>
		                    <option value="monatlich" <?= ($data["abrechnungsintervall"] == "monatlich")?"selected":"" ?>>monatlich</option>
		                    <option value="jährlich" <?= ($data["abrechnungsintervall"] == "jährlich")?"selected":"" ?>>jährlich</option>
		                </select>
		        	</div>        	
		        	
		        	<div class="form-group col-md-4">
		                <label>Wiedervorlage</label>
		                <select class="form-control" name="o_berechnung_wiedervorlage">
		                    <option value="0" <?= ($data["wiedervorlage"] == 0)?"selected":"" ?>>nein</option>
		                    <option value="1" <?= ($data["wiedervorlage"] == 1)?"selected":"" ?>>ja</option>
		                </select>
		        	</div>        	
	        	</div>     
	        	
				
				
	        </div>
		</div>
		<div id="positionen">
				<?php  
					if(is_array($data["positionen"])){
						foreach($data["positionen"] as $pos){
							$this->get_template_part("order/single_position_templatePart",array($pos,$orderTypes,$orderStatus,$orderPortal));
						}
					}
				?>
			
		</div>
		<button type="button" id="add"> + Position</button>
	
		
		</div>
	
		<div class="col-md-3">
	
			<div class="box box-primary">
		        <div class="box-header">
		          <h3 class="box-title">Aktionen
		            <small></small>
		          </h3>
		          <!-- tools box -->
		          <div class="pull-right box-tools">
		            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
		              <i class="fa fa-minus"></i></button>
		          </div>
		          <!-- /. tools -->
		        </div>
				<div class="box-body padding ">
					<div class="row text-center">
						<div class="col-xs-12">
							<p>	
								<?php if($data){ ?> 
									<input type="hidden" value="<?= $data["orderID"] ?>" name="orderID" />
								<?php } ?>
	
								<input type="submit" class="btn btn-success" value="Speichern" style="width: 100%"  />
							</p>
						</div>				
					</div>
				</div>
				<!-- /.box-body -->
			</div>		
			
			<?php  $this->get_template_part("media/media_select_data",array('title' => "Medien","crm_files" => $data["media"]))  ?>

		</div>
	</div>
	
</div>



</form>       


<!-- MODALS START -->
	<div class="modal fade " id="newClient">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="newCustomerForm" class="ajaxForm" action="<?= $config["base_url"] ?>order/ajax_add_client/query/" data-callback="updateCustomerForm" data-id="newClient">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">neuer Kunde</h4>
				</div>
				<div class="modal-body">

				<?php  $this->get_template_part("clients/client_form",array()) ?>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">schlie&szligen</button>
					<button type="submit" class="btn  pull-right btn-success saveContactData" >Speichern</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
		<script>
			
			$(".ajaxForm").submit(function(e) {
				e.preventDefault();
				var formData =  $(this).serializeArray().reduce((a, x) => ({ ...a, [x.name]: x.value }), {});
				var action = $(this).attr("action");
				var senderModal = $(this).data("id");
				
				
				$.post(action, {
				    dataType: "json",
				    data:formData,
				}).done(function(data) { 
					
					if(data.status){
						
						$(".selectClient").html("<option value='"+data.data.id+"'>"+data.data.company+" ("+data.data.gp+")</option>");
						
					    $('#'+senderModal).modal('toggle');
						
					}else{

						alert(data.error);
					}
					
				});

				
			});
			
		</script>
		
	</div>


<!-- MODALS END -->





<script>
	

	
	$(function(){
    	$(".selectVermittler").select2({

			ajax: {
				url: '<?= $config["base_url"] ?>order/ajax_find_vermittler/query',
			    dataType: 'json',

				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
			      // Tranforms the top-level key of the response object from 'items' to 'results'
				  return {
			        results: data.result
			      };
			    }
			}
		});
	});
	
	
	$(function(){
    	$(".selectClient").select2({
		
			ajax: {
				url: '<?= $config["base_url"] ?>order/ajax_find_client/query',
			    dataType: 'json',

				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
			      // Tranforms the top-level key of the response object from 'items' to 'results'
				  return {
			        results: data.result
			      };
			    }
			}
		});
	});

	
	
	
	
	
	
	
	$(document).on('click', '#add', function(){

		

		var jqxhr = $.getJSON( "<?= $config["base_url"] ?>order/ajax_get_new_position/getData", function() {
			  console.log( "success" );
		})
			  .done(function(data) {
				  
				  if(data.status){
				  		$("#positionen").append(data.content);
					}
			  })
			  .fail(function() {
// 			    console.log( "error" );
			  })
			  .always(function() {
// 			    console.log( "complete" );
			  });
			 
			 
				
	
	});

	$(document).ready(function(){
		$('.datepicker').datepicker({
			autoclose: true,
			format: 'dd.mm.yyyy',
			  language: 'de'
		});
	});
				
	

</script>






<?php include(APP_DIR.'/views/_footer.php'); ?>