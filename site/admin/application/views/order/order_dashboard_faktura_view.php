<?php include(APP_DIR.'/views/_header.php'); ?>
<div class="col-md-12" style="margin-top: 10px">
	
	<div class="col-md-8">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">nicht fakturierte Aufträge</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders_faktura" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Rechnungsdatum</th>
							<th>Kunde</th>
							<th>GP</th>
							<th>Preis</th>
							<th>Vermittler</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>	
	
	<div class="col-md-4">
		<div class="box box-danger">
			<div class="box-header">
				<h3 class="box-title">Wiedervorlage</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders_vorlage" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Rechnungsdatum</th>
							<th>Kunde</th>
							<th>GP</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>	
	
</div>        

<!-- MODALS START -->
	<div class="modal fade" id="showOrder">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<form id="newCustomerForm" class="ajaxForm" action="<?= $config["base_url"] ?>order/save_order/" data-callback="updateCustomerForm" data-id="newClient">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Auftragsdatenblatt</h4>
				</div>
				<div class="modal-body" id="oderDetail">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">schlie&szligen</button>
					<button type="submit" class="btn  pull-right btn-success saveContactData" >Speichern</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
		<script>
			
			$(".ajaxForm").submit(function(e) {
				e.preventDefault();
				var formData =  $(this).serializeArray().reduce((a, x) => ({ ...a, [x.name]: x.value }), {});
				var action = $(this).attr("action");
				var senderModal = $(this).data("id");
				
				
				$.post(action, {
				    dataType: "json",
				    data:formData,
				}).done(function(data) { 
					
					if(data.status){
						
						$(".selectClient").html("<option value='"+data.data.id+"'>"+data.data.company+" ("+data.data.gp+")</option>");
						
					    $('#'+senderModal).modal('toggle');
						
					}else{

						alert(data.error);
					}
					
				});

				
			});
			
		</script>
		
	</div>
<!-- MODALS END -->


<script>
$(function () {
	
 
  
    
   var tableOrder = $('#orders_faktura').DataTable({
		"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        "buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_fakturaTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'}, // preis
			{'data':'4'}, // vermittler
			{'data':'5'}, // status

		]
    });
    
    $('#orders_faktura').on( 'click', 'tr', function () {

		var id = tableOrder.row( this ).data()[0];
		var jqxhr = $.getJSON( "<?= $config["base_url"] ?>order/ajax_get_order/"+id, function() {
			console.log( "success" );
		})
		.done(function(data) {
		
			if(data.status){
				$("#oderDetail").html(data.content);
			}
		})
		.fail(function() {
		// 			    console.log( "error" );
		})
		.always(function() {
		// 			    console.log( "complete" );
		});
			 

		$('#showOrder').modal('show');
		
		


		//location.href='/admin/order/faktura/'+ id;

	});	    
	

   var table = $('#orders_vorlage').DataTable({
		"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        "buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_vorlageTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'}, 
		//	{'data':'4'}, 

		]
    });
    
        $('#orders_vorlage').on( 'click', 'tr', function () {
		var id = table.row( this ).data()[0];
		location.href='/admin/order/faktura/'+ id;

	});	 	
    
    
  });
  

</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>

