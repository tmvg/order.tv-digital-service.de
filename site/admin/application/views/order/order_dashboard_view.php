<?php include(APP_DIR.'/views/_header.php'); ?>
<div class="col-md-12" style="margin-top: 10px">

	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">Aufträge Wiedervorlage</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders_2018" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Startdatum</th>
							<th>Kunde</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>	

	
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header">
				<h3 class="box-title">kommende Aufträge</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders_todo" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Startdatum</th>
							<th>Kunde</th>
							<th>GP</th>
							<th>Preis</th>
							<th>Vermittler</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">neue Aufträge</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders_new" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Startdatum</th>
							<th>Kunde</th>
							<th>GP</th>
							<th>Preis</th>
							<th>Vermittler</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	
	

	
</div>        
<script>
$(function () {
	
 
	  
   var table_todo = $('#orders_todo').DataTable({
		//"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        //"buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_todoTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'}, 
			{'data':'4'}, 
			{'data':'5'}, 

		]
    });
    
        $('#orders_todo').on( 'click', 'tr', function () {
		var id = table_todo.row( this ).data()[0];
		location.href='/admin/order/edit/'+ id;

	});
	
	
   var table_new = $('#orders_new').DataTable({
		//"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        //"buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_newTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'}, 
			{'data':'4'}, 
			{'data':'5'}, 

		]
    });
    
        $('#orders_new').on( 'click', 'tr', function () {
		var id = table_new.row( this ).data()[0];
		location.href='/admin/order/edit/'+ id;

	});	
    
    
   var table = $('#orders_2018').DataTable({
		//"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        //"buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_importTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},


		]
    });
    
        $('#orders_2018').on( 'click', 'tr', function () {
		var id = table.row( this ).data()[0];
		location.href='/admin/order/edit/'+ id;

	});	    
    
    
  });
  

</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>

