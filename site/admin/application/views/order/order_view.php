<?php include(APP_DIR.'/views/_header.php'); ?>
<div class="col-md-12" style="margin-top: 10px">
	
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Aufträge letzte 365 Tage</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  			<table id="orders" class="table table-bordered table-hover responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Kunde</th>
							<th>GP</th>
							<th>Preis</th>
							<th>Vermittler</th>
							<th>Rechnungsdatum</th>
							<th>Auftragszeitraum</th>
							<th>SAP Job Nr.</th>
							<th>Status</th>
						</tr>
					</thead>

				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		</div>

</div>        
<script>
$(function () {
	
 
	  
   var table = $('#orders').DataTable({
		"dom": 'Blfrtip',
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"order": [[ 0, 'asc' ]],
		"autoWidth": true,
        "stateSave": true,
        
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "alle"]],
        "buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],        
        
		"ajax": "/admin/order/ajax_orderTable/call",
		"language": {
			"url": "/admin/static/assets/plugins/datatables/language/german.json"
		},
		"columns" : [
			{'data':'0'},
			{'data':'1'},
			{'data':'2'},
			{'data':'3'}, // preis
			{'data':'4'}, // vermittler
			{'data':'5'},           
            {'data':'6'},
    		{'data':'7'}, 
			{'data':'8',
                "render": function (data, type, row  ){
				if(data == 0) return "neu";
				if(data == 1) return "offen";
				if(data == 2) return "erledigt";
				if(data == -1) return "gelöscht";
				if(data == -2) return "storniert";

            }}, 

		]
    });
    
        $('#orders').on( 'click', 'tr', function () {
		var id = table.row( this ).data()[0];
		location.href='/admin/order/edit/'+ id;

	});
    
    
    
  });
  

</script>
<?php include(APP_DIR.'/views/_footer.php'); ?>

