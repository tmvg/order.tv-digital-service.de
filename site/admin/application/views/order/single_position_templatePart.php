<div class="box box-warning" style="padding-top: 15px; background-color: #fbfbfb">
	<?php $rand = rand() ?>
	<?php
		
		if(is_array($data)){
			$orderTypes = $data[1];
			$orderStatus = $data[2];
			$orderPortal = $data[3];
			if(array_key_exists(0, $data)){
				if(array_key_exists("positionID", $data[0])){
					?>
						<input type="hidden" name="positon[<?= $rand ?>][id]" value="<?= $data[0]["positionID"]?>" />
					<?php				
				}
			}
		}
	?>
	
	<div class="col-md-6">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">Abrechnung</h3>
		    </div>
		    <div class="box-body">
			    
			<div class="row">

	        	
      	<div class="form-group col-md-6">
	                <label>Produkt (BE)</label>
					<select class="form-control" name="positon[<?= $rand ?>][o_portal]">
						<?php
							$opArray = array();
							
							foreach($orderPortal as $op){
								$opArray[] = $op["portal"];
							}
							if(!in_array($data[0]["portal"], $opArray)){
								?>
									<option value="<?= $data[0]["portal"] ?>" selected><?= $data[0]["portal"] ?></option>
								<?php	
							}
							foreach($opArray as $op){
							?>
								<option value="<?= $op ?>" <?= ($data[0]["portal"] == $op)?"selected":"" ?>><?= $op ?></option>
							<?php
							}
						?>
	                </select>
					<!--
					<div class="input-group">
						<span class="input-group-addon">anderes Portal</span>
						<input class="form-control" name="positon[<?= $rand ?>][o_portal_new]" placeholder="anderes Portal" type="text" value="">
					</div>
	                -->
	                
	        	</div>	        	
	        	<div class="form-group col-md-6">
	                <label>Auftragsart (IKO)</label>
	                <select class="form-control" name="positon[<?= $rand ?>][o_auftrag_art]">
		                <?php 
			                foreach($orderTypes as $orderType){
				                ?>
              	                    <option value="<?= $orderType["id"] ?>" <?= ($data[0]["auftragsart"] == $orderType["id"])?"selected":"" ?>><?= $orderType["type"] ?></option>
				                <?php
			                }
		                ?>
		            </select>
	        	</div>	        	
	        	
	        	<div class="form-group col-md-12">
	                <label>Status</label>
	                <select class="form-control" name="positon[<?= $rand ?>][o_status]">
		                
		                <?php 
			                foreach($orderStatus as $orderStat){
				                ?>
              	                    <option value="<?= $orderStat["status_value"] ?>" <?= ($data[0]["status"] == $orderStat["status_value"])?"selected":"" ?>><?= $orderStat["status_name"] ?></option>
				                <?php
			                }
		                ?>
		                
<!--
	                    <option value="offen" <?= ($data[0]["status"] == "offen")?"selected":"" ?>>offen</option>
						<option value="in Arbeit" <?= ($data[0]["status"] == "in Arbeit")?"selected":"" ?>>in Arbeit</option>                    
	                    <option value="fertig" <?= ($data[0]["status"] == "fertig")?"selected":"" ?>>fertig</option>
	                    <option value="storniert" <?= ($data[0]["status"] == "storniert")?"selected":"" ?>>storniert</option>
-->
	                </select>
	        	</div>        	
	        </div>
	            
	            <div class="row">
	            <div class="form-group col-md-6">
	                <label>Zeitraum von</label>
	                	<input class="form-control datepicker" name="positon[<?= $rand ?>][o_von]" placeholder="von DD.MM.YYYY" type="text" value="<?= ($data[0])?date("d.m.Y", strtotime($data[0]["von"])):date("d.m.Y", strtotime("now")) ?>">
	        	</div>
	            <div class="form-group col-md-6">
	                <label>bis</label>
	                	<input class="form-control datepicker" name="positon[<?= $rand ?>][o_bis]" placeholder="bis DD.MM.YYYY" type="text"  value="<?= ($data[0])?date("d.m.Y", strtotime($data[0]["bis"])):date("d.m.Y", strtotime("next week")) ?>">
	        	</div>        	
	            </div>		    
			    
	            <div class="row">
	            <div class="form-group col-md-6">
	                <label>Gesamtpreis</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_preis]" placeholder="Netto-Preis (Rechnungsbetrag)" type="text" value="<?= $data[0]["gesamtpreis"] ?>"> 
	        	</div>
	            
	            <div class="form-group col-md-6">
	                <label>Rabatt</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_rabatt]" placeholder="Netto-Rabatt" type="text" value="<?= $data[0]["rabatt"] ?>">
	        	</div>
	        	        	
	            </div>
	        	

	
<!--
	        	<div class="row">
		        	<div class="form-group col-md-4">
		                <label>SAP BE</label>
		                <select class="form-control" name="positon[<?= $rand ?>][o_sap_be]">
		                    <option value="Rechnung">Rechnung</option>
		                    <option value="Lastschrift">Lastschrift</option>
		                    <option value="keine Berechnung">keine Berechnung</option>
		                </select>
		        	</div>
		        	
		        	<div class="form-group col-md-4">
		                <label>SAP IKO</label>
		                <select class="form-control" name="positon[<?= $rand ?>][o_sap_iko]">
		                    <option value="einmalig">einmalig</option>
		                    <option value="monatlich">monatlich</option>
		                    <option value="jährlich">jährlich</option>
		                </select>
		        	</div>        	
		        	
		        	<div class="form-group col-md-4">
		                <label>SAP Format</label>
		                <select class="form-control" name="positon[<?= $rand ?>][o_sap_format]">
		                    <option value="nein">nein</option>
		                    <option value="ja">ja</option>
		                </select>
		        	</div>        	
	        	</div>  
-->  
	        	 	
	
	        	<div class="form-group">
	                <label>SAP Stichwort für Rechnung</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_sap_stichzeile]" placeholder="Zeile für SAP Rechnung" type="text" value="<?= $data[0]["SAP_STICHWORT"] ?>">
	        	</div>
	        	
	        	<div class="form-group">
	                <label>Bemerkung</label>
	                <textarea class="form-control" name="positon[<?= $rand ?>][o_bemerkung]" placeholder="Bemerkungen zur Abrechnung und Rabatt"><?= $data[0]["bemerkung"] ?></textarea>
	        	</div>
	        	
<!--
	        	<div class="form-group">
	                <label>SAP Jobnummer</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_sap_jobnummer]" placeholder="SAP Jobnummer" type="text">
	        	</div>        	
	        	
-->
	        	        	        	        	
	        </div>
		</div>
	
		
		</div>
		
		<div class="col-md-6">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">Werbedaten</h3>
		    </div>
		    <div class="box-body">
	            <div class="form-group">
	                <label>gebuchte Reichweite</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_tkp]" placeholder="gebuchte Reichweite" type="text" value="<?= $data[0]["reichweite"] ?>">
	        	</div>
	        	
	            <div class="form-group">
	                <label>Verlinkung</label>
	                <input class="form-control" name="positon[<?= $rand ?>][o_url]" placeholder="Verlinkung des Werbemittels" type="text" value="<?= $data[0]["clickUrl"] ?>">
	        	</div>        	
	        	
	        	
	  
	        	
	        	<?php 
		        	
		        	if($data[0]["import"] == 1){
			        	if(array_key_exists("import_kat_text", $data[0])){
				        	if(is_array($data[0]["import_kat_text"])){
					        	
					        	echo("<pre>");
					        	
					        	foreach($data[0]["import_kat_text"] as  $import_kat_text){
						        	$import_kat_text_array = array();
						        	foreach($import_kat_text as $import_kat_text_item){
							        	if(strlen(trim($import_kat_text_item)) > 0) $import_kat_text_array[] = $import_kat_text_item;
						        	}
						        	echo  (implode(" &gt; ", $import_kat_text_array))."\n";
					        	}
					        	
					        	echo("</pre>");
				        	}
			        	}
		        	}
	        	?>
	        	
	        	<div class="row">
	        	<div class="form-group col-md-6">
	                <label>Rubrik</label>
	                <?php $rubriken = array("Startseite","Region Haupt","Region Eifel","Region Mosel","Blaulicht","Nachrichten","Sport","Fotostrecken","Gesamtes Netzwerk");
		                $savedRubrik = array();
		                if(is_array($data[0])) $savedRubrik = unserialize($data[0]["rubrik"]);
		                if(!is_array($savedRubrik))$savedRubrik = array();
		                
		                foreach($rubriken as $r){
			                ?>
				                <div class="checkbox">
					                <label><input name="positon[<?= $rand ?>][o_rubrik][]" type="checkbox" value="<?= $r ?>" <?= ( in_array($r,$savedRubrik)?"checked":"" )?>><?= $r ?></label>
					            </div>
			                <?php
							if (($key = array_search($r, $savedRubrik)) !== false) {
								unset($savedRubrik[$key]);
							} 

		                }
	                ?>
	                <input class="form-control" name="positon[<?= $rand ?>][o_rubrik_sonstiges]" placeholder="Sonstiges" type="text" value="<?= implode(" ",$savedRubrik) ?>">
	        	</div>        	
	        	
	        	<div class="form-group col-md-6">
	                <label>Werbeformat</label>

	                <?php $formate = array("Wallpaper","Leaderboard","Skyscraper","Billboard","Premium Billboard","Rectangle","Premium Rectangle","HeaderAd","MaxiAd","FloorAd","NativeAd");
		               $savedFormate = array();
		               if(is_array($data[0])) $savedFormate = unserialize($data[0]["format"]);
		               if(!is_array($savedFormate))$savedFormate = array();
		                
		                foreach($formate as $f){
			                ?>
			                <div class="radio">
				                <label><input name="positon[<?= $rand ?>][o_format][]" type="radio" value="<?= $f ?>" <?= ( in_array($f,$savedFormate)?"checked":"" )?>><?= $f ?></label>
				            </div>

			                <?php
							if (($key = array_search($f, $savedFormate)) !== false) {
								unset($savedFormate[$key]);
							} 

		                }
	                ?>


		                            
	                <input class="form-control" name="positon[<?= $rand ?>][o_format_sonstiges]" placeholder="Sonstiges" type="text" value="<?= implode(" ",$savedFormate) ?>">

	        	</div>
	        	</div>
	
	        	<div class="form-group">
	                <label>Werbemittel</label>
	                <select class="form-control" name="positon[<?= $rand ?>][o_werbemittel_erstellung]">
	                    <option value="liefert Kunde" <?= ($data[0]["werbemittel_anlieferung"] == "liefert Kunde")?"selected":"" ?>>liefert Kunde</option>
						<option value="muss erstellt werden" <?= ($data[0]["werbemittel_anlieferung"] == "muss erstellt werden")?"selected":"" ?>>muss erstellt werden</option>                    
	                </select>
	        	</div>
	       	        	        	        	
	        </div>
		</div>		
		
		</div>	
		<br clear="all" />

</div>


