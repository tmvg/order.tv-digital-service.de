<?php include(APP_DIR.'/views/_header.php'); ?>
	
  
 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Setup<small></small></h1>  
    </section>

    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-xs-12 ">
         
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Einstellungen</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="/admin/setup" method="post" id="setup">
              <div class="box-body">
	              
	              
	              <?php
             
		              
		          foreach($setupData as $set) {
			          
			          echo '<label class="col-sm-3 control-label">'.$set['name'].'</label><div class="col-sm-3"><input type="text" class="form-control" name="'.$set['slug'].'"  value="'.$set['value'].'" required="required"></div>';
			          
		          }    
		              
		          ?>
	                
	              
                  
       
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="setSetup" value="1" class="btn btn-primary pull-right">Speichern</button>
              </div>
              <!-- /.box-footer -->
            </form>
         

        </div>
      </div>
      </div></section>

     
      


<?php include(APP_DIR.'/views/_footer.php'); ?>