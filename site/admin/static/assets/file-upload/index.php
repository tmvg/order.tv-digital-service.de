<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

include_once("../../../application/config/config.php");

include_once("../vendor/autoload.php");




error_reporting(0);
require('UploadHandler.php');

$dir = date("Y-m", time());
$upload_dir = $config['media_url']."/".$dir."/";
if(!is_dir($upload_dir)) mkdir($upload_dir, 0777, true);

$upload_url = "/".$dir."/";

$options =   array(
	
		'delete_type' => 'POST',
	    'db_host' => $config["db_host"],
	    'db_user' => $config["db_username"],
	    'db_pass' => $config["db_password"],
	    'db_name' => $config["db_name"],
	    'db_table' => 'media',
	
            'upload_dir' => $upload_dir,
            'upload_url' => $upload_url,
            'image_versions' => array(
                '' => array(
                    'auto_orient' => true
                ),
                'medium' => array(
                    'max_width' => 800,
                    'max_height' => 600
                ),
                'thumbnail' => array(
                    'max_width' => 300,
                    'max_height' => 300
                ),
                'minithumb' => array(
                    'crop' => true,
                    'max_width' => 100,
                    'max_height' => 100
                )

            ),
            'print_response' => true
        );





class CustomUploadHandler extends UploadHandler {

    protected function initialize() {
    	$this->db = new mysqli(
    		$this->options['db_host'],
    		$this->options['db_user'],
    		$this->options['db_pass'],
    		$this->options['db_name']
    	);
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
    	$file->userID = @$_REQUEST['userID'];
    }


	protected function is_last_chunk(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		    $is_chunked_upload = !empty($_SERVER['HTTP_CONTENT_RANGE']);
		    if ($is_chunked_upload) {
		        $is_last_chunk = false;
		
		        // [HTTP_CONTENT_RANGE] => bytes 10000000-17679248/17679249 - last chunk looks like this
		
		        if (preg_match('|(\d+)/(\d+)|', $_SERVER['HTTP_CONTENT_RANGE'], $range)) {
		
		            if ($range[1] == $range[2] - 1) {
		                $is_last_chunk = true;
		            }
		        }
		        return $is_last_chunk;
		    }else return true;
		}
	        
	}
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {
	    
        $file = parent::handle_file_upload(
        	$uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        
        
        if (empty($file->error)) {
	        

	        
			if($file->type === "application/pdf") $this->handle_data_file($this->get_upload_path($file->name), $file);

	        
	        if($this->is_last_chunk()){
				$sql = 'INSERT INTO `'.$this->options['db_table']
					.'` (`mediaFile`, `mediaPath`, `mediaDate`,`mediaVersion`,   `mediaType` )'
					.' VALUES (?, ?, ?, ?, ? )';
		        $query = $this->db->prepare($sql);
		        $query->bind_param(
		        	'ssiss',
		        	$file->name,
		        	date("Y-m", time()),
		        	time(),
		        	serialize((array)$file),
		        	$file->type
		        );
		        $query->execute();
		        $file->id = $this->db->insert_id;
	        }
        }
        
        
        
        return $file;
    }


	function handle_data_file($file_path, $file){
		
		if($file->type === "application/pdf"){
			
			$pdf = new Spatie\PdfToImage\Pdf($file_path);
			
			$tumb = $this->options["upload_dir"]."/".$file->name.".jpg";

			$pdf->setPage(1)->setOutputFormat('jpg')->saveImage($tumb);
		
		
	        foreach ($this->options['image_versions'] as $version => $options) {
	            if ($this->create_scaled_image($file->name.".jpg", $version, $options)) {
	                if (!empty($version)) {
	                    $file->{$version.'Url'} = $this->get_download_url(
	                        $file->name.".jpg",
	                        $version
	                    );
	                } else {
	                    $file->size = $this->get_file_size($file_path, true);
	                }
	            } else {
	                $failed_versions[] = $version ? $version : 'original';
	            }
	        }
	        
	        unlink($tumb);
	        
		}
	}



}











$upload_handler = new CustomUploadHandler($options);