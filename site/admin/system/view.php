<?php

class View {

	private $pageVars = array();
	private $template;
	

	public function __construct($template)
	{
		$this->template = APP_DIR .'views/'. $template .'.php';
	}

	public function set($var, $val)
	{
		$this->pageVars[$var] = $val;
	}


	public function info($val, $alert, $type="info", $msgArray = NULL, $tag = "span") {
		
		if($msgArray) {
			$info = '<section class="content" style="min-height:0;"><div class="callout callout-'.$type.'"><h4>'.$alert.'</h4>';
			$info .= $this->getMsg($msgArray, $tag);			
			$info .= '</div></section>';
			
		} else {
			$info = '<section class="content" style="min-height:0;"><div class="callout callout-'.$type.'"><h4>'.$alert.'</h4>'.$val.'</div></section>';
			

		}
		
		$this->pageVars[PAGEINFO] = $info;
				

	}


	public function getMsg($param, $tag = "span") {
		global $lang;
		
		if(is_array($param)) {
			foreach($param as $p) {
				
				$out .= '<'.$tag.'>'.$lang[$p].'</'.$tag.'>';
			}
			
			if($tag == "li") return "<ul>$out</ul>"; else return $out;
			
		} else {
			return '<'.$tag.'>'.$lang[$param].'</'.$tag.'>';
		}
	} 

	

	public function render()
	{
		global $config;
		global $config_menu;
		global $setup;
		$this->set("config", $config);
		$this->set("menu", $config_menu);
		$this->set("setup", $setup);
		extract($this->pageVars);

		ob_start();
		require($this->template);
		echo ob_get_clean();
	}
	

	public function getRendered()
	{
		global $config;
		global $config_menu;
		global $setup;
		$this->set("config", $config);
		$this->set("menu", $config_menu);
		$this->set("setup", $setup);
		
		extract($this->pageVars);
		ob_start();
		require($this->template);
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
		
	}	
	
	public function get_template_part($template,$data = NULL,$key = 0){

		ob_start();
		require(APP_DIR .'views/'. $template .'.php');
		$out = ob_get_contents();
		ob_end_clean();
		echo $out;
		
	}
	
	public function getTemplate(){
		return $this->template;
	}
    
}

?>