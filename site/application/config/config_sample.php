<?php 


// CORE
$config['title'] = "BURGENERATOR";
$config['language'] = "de";

if($_SERVER["HTTP_HOST"] == "voting.volksfreund.local") {  
	$config['domain'] = "http://voting.volksfreund.local"; 
	$config['base_url'] = 'http://voting.volksfreund.local/'; // Base URL including trailing slash (e.g. http://localhost/)
	// DATABASE
	$config['db_host'] = 'localhost'; // Database host (e.g. localhost)
	$config['db_name'] = 'voting.volksfreund.de'; // Database name
	$config['db_username'] = 'root'; // Database username
	$config['db_password'] = 'root'; // Database password

} else {
	$config['domain'] = "https://voting.volksfreund.de";
	$config['base_url'] = 'https://voting.volksfreund.de/'; // Base URL including trailing slash (e.g. http://localhost/)
	// DATABASE
	$config['db_host'] = ''; // Database host (e.g. localhost)
	$config['db_name'] = ''; // Database name
	$config['db_username'] = ''; // Database username
	$config['db_password'] = ''; // Database password

}


$config['verify_url'] = $config['domain'].'/user/verify/';
$config['newpass_url'] = $config['domain'].'/user/newpass/';
$config['media_url'] = $_SERVER['DOCUMENT_ROOT'].'/media/';
$config['image_url'] = $config['domain'].'/media/';
$config['default_controller'] = 'home'; // Default controller to load
$config['error_controller'] = 'error'; // Controller used for errors (e.g. 404, 500 etc)
$config['version'] = '1.0.1';

// PASSWORD SALT
$config['salt'] = 'zX{=_Ukb7La;GrU_Dzg_';
$config['token'] = 'K1fKTUsDelnjYXpCNnS8MoYPJxtoSY';

$config['mail_reply'] = "voting@tv-digital-service.de";
$config['mail_name'] = "votr";
$config['mail_use_smtp'] = true;
$config['mail_smtp_auth'] = true;
$config['mail_host'] = "mail.your-server.de";
$config['mail_smtp_username'] = "voting@tv-digital-service.de";
$config['mail_smtp_password'] = "FUbI93o2ztA4YJgb";
$config['mail_smtp_encryption'] = "ssl";
$config['mail_smtp_port'] = "465";

$config['mail_template_header'] =  'views/mail/header.php';
$config['mail_template_footer'] =  'views/mail/footer.php';

$config['mail_template_path'] =  'views/mail/';


$config['label']['key'] = "Bnm8xRWYFxqhLUMPeUyHo4nBU94Ruqkq";





?>