<?php

// Menu Arrays

// Role logged User
$config_menu['role']['kitchen'] = array("slug"=>"kitchen", "name"=>"BurgerMeister", "permissions"=>array("kitchen", "controlling", "admin"));
$config_menu['role']['kitchen']['menu'] = array(
	"Dashboard"=>array("/dashboard", "fa-dashboard"), 
	"Warenwirtschaft"=>array("/warehouse", "fa-calculator"), 
	"Burger"=>array("/burger", "fa-certificate"), 
	"Zutaten"=>array("/ingredients", "fa-barcode"),  
	"Zusatzstoffe"=>array("/additives", "fa-hand-pointer-o"),
	"Mediathek"=>array("/media", "fa-film")
);

// Role Controlling
$config_menu['role']['user'] = array("slug"=>"controlling", "name"=>"Controlling", "permissions"=>array("controlling", "admin"));
$config_menu['role']['controlling']['menu'] = array(
	"Reporting"=>array("/reporting", "fa-bar-chart"), 
	"Bestellungen"=>array("/purchase", "fa-reorder"), 
	"Kunden"=>array("/user", "fa-user")
);

// Role Admin
$config_menu['role']['admin'] = array("slug"=>"admin", "name"=>"Admin", "permissions"=>array("admin"));
$config_menu['role']['admin']['menu'] = array(
	"Setup"=>array("/setup", "fa-dashboard"), 
	"Admins"=>array("/admin", "fa-star")
);

?>