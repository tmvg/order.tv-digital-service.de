<?php

class Error extends Controller {
	
	function index($url = null)
	{
		
		$model = $this->loadModel('VotingModel');
		
		if(!$voting = $model->getVotingByPermalink($_SERVER["REQUEST_URI"])) $this->error404();

		
		$controller = "voting";
		$action = "vote";
		
		// Get our controller file
	    $path = APP_DIR . 'controllers/' . $controller . '.php';
		if(file_exists($path)){
	        require_once($path);
		} else {
			$controller = $config['error_controller'];
	        require_once(APP_DIR . 'controllers/' . $controller . '.php');
		}
	    
	    // Check the action exists
	    if(!method_exists($controller, $action)){
	        $controller = $config['error_controller'];
	        require_once(APP_DIR . 'controllers/' . $controller . '.php');
	        $action = 'index';
	    }
		
		// Create object and call method
		$obj = new $controller;
	    die(call_user_func_array(array($obj, $action),array($voting["id"])));
		
		
		
		
		var_dump($voting);
	}

	
	function error404()
	{
		$template = $this->loadView('_error');
		$error .= '<h1>Sorry, hier ging etwas schief!</h1>';
		$template->set("error", $error);
		$template->render();
	}
	
	function permission()
	{
		global $config;
		$template = $this->loadView('_error');
		$error .= '<h1>Hoppla!</h1>';
		$error .= '<p>Hierzu reicht Ihre Berechtigung nicht aus!</p>';
		$error .= '<p><a href="'.$config['base_url'].'" class="btn btn-success">zur Startseite</a></p>';
		$template->set("error", $error);
		$template->render();
		
	}
    
}
