<?php 
	
	


	
	
class label_helper {
	
	var $header = "";
	
	function __construct(){
		global $config;
		
		
		$this->header = $config['label']['header'];

	}
	

	
	function generate($labelData){
		
		
		$orderIDs = false;
		
		foreach($labelData as $label){
			$orderIDs[] = $this->render($label);
		}
		
		return $orderIDs;		
	}	
	
	
	private function render($labelData){
		global $config;
		global $setup;

		$order = $labelData["order"];
		$zutaten = $labelData["zutaten"];
		$price = $labelData["order"]["orderPrice"];
		$burgerCount =  $labelData["order"]["burgerCount"];
		$burgerName = $labelData["order"]["burgerName"];

		$my_img = imagecreate( 580, 1200 );
	
		$background = imagecolorallocate( $my_img, 255, 255, 255 );
		$text_colour = imagecolorallocate( $my_img, 0, 0, 0 );
		$line_colour = imagecolorallocate( $my_img, 0, 0, 0 );
		

		$logo = imagecreatefrompng($this->header);
		
		$brand = imagecreatefrompng($config['label']['brand']);
		// Copy and merge
		imagecopymerge($my_img, $logo, 50, 10, 0, 0, 500, 255, 100);

		$code = "Ihr Abholcode:";
		ImageTTFText ($my_img, 15, 0, $this->offsetCenter($code,"static/fonts/OpenSans-Regular.ttf",15), 330, $text_colour, "static/fonts/OpenSans-Regular.ttf", $code);

		$code = $burgerCount."x ".$order["orderCode"];
		ImageTTFText ($my_img, 40, 0, $this->offsetCenter($code,"static/fonts/OpenSans-Bold.ttf",40), 380, $text_colour, "static/fonts/OpenSans-Bold.ttf", $code);
		imagerectangle ( $my_img , 50 , 300 , 550 ,400 , $line_colour );
		
		if(strlen(trim($burgerName)) > 0) ImageTTFText ($my_img, 25, 0, 50, 445 , $text_colour, "static/fonts/OpenSans-Bold.ttf", '"'.$burgerName.'"');
		
		ImageTTFText ($my_img, 25, 0, 50, 480 , $text_colour, "static/fonts/OpenSans-Bold.ttf", "Zutaten");

		imagecopymerge($my_img, $brand, 50, 760, 0, 0, 500, 300, 100);
		
		$this->imageTextWrapped($my_img,50,490,500,"static/fonts/OpenSans-Regular.ttf",$text_colour,implode(", ",$zutaten),20);

		ImageTTFText ($my_img, 15, 0, 55, 1105 , $text_colour, "static/fonts/OpenSans-Bold.ttf", str_replace(".",",",$price) ." ".$setup['currency']);

		$code = "Abholzeit:";
		ImageTTFText ($my_img, 15, 0, $this->offsetCenter($code,"static/fonts/OpenSans-Regular.ttf",15), 1110, $text_colour, "static/fonts/OpenSans-Regular.ttf", $code);

		$code = date("d.m.Y H:i", $order["orderTimeslot"])."Uhr";
		ImageTTFText ($my_img, 30, 0, $this->offsetCenter($code,"static/fonts/OpenSans-Bold.ttf",40), 1160, $text_colour, "static/fonts/OpenSans-Bold.ttf", $code);
		
		imagerectangle ( $my_img , 50, 1080 , 550 ,1180 , $line_colour );
		
		$filename = $order["orderID"].".png";

		imagepng( $my_img,$config['label']['path']. $filename);
		imagedestroy($my_img);

			
		return $order["orderID"];
		
	}
	
	
	function offsetCenter($text,$font,$fontsize){
		$strlen = strlen($text);
		$sum = 0;
		
		for ($i = 0; $i < $strlen; $i++)
		 {
		    $dimensions = imagettfbbox($fontsize, 0, $font, $text[$i]);
		    $sum += $dimensions[2];
		
		 }
		
		return round(285 - ($sum / 2)) ;
	}
	
	
	function resize($filename){
        
        $percent = 0.5;
    
        
        // Neue Größe berechnen
        list($width, $height) = getimagesize($filename);
        $newwidth = $width * $percent;
        $newheight = $height * $percent;
        
        // Bild laden
        $mailImg = imagecreatetruecolor($newwidth, $newheight);
        $source = imagecreatefrompng($filename);
        
        // Skalieren
        imagecopyresized($mailImg, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        
        
        $filename = $filename."_mail.png";
        
        imagepng($mailImg, $filename);


        
        return $filename;        
                
    }	
	
	
	function imageTextWrapped(&$img, $x, $y, $width, $font, $color, $text, $textSize, $align="l") {
	    //Recalculate X and Y to have the proper top/left coordinates instead of TTF base-point
	    $y += $textSize;
	    $dimensions = imagettfbbox($textSize, 0, $font, " "); //use a custom string to get a fixed height.
	    $x -= $dimensions[4]-$dimensions[0];
	
	    $text = str_replace ("\r", '', $text); //Remove windows line-breaks
	    $srcLines = explode ("\n", $text); //Split text into "lines"
	    $dstLines = Array(); // The destination lines array.
	    foreach ($srcLines as $currentL) {
	        $line = '';
	        $words = explode (" ", $currentL); //Split line into words.
	        foreach ($words as $word) {
	            $dimensions = imagettfbbox($textSize, 0, $font, $line.$word);
	            $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line, if the word is to be included
	            if ($lineWidth > $width && !empty($line) ) { // check if it is too big if the word was added, if so, then move on.
	                $dstLines[] = ' '.trim($line); //Add the line like it was without spaces.
	                $line = '';
	            }
	            $line .= $word.' ';
	        }
	        $dstLines[] =  ' '.trim($line); //Add the line when the line ends.
	    }
	    //Calculate lineheight by common characters.
	    $dimensions = imagettfbbox($textSize, 0, $font, "MXQJPmxqjp123"); //use a custom string to get a fixed height.
	    $lineHeight = $dimensions[1] - $dimensions[5]; // get the heightof this line
	
	    $align = strtolower(substr($align,0,1)); //Takes the first letter and converts to lower string. Support for Left, left and l etc.
	    foreach ($dstLines as $nr => $line) {
	        if ($align != "l") {
	            $dimensions = imagettfbbox($textSize, 0, $font, $line);
	            $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line
	            if ($align == "r") { //If the align is Right
	                $locX = $x + $width - $lineWidth;
	            } else { //If the align is Center
	                $locX = $x + ($width/2) - ($lineWidth/2);
	            }
	        } else { //if the align is Left
	            $locX = $x;
	        }
	        $locY = $y + ($nr * $lineHeight);
	        //Print the line.
	        imagettftext($img, $textSize, 0, $locX, $locY, $color, $font, $line);
	    }        
	}
		

}