<?php



include(APP_DIR .'config/config.php');	
include(APP_DIR .'plugins/PHPMailer/PHPMailerAutoload.php');

define("MAIL_REPLY", $config['mail_reply']);
define("MAIL_NAME", $config['mail_name']);

/*
define("COOKIE_RUNTIME", 1209600);
define("COOKIE_DOMAIN", $config['domain']);
define("COOKIE_SECRET_KEY", "1gp@TMPS{+$78sfpMJFe-92s");
*/
define("EMAIL_USE_SMTP", $config['mail_use_smtp']);
define("EMAIL_SMTP_HOST", $config['mail_host']);
define("EMAIL_SMTP_AUTH", $config['mail_smtp_auth']);
define("EMAIL_SMTP_USERNAME", $config['mail_smtp_username']);
define("EMAIL_SMTP_PASSWORD", $config['mail_smtp_password']);
define("EMAIL_SMTP_ENCRYPTION", $config['mail_smtp_encryption']);
define("EMAIL_SMTP_PORT", $config['mail_smtp_port']);

define("EMAIL_TEMPLATE_HEADER", $config['mail_template_header']);
define("EMAIL_TEMPLATE_FOOTER", $config['mail_template_footer']);
define("EMAIL_TEMPLATE_PATH", $config['mail_template_path']);



class Mail_helper extends PHPMailer {


	function __construct($exceptions = false){
		parent::__construct();
	
		
		if(! EMAIL_USE_SMTP){
			$this->IsSendmail(); 								// set Mailer to Sendmail
		}else{
			$this->isSMTP(); 									// set Mailer to SMTP  
			$this->Host = EMAIL_SMTP_HOST;  				  	// Specify main and backup SMTP servers
			$this->SMTPSecure = EMAIL_SMTP_ENCRYPTION;          // Enable TLS encryption, `ssl` also accepted
			$this->Mailer = "smtp"; 
			$this->SMTPKeepAlive = true;  
	
			if(EMAIL_SMTP_AUTH) $this->SMTPAuth = true;        // Enable SMTP authentication
			$this->Username = EMAIL_SMTP_USERNAME;             // SMTP username
			$this->Password = EMAIL_SMTP_PASSWORD;             // SMTP password
			$this->Port = EMAIL_SMTP_PORT;                     // TCP port to connect to	                                 
		}
		$this->From = MAIL_REPLY;
		$this->FromName = MAIL_NAME;

		$this->AddReplyTo(MAIL_REPLY, MAIL_NAME);
		$this->CharSet = 'UTF-8';
		$this->isHTML(true);  

	}
	
	function sendMail($sendto, $firstname, $lastname,  $subject, $mailcontent, $attachment = NULL) {
		
		//$this->clearAttachments();
		$this->MessageID = "<".md5(time().'_'.$mailto).COOKIE_DOMAIN;
		$this->AddAddress($sendto, $sendto);
		
		$htmltext = $mailcontent;
		
		$this->Subject = $subject;
		$this->Body    = trim( ($htmltext));
		$this->AltBody  = strip_tags($htmltext);

		//if($attachment) $this->AddStringAttachment($attachment[0], $attachment[1], $attachment[2], $attachment[3]);
		//if($attachment != "")	$this->AddAttachment( $attachment , 'label.png' );
		//$this->addAttachment($attachment, 'label.png');    // Optional name
		//$this->addAttachment($attachment, 'label.png');
// 		$this->AddEmbeddedImage($attachment, $labelname, $labelname.".png");
		
		if(! $this->send()) {
		
			$this->clearAllRecipients();
			$this->clearAttachments();

			$mailContent = ob_get_contents();

			ob_end_clean();	

			return false;
		} else {
			
			$mailContent = ob_get_contents();


			$this->clearAllRecipients();
			$this->clearAttachments();

			return true;
		}

		
	}

}

$mail = new Mail_helper();


