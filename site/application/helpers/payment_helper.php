<?php 
	
	
	require_once 'application/plugins/omnipay/autoload.php';
	use Omnipay\Omnipay;


	
	
class payment_helper {
	
	var $gateway = false;
	var $gatewayName = "";
	
	var $response = false;
	
	public function init($gateway){

		global $config;
		
		if(!isset($config['payment'][$gateway])) throw new Exception("Payment Gateway ".$gateway." nicht definiert.");
		
		$this->gatewayName = $gateway;

		switch($gateway){
			
			case "Sofort":
				$this->gateway = Omnipay::create($gateway);
				$this->gateway->initialize(array(
				    'username' => $config['payment'][$gateway]['username'],
				    'password' => $config['payment'][$gateway]['apiKkey'] ,
				    'projectId' => $config['payment'][$gateway]['projectId'],
				    'testMode' => $config['payment'][$gateway]['testMode']
				));
				break;
			case "PayPal_Express":
				$this->gateway = Omnipay::create($gateway);
				$this->gateway->setUsername($config['payment'][$gateway]['username']);
				$this->gateway->setPassword($config['payment'][$gateway]['password']);
				$this->gateway->setSignature($config['payment'][$gateway]['signature']);
				$this->gateway->setTestMode($config['payment'][$gateway]['testMode']);
				break;
		}

	}
		
		
	function pay($amount = 0,$description = ""){
		global $config;

		if(strlen($description) == 0) throw new Exception ("Keine Produktbeschreibung angegeben");
		if($amount == 0) throw new Exception ("Preis muss größer als 0 sein.");
		
		switch($this->gatewayName){
			case "Sofort":
				$this->response = $this->gateway->authorize(array('amount' => $amount, 'description' => $description ))->send();
				break;
			case "PayPal_Express":
				
				if(strlen($description) == 0) throw new Exception ("Für Paypal wird eine Transaktions ID aus dem Shop System benötigt.");
			
				$this->response = $this->gateway->purchase(
					array(
				        'amount' => $amount,
				        'currency' => 'EUR',
				        'transactionId' => $description,
				        'returnUrl' => $config['payment']['PayPal_Express']['return']["success"],
				        'cancelUrl' => $config['payment']['PayPal_Express']['return']["cancel"]
				    )
				)->send();
				
				break;
		}

		
		$transactionReference = $this->response->getTransactionReference();
		return $transactionReference;

	}
	
	public function redirect(){
		if ($this->response->isRedirect()) {
		    // redirect to offsite payment gateway
		    $this->response->redirect();
		} else {
		    // payment failed: display message to customer
		    throw new Exception($this->response->getMessage());
		}
	}
	
	
	function authorizeSofort($transactionId){
		$response = $this->gateway->completeAuthorize(array(
		    'transactionId' => $transactionId,
		))->send();
		
		if ($response->isSuccessful()) {
		    return true;
		} 
		
		return false;
	}


	function authorizePaypal($amount,$description){
		global $config;

		// load amount and description from database by $transactionId
		$response = $this->gateway->completePurchase(array(
				        'amount' => $amount,
				        'currency' => 'EUR',
				        'transactionId' => $description,
				        'returnUrl' => $config['payment']['PayPal_Express']['return']["success"],
				        'cancelUrl' => $config['payment']['PayPal_Express']['return']["cancel"]
				    )
			)->send();
		
		if ($response->isSuccessful()) {
		    $data = $response->getData();
		 
			//save Transaktion DATA ARRAY TO DB
			
			if($data["ACK"] == "Success" ) return true;
		 
		
		} 
		
		return false;

	}

	
function paydirektOrder($amount, $email, $orderCode) {
	
	global $config;
	
	$ENDPOINT = $config['payment']['pay_direkt']['api_url']."api/checkout/v1/checkouts";
	$access_token = $this->paydirektConnect();

	
	$order = json_encode(array(
	    'type' => 'DIRECT_SALE',
	    'totalAmount' => $amount,
	    'currency' => 'EUR',
	    'deliveryType' => 'STORE_PICKUP',
		'shippingAddress' => [
		    "emailAddress" => $email,
		    "addresseeGivenName" => "",
		    "addresseeLastName" => ""
		 ],
		 'shoppingCartType' => "DIGITAL",
	
	    'merchantOrderReferenceNumber' => $orderCode,
	    'redirectUrlAfterSuccess' => $config['payment']['pay_direkt']['return']["success"]."&transaction=".$orderCode,
	    'redirectUrlAfterCancellation' => $config['payment']['pay_direkt']['return']["cancel"]."&transaction=".$orderCode,
	    'redirectUrlAfterRejection' => $config['payment']['pay_direkt']['return']["cancel"]."&transaction=".$orderCode,
	    'note' => 'Vielen Dank',
	));
	
	$now = new \DateTime("now", new \DateTimeZone('UTC'));
	
	$headerOrder = array();
	array_push($headerOrder, "Authorization: " ."Bearer ".$access_token);
	array_push($headerOrder, "Content-Type: application/hal+json;charset=utf-8");
	array_push($headerOrder, "Accept: application/hal+json");
	array_push($headerOrder, "Date: " .$now->format(DATE_RFC1123));
	
	$request = curl_init();
	curl_setopt($request, CURLOPT_URL, $ENDPOINT);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($request, CURLOPT_HTTPHEADER, $headerOrder);
	curl_setopt($request, CURLOPT_POST, 1);
	curl_setopt($request, CURLOPT_POSTFIELDS, $order);
	
	$response = curl_exec($request);
	$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	
	if ($responseCode != 201)
	{
	    $message = ($responseCode > 0 ? "Unexpected status code " .$responseCode .": " .$response : "");
	    $message .= (curl_error($request) ? curl_error($request) : "");
	    echo "<pre>error";
	    print_r($message);
	    echo "</pre>";
	    
	}
	
	

	
	curl_close($request);
	
	$responseDecoded = json_decode($response, true);
	return $responseDecoded;
	
}	
	
function paydirektExpressOrder($amount, $orderCode) {
	
	global $config;
	
	$ENDPOINT = $config['payment']['pay_direkt']['api_url']."api/checkout/v1/checkouts";
	$access_token = $this->paydirektConnect();

	
	$order = json_encode(array(
	    'type' => 'DIRECT_SALE',
	    'express' => true,
	    'totalAmount' => $amount,
	    'orderAmount' => $amount,
	    'currency' => 'EUR',
	    'deliveryType' => 'STORE_PICKUP',
	   /* 'shippingAddress' => [
		    "emailAddress" => "a.hettinger@tmvg.de",
		    "addresseeGivenName" => "",
		    "addresseeLastName" => ""
		 ],
		 */
		 'shoppingCartType' => "DIGITAL",
	
	    'merchantOrderReferenceNumber' => $orderCode,
	    'redirectUrlAfterSuccess' => $config['payment']['pay_direkt']['return']["success"]."&transaction=".$orderCode,
	    'callbackUrlCheckDestinations' => "https://burgenerator.de/payment/paydirektCallback/", //$config['payment']['pay_direkt']['return']["callback"]."&transaction=".$orderCode,
	    'redirectUrlAfterCancellation' => $config['payment']['pay_direkt']['return']["cancel"]."&transaction=".$orderCode,
	    'redirectUrlAfterRejection' => $config['payment']['pay_direkt']['return']["cancel"]."&transaction=".$orderCode,
	    'note' => 'Vielen Dank',
	    'webUrlShippingTerms' => "http://burgenerator.de/article/text/impressum"
    
	));
	
	$now = new \DateTime("now", new \DateTimeZone('UTC'));
	
	$headerOrder = array();
	array_push($headerOrder, "Authorization: " ."Bearer ".$access_token);
	array_push($headerOrder, "Content-Type: application/hal+json;charset=utf-8");
	array_push($headerOrder, "Accept: application/hal+json");
	array_push($headerOrder, "Date: " .$now->format(DATE_RFC1123));
	
	$request = curl_init();
	curl_setopt($request, CURLOPT_URL, $ENDPOINT);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($request, CURLOPT_HTTPHEADER, $headerOrder);
	curl_setopt($request, CURLOPT_POST, 1);
	curl_setopt($request, CURLOPT_POSTFIELDS, $order);
	
	$response = curl_exec($request);
	$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	
	if ($responseCode != 201)
	{
	    $message = ($responseCode > 0 ? "Unexpected status code " .$responseCode .": " .$response : "");
	    $message .= (curl_error($request) ? curl_error($request) : "");
	    echo "<pre>error";
	    print_r($message);
	    echo "</pre>";
	    
	}
	
	

	
	curl_close($request);
	
	$responseDecoded = json_decode($response, true);
	echo "<pre>";
	print_r($responseDecoded);
	echo "</pre>";
	
	die();
	
	return $responseDecoded;
	
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pay Direkt Helper
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function createRandomUUID() {
    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function createRandomNonce() {
    $bytes = openssl_random_pseudo_bytes(48);
    return $this->encode($bytes);
}
	
function encode($string) {
    return strtr(base64_encode($string), '+/', '-_');
}

function decode($string) {
    return base64_decode(str_pad(strtr($string, '-_', '+/'), strlen($string) % 4, '=', STR_PAD_RIGHT));
}

function isBase64UrlEncoded($string) {
    return preg_match("/^[0-9a-zA-Z-_]+[=]{0,2}$/", $string) ? true : false;
}	

function isUUID($string) {
    return preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/", $string) ? true : false;
}

function signature($requestId, $timestamp, $apiKey, $apiSecret, $randomNonce) {
    
    $CRYPTO_ALGORITHM = "sha256";
    $this->validateApiSecret($apiSecret);

    $stringToSign = $this->stringToSign($requestId, $timestamp, $apiKey, $randomNonce);
    $apiSecretDecoded = $this->decode($apiSecret);

    if (!in_array($CRYPTO_ALGORITHM, hash_algos(), true)) {
        print("Could not initialize hmac. " .$CRYPTO_ALGORITHM ." is not supported.");
    }

    $hash = hash_hmac($CRYPTO_ALGORITHM, $stringToSign, $apiSecretDecoded, true);
    $signature = $this->encode($hash);

    return $signature;
}

function stringToSign($requestId, $timestamp, $apiKey, $randomNonce) {
    $this->validateRequestId($requestId);
    $this->validateDateString($timestamp);
    $this->validateApiKey($apiKey);
    $this->validateNonce($randomNonce);
    $stringToSign = sprintf("%s:%s:%s:%s", $requestId, $timestamp, $apiKey, $randomNonce);
    return $stringToSign;
}

function validateNonce($randomNonce) {
    if ($randomNonce === NULL || !trim($randomNonce)) {
        print("randomNonce is not set");
    }
    if (strlen($randomNonce) < 10) {
        print("randomNonce is not greater or equal the minimum length (10): " .$randomNonce);
    }
    if (strlen($randomNonce) > 64) {
        print("randomNonce is not less or equal the maximum length (64): " .$randomNonce);
    }
    /*
    if (isBase64UrlEncoded($randomNonce)) {
        print("randomNonce is not base 64 url encoded: " .$randomNonce);
    }
    */
}

function validateApiKey($apiKey) {
    if ($apiKey === NULL || !trim($apiKey)) {
        print("apiKey is not set");
    }
    if (!$this->isUUID($apiKey)) {
        print("apiKey is not a valid UUID: " .$apiKey);
    }
}

function validateApiSecret($apiSecret) {
    if ($apiSecret === NULL || !trim($apiSecret)) {
        print("apiSecret is not set");
    }
    /*
    if (!isBase64UrlEncoded($apiSecret)) {
        print("apiSecret is not base 64 url encoded");
    }
    */
    if (strlen($this->decode($apiSecret)) < 32) {
        print("apiSecret is not less or equal the maximum length (32): " .strlen($this->decode($apiSecret)));
    }
}

function validateRequestId($requestId) {
    if ($requestId === NULL || !trim($requestId)) {
        print("requestId is not set");
    }
    if (!$this->isUUID($requestId)) {
        print("requestId is not a valid UUID: " .$requestId);
    }
}

function validateDateString($dateString) {
    if ($dateString === NULL || !trim($dateString)) {
        print("dateString is not set");
    }

    $ts = \DateTime::createFromFormat('YmdHis', $dateString, new \DateTimeZone('UTC'));
    if (strlen($dateString) != 14 || ($ts && $ts->format('YmdHis') != $dateString)) {
        print("dateString is not a valid timestamp in format yyyyMMddHHmmss: " .$dateString);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
function paydirektConnect() {
	global $config;
	$API_KEY = $config['payment']['pay_direkt']['api_key'];
	$API_SECRET = $config['payment']['pay_direkt']['api_secret'];

	
	$ENDPOINT = $config['payment']['pay_direkt']['api_url']."api/merchantintegration/v1/token/obtain";
	$requestId = $this->createRandomUUID();
	$randomNonce = $this->createRandomNonce();
	$now = new \DateTime("now", new \DateTimeZone('UTC'));
	$timestamp = $now->format('YmdHis');
	$signature = $this->signature($requestId, $timestamp, $API_KEY, $API_SECRET, $randomNonce);
	
	$header = array();
	array_push($header, "X-Date: " .$now->format(DATE_RFC1123));
	array_push($header, "X-Request-ID: " .$requestId);
	array_push($header, "X-Auth-Key: " .$API_KEY);
	array_push($header, "X-Auth-Code: " .$signature);
	array_push($header, "Content-Type: application/hal+json;charset=utf-8");
	array_push($header, "Accept: application/hal+json");
	
	$payload = json_encode(array(
	    'grantType' => 'api_key',
	    'randomNonce' => $randomNonce
	));
	
	$request = curl_init();
	curl_setopt($request, CURLOPT_URL, $ENDPOINT);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($request, CURLOPT_HTTPHEADER, $header);
	curl_setopt($request, CURLOPT_POST, 1);
	curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
	
	$response = curl_exec($request);
	$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	
	if ($responseCode != 200)
	{
	    $message = ($responseCode > 0 ? "Unexpected status code " .$responseCode .": " .$response : "");
	    $message .= (curl_error($request) ? curl_error($request) : "");
	    echo "<pre>";
	    print_r($message);
	    echo "</pre>";
	}
	
	curl_close($request);
	
	$responseDecoded = json_decode($response, true);
	$access_token = $responseDecoded[access_token];
	
	return $access_token;
}


function getPaymentStatus($checkoutID) {
	
	global $config;
	$ENDPOINT = $config['payment']['pay_direkt']['api_url']."api/checkout/v1/checkouts/$checkoutID";
	$access_token = $this->paydirektConnect();
	
	$now = new \DateTime("now", new \DateTimeZone('UTC'));
	
	$headerOrder = array();
	array_push($headerOrder, "Authorization: " ."Bearer ".$access_token);
	array_push($headerOrder, "Content-Type: application/hal+json;charset=utf-8");
	array_push($headerOrder, "Accept: application/hal+json");
	array_push($headerOrder, "Date: " .$now->format(DATE_RFC1123));	
	
	$request = curl_init();
	curl_setopt($request, CURLOPT_URL, $ENDPOINT);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($request, CURLOPT_HTTPHEADER, $headerOrder);
	curl_setopt($request, CURLOPT_HTTPGET, 1);
	
	$response = curl_exec($request);
	$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	
	if ($responseCode != 200)
	{
	    $message = ($responseCode > 0 ? "Unexpected status code " .$responseCode .": " .$response : "");
	    $message .= (curl_error($request) ? curl_error($request) : "");
	}
	
	curl_close($request);
	
	$responseDecoded = json_decode($response, true);	
	
	return $responseDecoded;
	
}

	

}