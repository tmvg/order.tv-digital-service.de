<?php
/*
 * PIP v0.5.3
 */
error_reporting(E_ALL & ~E_NOTICE);
//Start the Session
session_start(); 

//if($_SESSION["userRole"] == "") header("Location: /soon.html");

// Defines
define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('APP_DIR', ROOT_DIR .'application/');

// Includes
require_once(APP_DIR .'config/config.php');
require_once(APP_DIR .'config/menu.php');
require_once(APP_DIR .'config/setup.php');
require(ROOT_DIR .'static/lang/'.$config['language'].'.php');
require(ROOT_DIR .'system/model.php');
require(ROOT_DIR .'system/view.php');
require(ROOT_DIR .'system/controller.php');
require(ROOT_DIR .'system/pip.php');

// Define base URL
global $config;
global $setup;
global $config_menu;
define('BASE_URL', $config['base_url']);
define('DOMAIN', $config['domain']);
define('MEDIA_URL', $config['media_url']);
define('IMG_URL', $config['image_url']);
define('TOKEN', $config['token']);
define('TITLE', $config['title']);

pip();

?>
