<?php

class Controller {
	
	public function loadModel($name)
	{
		require_once(APP_DIR .'models/'. $name .'.php'); 

		$model = new $name;
		return $model;
	}
	
	public function loadView($name)
	{
		$view = new View($name);
		return $view;
	}
	
	public function loadPlugin($name)
	{
		require_once(APP_DIR .'plugins/'. $name .'.php');
	}
	
	public function loadHelper($name)
	{
		require_once(APP_DIR .'helpers/'. $name .'.php');
		$helper = new $name;
		return $helper;
	}
	
	public function redirect($loc)
	{
		global $config;
		
		header('Location: '. $config['base_url'] . $loc);
	}
	
	public function checkRequiredFields($dataArray, $params) {
		
		$error = array();
		$params = explode(",", $params);
		
		foreach($params as $p) {
			
			if($dataArray[$p] == "") array_push($error, $p);
			
		}
		
		if(count($error) > 0) { return $error; } else return false;
		
	}	
	
	public function isLogged() {
		$session = $this->loadHelper('session_helper');
		if($session->get("login")) return "logged"; else return "no";
	}
	
	public function getRole() {
		$user = $this->loadHelper('auth_helper');
		$role = $user->getRole();
		return $role;
	}	
	
	public function checkRole($roles) {
		$user = $this->loadHelper('auth_helper');
		$role = $user->getRole();
		if(in_array($role, $roles)) return true; else return false;
	}
    
}


?>